## 箭头函数
+ Arrow Function(箭头函数)
```
x => x * x

   等于

function (x) {
    return x + x;
}
```

+ 如果参数不是一个，就需要用括号()括起来
```
(x, y) => x * x + y * y
单行箭头函数
let x = () x+x;
多行箭头函数
let x = () =>{
    conole.log();
    return x+x
}
```
+ this

箭头函数内部的this是词法作用域