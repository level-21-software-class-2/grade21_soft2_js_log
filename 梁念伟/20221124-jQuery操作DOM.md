## 操作DOM
修改Text和HTML
jQuery对象的text()和html()方法分别获取节点的文本和原始HTML文本，例如，如下的HTML结构：
```
<!-- HTML结构 -->
<ul id="test-ul">
    <li class="js">JavaScript</li>
    <li name="book">Java &amp; JavaScript</li>
</ul>
分别获取文本和HTML：

$('#test-ul li[name=book]').text(); // 'Java & JavaScript'
$('#test-ul li[name=book]').html(); // 'Java &amp; JavaScript'
```
修改css
```
css();//获取css属性
css('color','red')//设置属性
css('color','')//清除属性
addclass();//增加class属性，不覆盖原有
```
显示和隐藏DOM
```
hide();//隐藏
show();//显示
```