## Ajax

### Ajax概念

ajax：async javascript and xml - 前端和后端的交互技术，是异步的js技术，作用是前端跟后端进行传递数据

### 使用js的Ajax跟服务器接口进行交互

步骤：

#### 一、创建对象



```js
var xhr = new XMLHttpRequest()
```

#### 二、设置请求参数



```js
xhr.open('get', 'http://localhost:8888/test/first', true)
```

参数一：请求的方式

参数二：请求的地址

参数三：是否异步执行 - 为布尔值默认为true表示异步执行

#### 三、监听请求状态



```js
xhr.onreadystatechange = function() {
    if(xhr.readyState === 4) {
        if(xhr.status >= 200 && xhr.status < 300) {
            var res = xhr.responseText
            console.log(res);
        }
    }
}
```

1. 其中`xhr.onreadystatechange`是类似绑定事件的语法，表示这个事件是当ajax请求的状态发生改变的时候触发

   

   ```js
   xhr.onreadystatechange = function() {}
   ```

2. `xhr.readyState`表示ajax的状态，总共有5种状态对应五种返回值

   0 -- 表示没有初始化，说明还没有创建对象

   1 -- 表示已经创建对象但是，还没有发送请求

   2 -- 表示已经发送，对方已经收到消息了，但是还没有彻底看懂我们什么意思，还有很多事情需要准备

   3 -- 服务器已经在给我们响应信息了，但是信息不完整

   4 -- 数据已经完整了 - 可以接收到完整的数据了

3. `xhr.status`判断请求是否成功，成功的请求是以2开头的

   

   ```js
           if(xhr.status >= 200 && xhr.status < 300) {
               var res = xhr.responseText
               console.log(res);
           }
            // 请求状态-http状态：
           /*
           1开头：正在进行中
           2开头：各种成功
           3开头：重定向-缓存
           4开头：客户端错误
           5开头：服务器错误
           */
   ```

4. 接收传来的数据并处理

#### 四、发送请求

`xhr.send()`来向服务器发送请求

### Ajax的一些操作

1. `xhr.responseText`

   获取从服务器端发送过来的数据

2. `JSON.parse(json字符串)`

   可以将Json字符串转化成为数组

3. `JSON.stringify(json对象)`

   json对象转成json字符串

### get请求参数

get请求：

1. 直接在浏览器中输入地址敲回车
2. 在页面中点击了a标签跳转到另一个页面，显示另一个页面中的内容，显示内容也是要发送请求的 - get
3. 引入文件的请求也是get请求，link引入css、script引入js、img引入图片、iframe引入html页面
4. form表单默认能提交，默认提交方式也是get
5. 通过ajax发送get请求



```js
var xhr = new XMLHttpRequest;
xhr.open('post','http://localhost:8888/test/third?username=zhangsan&age=18');
xhr.send()
xhr.onreadystatechange = function(){
    if(xhr.readyState === 4){
        if(xhr.status>=200 && xhr.status<300){
            var res = xhr.responseText;
            // 将json字符串转成json对象
            // res = JSON.parse(res)
            console.log(res);
        }
    }
}
```

### POST请求参数



```js
var xhr = new XMLHttpRequest;

xhr.open('post','http://localhost:8888/test/fourth');
// post请求的参数要放在send方法中作为参数的 - 必须的字符串
// post请求要带参数必须在send之前设置 头信息
xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded')
// 数据在传送之前需要进行编码
xhr.send('name=王五&age=666')
xhr.onreadystatechange = function(){
    if(xhr.readyState === 4){
        if(xhr.status>=200 && xhr.status<300){
            var res = xhr.responseText;
            res = JSON.parse(res)
            console.log(res);
        }
    }
}
```

