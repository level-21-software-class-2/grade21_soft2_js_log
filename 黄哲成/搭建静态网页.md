### 搭建静态网页

```
先在阿里云的域名中先添加解析主机记录为：efg.shaolai.top  记录值为IP地址，其它默认
mkdir /var/www      //在/var/下创建www文件夹，已有就可忽略
1.cd  /var/www      //进入这个文件路径

2.mkdir efg.shaolai.top            //创建这个域名efg.shaolai.top
3.cd /var/www/efg.shaolai.top/     //进入这个路径

4.vim index.html    //创建index网页
5. 进入 index.html后写好基本框架和内容  --按i进入写入模式，按esc推出写入模式，再打 ：wq 保存退出

6.cd /etc/nginx/conf.d            //进入/etc/nginx/conf.d/目录下，
7.vim efg.shaolai.top.conf        //创建一个以域名为名称的配置文件

8.配置文件的命令如下：
server{
	listen 80;  //监听的端口
	server_name efg.shaolai.top;   //监听的域名
	
		location /{
			root /var/www/efg.shaolai.top;   //网站所在的路径
			index index.html;                //默认的首页文件
		}

}

9.然后就可以去浏览器输入网址愉快的测试了
```

