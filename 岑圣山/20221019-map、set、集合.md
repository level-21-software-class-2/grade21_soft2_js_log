## 一、Map

*Map*是一组键值对的结构，具有极快的查找速度。

举个例子，假设要根据同学的名字查找对应的成绩，如果用*Array*实现，需要两个*Array*： 

```js
1 var names = ['Michael', 'Bob', 'Tracy'];
2 var scores = [95, 75, 85]; 
```

给定一个名字，要查找对应的成绩，就先要在names中找到对应的位置，再从*scores*取出对应的成绩，*Array*越长，耗时越长。

如果用***Map***实现，只需要一个“名字”-“成绩”的对照表，直接根据名字查找成绩，无论这个表有多大，查找速度都不会变慢。用JavaScript写一个Map如下： 

```js
1 var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
2 m.get('Michael'); // 95 
```

初始化*Map*需要一个二维数组，或者直接初始化一个空*Map*。*Map*具有以下方法： 

```js
1 var m = new Map(); // 空Map
2 m.set('Adam', 67); // 添加新的key-value
3 m.set('Bob', 59);
4 m.has('Adam'); // 是否存在key 'Adam': true
5 m.get('Adam'); // 67
6 m.delete('Adam'); // 删除key 'Adam'
7 m.get('Adam'); // undefined 
```

由于一个*key*只能对应一个*value*，所以，多次对一个*key*放入*value*，后面的值会把前面的值冲掉：

```js
1 var m = new Map();
2 m.set('Adam', 67);
3 m.set('Adam', 88);
4 m.get('Adam'); // 88 
```

## 二、Set 

*Set*类似于数组，但是成员的值都是唯一的，没有重复的值。

*Set*本身是一个构造函数，用来生成 *Set* 数据结构。

```javascript
const s = new Set();

[2, 3, 5, 4, 5, 2, 2].forEach(x => s.add(x));

for (let i of s) {
  console.log(i);
}
// 2 3 5 4
```

上面代码通过***add()***方法向 *Set* 结构加入成员，结果表明 *Set* 结构不会添加重复的值。

向 Set 加入值的时候，不会发生类型转换，所以*5*和*"5"*是两个不同的值。*Set* 内部判断两个值是否不同，使用的算法叫做*“Same-value-zero equality”*，它类似于精确相等运算符（`===`），主要的区别是向 *Set* 加入值时认为*NaN*等于自身，而精确相等运算符认为*NaN*不等于自身。

*Set*和*Map*类似，也是一组*key*的集合，但不存储*value*。由于*key*不能重复，所以，在*Set*中，没有重复的*key*。

要创建一个*Set*，需要提供一个*Array*作为输入，或者直接创建一个空*Set*： 

```js
var s1 = new Set(); // 空Set
var s2 = new Set([1, 2, 3]); // 含1, 2, 3 
```

重复元素在*Set*中自动被过滤： 

```js
var s = new Set([1, 2, 3, 3, '3']);
s; // Set {1, 2, 3, "3"} 
```

注意数字`3`和字符串`'3'`是不同的元素。

通过*add(key)*方法可以添加元素到*Set*中，可以重复添加，但不会有效果： 

```js
>>> s.add(4)
>>> s
{1, 2, 3, 4}
>>> s.add(4)
>>> s
{1, 2, 3, 4} 
```

通过*delete(key)*方法可以删除元素： 

```js
var s = new Set([1, 2, 3]);
s; // Set {1, 2, 3}
s.delete(3);
s; // Set {1, 2} 
```