### push和pop
* push()向arr的末尾添加诺干个元素
* pop()向arr的最后的一个删除

```js
var arr = [1,2];
arr.push('A','B');
arr; //[1,2,A,B]
```
### unshift和shift
* 和push,pop函数相反
* unshift()从头部添加元素，shift()从头部删除一个元素

```js
var arr = [1,2];
arr.unshift('A','B');
arr; //[A,B,1,2]
```

### sort
* 对arr进行排序
```js
var arr = ['a','c','b'];
arr.sort
arr;//[a,b,c]
```

### join


