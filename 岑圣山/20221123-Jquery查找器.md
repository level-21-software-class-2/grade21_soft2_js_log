### 查找
使用find()方法，它本身又接收一个任意的选择器。

```js
用find()查找：

var ul = $('ul.lang'); // 获得<ul>
var dy = ul.find('.dy'); // 获得JavaScript, Python, Scheme

```
如果要从当前节点开始向上查找，使用parent()方法
```js
var a = $('[name=name]').parent();//一个上级
/*结果
S.fn.init [label, prevObject: S.fn.init(1)]
0: label
length: 1
...
*/
var a = $('[name=name]').parents();//所有上级
/*结果
0: label
1: p
2: form#test-form
3: body
4: html
length: 5
*/
```

如果要从当前节点开始向下查找，使用next()方法
```js
var a = $('#test-form p:first-child').next();
//原来的节点 <label>Name: <input name="name"></label>
//next后的节点 innerHTML: "<label>Email: <input name=\"email\"></la......
```
### 修改CSS

要高亮显示动态语言，调用jQuery对象的css('name', 'value')方法

为了和JavaScript保持一致，CSS属性可以用'background-color'和'backgroundColor'两种格式。

css()方法将作用于DOM节点的style属性，具有最高优先级。



### 练习：分别用css()方法和addClass()方法高亮显示JavaScript：
```js
<!-- HTML结构 -->
<style>
.highlight {
    color: #dd1144;
    background-color: #ffd351;
}
</style>

<div id="test-highlight-css">
    <ul>
        <li class="py"><span>Python</span></li>
        <li class="js"><span>JavaScript</span></li>
        <li class="sw"><span>Swift</span></li>
        <li class="hk"><span>Haskell</span></li>
    </ul>
</div>
'use strict';
var div = $('#test-highlight-css');
//设置Css
setTimeout(() => {
    var js = $('.js').css('color','red');
}, 1500);
//添加class
setTimeout(() => {
    var js = $('.js').addClass('jjjjjjjjjs');
    console.log(js);
}, 1500);

// TODO:
```



### 过滤

filter()方法可以过滤掉不符合选择器条件的节点：
```js
var a = langs.filter('.dy');

langs.filter(function () {
    return this.innerHTML.indexOf('S') === 0; // 返回S开头的节点
}); 

```