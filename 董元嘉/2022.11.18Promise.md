## Promise

在JS中代码都是单线程执行的，导致JS的所以网络操作，浏览器事件都必须异步执行，基于这个问题可能出现显示位置杂乱出错，异步执行可以用回调函数实现：

```javascript
function callback() {
	console.log('Done');
}
console.log('before setTimeout( )');
setTimeout(callback,1000); //1秒钟后调用callback函数
consoole.log('after setTimeout( )');
```

可见，异步操作会在将来的某个时间点触发一个函数调用。
AJAX就是典型的异步操作。

我们先看一个最简单的Promise例子：生成一个0-2之间的随机数，如果小于1，则等待一段时间后返回成功，否则返回失败：

```javascript
let fn = function (resolve, reject ){
	let rnd = parseInt(Math.random() * 2);  //Math.random()随机数
	

setTimeout( ( ) =>{
	if (rnd === 0){
		console.log('成功');
		resolve( );
	} else { 
		console.log('失败');
		reject( );
	}
	}
},1000);

}

let p1= new Peomise( fn );

p1.then(function(){
	console.log('做成大事'); //then()为执行成功与resolve()相绑定
}).catch(function(){
	console.log('没做成大事');//catch()为执行失败与reject()相绑定
})
```

可见Promise最大的好处是在异步执行的流程中，把执行代码和处理结果的代码清晰地分离了。

```javascript
let logger = document.getElementById('log');

function clearLog( ){
	logger.innerHTML = ' '; //将页面的内容清空
}

//将传入的字符串，构造为一个p标签，插入到logger元素下方
functtion log(str) {
	let p = document.createElement('p');
	p.textContent = str;
	logger.appendChild(p)
}

let fn = function (resolve,reject) {
	clearLog( );
	let rnd = Math.random( ) * 2;
	log('程序开始啦，接下来的耗时操作是：'+rnd)

setTimeout( ( ) =>{
if (rnd < 1){
	// console.log('成功')；
	log('成功')
	resolve();
} else {
	// console.log('失败');
	log('失败')
	reject();
	}
	},rnd * 1000);
}

let p1 = new Promise(fn);


p1.then(function (){
    console.log('我是刘备，我成功了');
}).catch(function(){
    console.log('我是曹操，失败了，又没有完全失败');
});
```

