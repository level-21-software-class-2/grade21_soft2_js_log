 闭包
将函数作为返回值
什么是闭包
1、闭包是一个函数
2、闭包是个被函数返回的函数
3、被函数A返回的函数B，并且函数B使用函数A的参数或局部变量
```js
 function fn(y){
    let x = y || 10 ;
    let inc = {
          newintc:function(){
               console.log(x);
          }        
    }
    return inc;
}
let fnn = fn(12);
fnn.newintc();
```
### 练习2

简单乘法，实现方法mult，可接受1~2个参数

1. 若参数中不包含回调函数，则返回一个包含mult方法的对象
2. 若参数中包含回调函数，则执行回调函数
3. 不需要考虑传入参数类型异常的情况

实现效果如下：



 
```js
mult(console.log) // 1
let mult4 = mult(4)
mult4(console.log) // 4
mult4.mult(7, console.log)//28
let mult28 = mult4.mult(7)
mult28(console.log) // 28
let mult2800 = mult28.mult(100)
mult2800(console.log) // 2800

multshow = mult(2, console.log) //2
// 计算10的阶乘
mult(1).mult(2).mult(3).mult(4).mult(5, console.log)//120
```


### 练习3

```js
/**
 * 实现栈结构
 */
function Stack() {
    this.items = [];
    this.push = item => {
        this.items.push(item);
    }
    this.pop = () => this.items.pop();
}
let stack = new Stack();

//经过测试发现问题。可以通过stack.items 直接访问和修改栈内的数据，请提出修改方案，要求不能直接访问items