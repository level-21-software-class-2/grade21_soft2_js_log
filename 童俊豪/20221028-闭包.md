# 闭包
## 闭包的含义
函数作为返回值  --高价函数除了可以接受函数作为参数外，还可以把函数作为结果值返回
闭包可以让开发者可以从内部函数访问外部函数的作用域，在JS中，闭包会随着函数的创建而被同时创建

## 运用init举例闭包
```
function init() {
  var name = "Mozilla"; // name 是一个被 init 创建的局部变量
  function displayName() { // displayName() 是内部函数，一个闭包
      alert(name); // 使用了父函数中声明的变量
  }
  displayName();
}
init();
```

### 解释
init() 创建了一个局部变量name和一个名为displayName() 的函数，dispalyName() 定义在init() 里的内部函数

并且仅在init() 函数体内可用。

注意：
displayName() 没有自己的局部变量，然而，因为它可以访问外部函数的变量，
所以displayName() 可以使用父函数 init() 中声明的变量name

## 运用makeAdder函数举例闭包
```
function makeAdder(x) {
  return function(y) {
    return x + y;
  };
}

var add5 = makeAdder(5);
var add10 = makeAdder(10);

console.log(add5(2));  // 7
console.log(add10(2)); // 12
```

### 解释
定义了一个函数makeAdder(x) 函数，它接受一个参数x，并返回一个新的函数。

返回的函数接受一个参数y,并返回x+y的值

从本质上讲，makeAdder 是一个函数工厂--它创建了将指定的值和它的参数相加求和的函数。

在上面，使用makeAdder--一个将参数和5求和，另一个和10求和。

add5 和 add10 都是闭包。它们共享相同的函数定义，但是保存了不同的词法环境。在 add5 的环境中，x 为 5。

而在 add10 中，x 则为 10














