# JS表单的常见提交方式
1. 通过形参的方式接收表单提交的数据(可以接收post与get提交的数据)。
2. 通过FormCollection来接收表单的数据（只能接收到post提交过来的数据）。
3. 通过 Request.Form[“name的属性值”]获取表单数据（只能接收到post提交过来的数据）。
4. EntityClass实体类接收数据（可以接收post与get提交的数据）。
``` javascript

submit提交：
     submit 按钮式提交

     onsubmit方式提交
提交

1
2
3
4
5
        function checkForm () {
                var input_pwd= document.getElementById('input_pwd');
                var md5_pwd= document.getElementById('md5_pwd');
                md5_pwd.value= toMD5(input_pwd.value);
            return true;
```