# Canvas
canvas 标签是 HTML5 新增的标签，可以使用 JavaScript 在此标签上绘制各种图形，canvas 相应的 DOM 元素有绘制路径、矩形、圆形、字符以及图片的方法，如下是绘制直线的一个示例：
``` javascript
    <style>
    * {
        margin: 0;
        padding: 0;
    }

    canvas {
        display: block;
        border: 1px solid black;
        box-shadow: 0 0 10px black;
        margin: 100px auto;
    }
</style>
<canvas></canvas>   <!-- 1. 创建 canvas 元素 -->
<script>
    /* 2. DOM 元素 */
    let oCanvas = document.querySelector("canvas");
    /* 3. 绘图工具 */
    let ctx = oCanvas.getContext("2d");
    /* 4. 绘图 */
    ctx.moveTo(50, 50); // 4.1 起始点
    ctx.lineTo(80, 50); // 4.2 终点
    ctx.stroke();       // 4.3 相连始末
</script>

```

+ canvas 标签默认是行内块级元素，且有默认的宽度（300px）以及高度（150px），此外，必须注意如下内容：

+ 不能通过 CSS 修改 canvas 元素的宽高（破坏内容），必须 canvas 标签的行内属性（width / height）
+ 默认情况下，canvas 标签中线条为 1px、黑色，而在实际情况下，默认将线条的中心点与像素的底部对齐，从而导致线条为 2px、灰色
