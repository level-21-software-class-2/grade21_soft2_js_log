# 过滤器
+ filter过滤器：用于把Array的某些元素过滤掉，返回剩下的元素。
1. 定义
filter()方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素。
2. 参数说明
filter()方法有两个参数，第一个是function()，第二个是thisValue。

function()：必须。数组中的每个元素都会执行这个函数。return后面判断结果，取布尔值，返回值是true则该元素被保留，是false则丢弃该元素。入参如下：

currentValue：必须。当前元素的值；
index：可选。当前元素的索引值；
arr：可选。当前元素属于的数组对象；
thisValue：可选。对象作为该执行回调时使用，传递给函数，用作 "this" 的值。如果省略了 thisValue ，"this" 的值为 "undefined"；

# every 
简而言之就是：它对数组中的每一项进行校验，只要有一项不通过它就是false。
```sql
let arr2 = [
    { value: "apple" },
    { value: "" },
    { value: "banana" },
    { value: "orange" },
    { value: "er" },
]
 
var res2 = arr2.every(item => {
    return item.value !== ""
})
console.log(res2);
```

# find 
在MDN中，find()方法返回数组中满足提供的测试函数的第一个元素的值。否则返回 undefined
注意：find()和上面两个不一样，它返回的是值，而且是第一个满足条件的值

``` sql
let arr3 = [
    { value: "" },
    { value: "" },
    { value: "" },
    { value: "" },
    { value: "apple" },
]
var res3 = arr3.find(item => {
    return item.value !== ""
})
console.log(res3);
```

