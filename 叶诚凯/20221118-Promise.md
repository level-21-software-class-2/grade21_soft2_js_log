# 最简单的Promise例子
```js
'use strict'
function fn(resolve,reject)
{
    let rnd =parseInt(Math.random()*2);
    if(rnd===0)
    {
        resolve();
    }else{
        reject();
    }
}
let p = new Promise(fn);
p.then(function(){
console.log('我成功了');
}).catch(function(){
console.log('我失败了');
})

```
变量p1是一个Promise对象，它负责执行test函数。由于test函数在内部是异步执行的，当test函数执行成功时，我们告诉Promise对象：

// 如果成功，执行这个函数：
p1.then(function (result) {
    console.log('成功：' + result);
});
当test函数执行失败时，我们告诉Promise对象：

p2.catch(function (reason) {
    console.log('失败：' + reason);
});
Promise对象可以串联起来，所以上述代码可以简化为：

new Promise(test).then(function (result) {
    console.log('成功：' + result);
}).catch(function (reason) {
    console.log('失败：' + reason);
});