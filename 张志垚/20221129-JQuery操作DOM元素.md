# 修改DOM结构
## 添加DOM
`JQuery.append()` 添加一个传入的 HTML 片段, 也可以传入 DOM 对象, JQuery对象和函数对象. 把对象添加到后面  
`JQuery.    end()` 和上面的方法类似, 但是它把对象添加到前面  

`JQuery.after()` 类似上面的 append 方法, 但 after 方法是添加到节点后面, 而 append 方法是添加到子节点里面  
`JQuery.before()` 类似 after 方法, 它将元素添加到节点的后面, prepend 方法是将元素添加到子节点里面  

## 删除DOM
`JQuery.remove()` 删除指定的对象, 如果对象有子节点, 则将子节点一并删除  