# Map
1. Map是一组键值对的结构，具有极快的查找速度。
2. 用Map实现，只需要一个“名字”-“成绩”的对照表，直接根据名字查找成绩，无论这个表有多大，查找速度都不会变慢。用JavaScript写一个Map如下：
```javascript
var m = new Map([['Michael', 95], ['Bob', 75], ['Tracy', 85]]);
m.get('Michael'); // 95
```
3. 初始化Map需要一个二维数组，或者直接初始化一个空Map。Map具有以下方法：
```javascript
var m = new Map(); // 空Map
m.set('Adam', 67); // 添加新的key-value
m.set('Bob', 59);
m.has('Adam'); // 是否存在key 'Adam': true
m.get('Adam'); // 67
m.delete('Adam'); // 删除key 'Adam'
m.get('Adam'); // undefined
```
4. 由于一个key只能对应一个value，所以，多次对一个key放入value，后面的值会把前面的值冲掉：
```javascript
var m = new Map();
m.set('Adam', 67);
m.set('Adam', 88);
m.get('Adam'); // 88
```

# Set
1. Set和Map类似，也是一组key的集合，但不存储value。由于key不能重复，所以，在Set中，没有重复的key。

2. 要创建一个Set，需要提供一个Array作为输入，或者直接创建一个空Set：
```javascript
var s1 = new Set(); // 空Set
var s2 = new Set([1, 2, 3]); // 含1, 2, 3
```
3. 重复元素在Set中自动被过滤：
```javascript
var s = new Set([1, 2, 3, 3, '3']);
s; // Set {1, 2, 3, "3"}
```
注意数字3和字符串'3'是不同的元素。

4. 通过add(key)方法可以添加元素到Set中，可以重复添加，但不会有效果：
```javascript
s.add(4);
s; // Set {1, 2, 3, 4}
s.add(4);
s; // 仍然是 Set {1, 2, 3, 4}
```
5. 通过delete(key)方法可以删除元素：
```javascript
var s = new Set([1, 2, 3]);
s; // Set {1, 2, 3}
s.delete(3);
s; // Set {1, 2}
```

# iterable
1. 为了统一集合类型，ES6标准引入了新的iterable类型，Array、Map和Set都属于iterable类型。
## for ... of
2. 具有iterable类型的集合可以通过新的for ... of循环来遍历。

```javascript
用for ... of循环遍历集合，用法如下：

var a = ['A', 'B', 'C'];
var s = new Set(['A', 'B', 'C']);
var m = new Map([[1, 'x'], [2, 'y'], [3, 'z']]);
for (var x of a) { // 遍历Array
    console.log(x);
}
for (var x of s) { // 遍历Set
    console.log(x);
}
for (var x of m) { // 遍历Map
    console.log(x[0] + '=' + x[1]);
}
```
### for ... of 与 for ... in 的区别
for ... in循环将把name包括在内，但Array的length属性却不包括在内。

for ... of循环则完全修复了这些问题，它只循环集合本身的元素

## forEach
更好的方式是直接使用iterable内置的forEach方法，它接收一个函数，每次迭代就自动回调该函数。

# 函数
## 定义函数
1. 在JavaScript中，定义函数的方式如下：
```javascript
function abs(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
```
上述abs()函数的定义如下：

* function指出这是一个函数定义；
* abs是函数的名称；
* (x)括号内列出函数的参数，多个参数以,分隔；
* { ... }之间的代码是函数体，可以包含若干语句，甚至可以没有任何语句。

2. 请注意，函数体内部的语句在执行时，一旦执行到return时，函数就执行完毕，并将结果返回。因此，函数内部通过条件判断和循环可以实现非常复杂的逻辑。

如果没有return语句，函数执行完毕后也会返回结果，只是结果为undefined。

3. 由于JavaScript的函数也是一个对象，上述定义的abs()函数实际上是一个函数对象，而函数名abs可以视为指向该函数的变量。

4. 因此，第二种定义函数的方式如下：
```javascript
var abs = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};
```
在这种方式下，function (x) { ... }是一个匿名函数，它没有函数名。但是，这个匿名函数赋值给了变量abs，所以，通过变量abs就可以调用该函数。

上述两种定义完全等价，注意第二种方式按照完整语法需要在函数体末尾加一个;，表示赋值语句结束。

## 函数的特性
函数是一种特殊的类，它的特性可以大概分为三类:

### 方法特性()
运行大括号包裹的代码；

### 对象特性
执行如f.bind()、f.call()之类的方法；

对象特性又可以分为自有属性和自定义属性。

#### 自有属性
主要有arguments、length、call方法、apply方法、bind方法、name、prototype、toString()等。
自有属性简单描述：
* arguments是接收实参返回一个数组，下图中返回null，是因为它存在，但没有内容，比如x，它不存在，所有返回undefined;

* length是形参的个数；

* name是返回函数的名称；

* toString()是返回函数的字符串形式；
#### 自定义属性
主要有:内部和外部的变量及方法的区别。

自定义属性：内外部有个优缺点
这种自定义属性有何用？比如你在函数中再写一个函数，很难执行，可以写成匿名函数，再赋值给自定义属性，这样就方便访问。

变量:

变量定义在内部会重新初始化，定义在外部又会被其他函数修改。根据实际业务使用。

3. 类的特性new: 可以做为构造函数。

## arguments
1. JavaScript还有一个免费赠送的关键字arguments，它只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数。arguments类似Array但它不是一个Array
2. 利用arguments，你可以获得调用者传入的所有参数。也就是说，即使函数不定义任何参数，还是可以拿到参数的值。
3. 实际上arguments最常用于判断传入参数的个数。你可能会看到这样的写法：
```javascript
// foo(a[, b], c)
// 接收2~3个参数，b是可选参数，如果只传2个参数，b默认为null：
function foo(a, b, c) {
    if (arguments.length === 2) {
        // 实际拿到的参数是a和b，c为undefined
        c = b; // 把b赋给c
        b = null; // b变为默认值
    }
    // ...
}
```
要把中间的参数b变为“可选”参数，就只能通过arguments判断，然后重新调整参数并赋值。

## rest
```javascript
function foo(a, b, ...rest) {
    console.log('a = ' + a);
    console.log('b = ' + b);
    console.log(rest);
}

foo(1, 2, 3, 4, 5);
// 结果:
// a = 1
// b = 2
// Array [ 3, 4, 5 ]

foo(1);
// 结果:
// a = 1
// b = undefined
// Array []
```
1. rest参数只能写在最后，前面用...标识，从运行结果可知，传入的参数先绑定a、b，多余的参数以数组形式交给变量rest，所以，不再需要arguments我们就获取了全部参数。

2. 如果传入的参数连正常定义的参数都没填满，也不要紧，rest参数会接收一个空数组（注意不是undefined）。

## return
注意：保证返回语句和return在同一行