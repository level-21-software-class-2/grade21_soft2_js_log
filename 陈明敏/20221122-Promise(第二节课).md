1. Promise最大的好处是在异步执行的流程中，把执行代码和处理结果的代码清晰地分离了
# 回调可能导致的问题
1. 代码不好看
2. 容易形成回调地狱
# Promise可以让异步任务达到三个方面的效果
1. 任务的顺序执行（串行执行）

有若干个异步任务，需要先做任务1，如果成功后再做任务2，任何任务失败则不再继续并执行错误处理函数。

要串行执行这样的异步任务，不用Promise需要写一层一层的嵌套代码。
```javascript
let p1 = new Promise(function(resolve,reject){
    console.log('p1任务开始了');
    setTimeout(resolve,1500,'p1正在执行');
});
let fn2=function(y){
    return new Promise(function(resolve,reject){
        console.log('p2任务开始了');
        setTimeout(resolve,500,y);
    });
}
let fn3=function(x){
    return new Promise(function(resolve,reject){
        console.log('p3任务开始了');
        setTimeout(resolve,500,x);
    });
}

p1.then(fn2).then(fn3).then(function(x){
    console.log(x);
}).catch(function(){
    console.log('三个任务，肯定有一个出错了');
})
```
```js
function multip(x){
    return new Promise(function(resolve,reject){
        let result=x*x;
        console.log(`这里进行乘法运算，传入的值是：${x}，返回的值是：${result}`);
        setInterval(resolve,800,result);
    })
}

function add(x){
    return new Promise(function(resolve,reject){
        let result=x+x;
        console.log(`这里进行加法运算，传入的值是：${x}，返回的值是：${result}`);
        setTimeout(resolve,500,result);
    })
}

let p= new Promise(function(resolve,reject){
    resolve(3);
})

p.then(add).then(multip).then(multip).then(function(x){
    console.log(x);
})
```
2. 任务的并行执行,两个任务是可以并行执行的,Promise.all()
```js
var p1 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 500, 'P1');
});
var p2 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 600, 'P2');
});
// 同时执行p1和p2，并在它们都完成后执行then:
Promise.all([p1, p2]).then(function (results) {
    console.log(results); // 获得一个Array: ['P1', 'P2']
});
```
3. 更好的保证任务的容错性,只需要获得先返回的结果即可,Promise.race()
```js
var p1 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 500, 'P1');
});
var p2 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 600, 'P2');
});
Promise.race([p1, p2]).then(function (result) {
    console.log(result); // 'P1'
});
```
由于p1执行较快，Promise的then()将获得结果'P1'。p2仍在继续执行，但执行结果将被丢弃。




