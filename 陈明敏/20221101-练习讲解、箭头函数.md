# 练习讲解

## 练习一

for循环中的定时器 观察以下代码，提出修改方案，要求按预期输出1、2、3：

* 方法一

```js
for(let i = 0; i < 10; i++) {
    setTimeout(function(){
        console.log(i);
    }, 1000*i);
}
```

* 方法二

```javascript
for(let i = 0; i < 10; i++) {   
    let fn = function (){
        console.log(i);
    }
    setTimeout(fn,1000*i);    
}
```

## 练习三

```js
/**
 * 实现栈结构
 */
function Stack() {
    this.items = [];
    this.push = item => {
        this.items.push(item);
    }
    this.pop = () => this.items.pop();
}
let stack = new Stack();

//经过测试发现问题。可以通过stack.items 直接访问和修改栈内的数据，请提出修改方案，要求不能直接访问items
```

* 方法一

```js
function StackNew(){
    let items = [];
    return {
        pop:function () {
            let x=items.pop();
            console.log(items);
            return x;
        },
        push:function (item) {
            items.push(item);
            console.log(items);
        }
    }
}
```

* 方法二

```js
function StackNew2(){
    let items= [];
    let res = [];
    
    res.push(function(){
        //实现pop的功能
        let x = items.pop();
        console.log(items);
        return x;
    });
    
    res.push(function(item){
        //实现push功能
        items.push(item);
        console.log(items);
    })
    
    return res;
}
```

### 栈结构

#### 一、栈结构是什么？
数组是一个线性结构，并且可以在数组的任意位置插入和删除元素。而栈就是比较常见的受限的线性结构。无论是插入或是删除只能在栈顶进行操作。在栈中插入操作被称为压栈或是入栈，删除操作被称为出栈或是退栈。
栈的特点为先进后出，后进先出（LIFO：last in first out）。

#### 二、常见的栈结构使用：函数调用栈、递归等。
函数调用栈：如A函数中调用了B函数，B函数中又调用了C函数，这样在调用A函数时先将A函数压入栈，由于A函数中调用了B函数，所以将B函数再压入栈，以此类推，c函数也压入栈。这时栈顶是C函数，只有C函数执行完出栈后，B函数才能执行出栈，最后A函数出栈。这就是函数调用栈。
递归：递归是在函数内部调用自身，原理和函数调用栈类似。

#### 三、栈的常见操作

- push（element）：添加一个新元素到栈顶位置；
- pop（）：移除栈顶的元素，同时返回被移除的元素；

- peek（）：返回栈顶的元素，不对栈做任何修改（该方法不会移除栈顶的元素，仅仅返回它）；

- isEmpty（）：如果栈里没有任何元素就返回true，否则返回false；

- size（）：返回栈里的元素个数。这个方法和数组的length属性类似；

- toString（）：将栈结构的内容以字符串的形式返回。
  

# 箭头函数

```js
x => x * x
```

上面的箭头函数相当于：

```js
function (x) {
    return x * x;
}
```

单行箭头函数

```js
let pp = x => x+x;
```

多行箭头函数

```js
let pp = (x) => {
    console.log(555);
    return x+x;
}
```

箭头函数相当于匿名函数，并且简化了函数定义。箭头函数有两种格式，一种像上面的，只包含一个表达式，连{ ... }和return都省略掉了。还有一种可以包含多条语句，这时候就不能省略{ ... }和return

# this

箭头函数看上去是匿名函数的一种简写，但实际上，箭头函数和匿名函数有个明显的区别：箭头函数内部的this是词法作用域，由上下文确定。

现在，箭头函数完全修复了this的指向，this总是指向词法作用域，也就是外层调用者obj：

由于this在箭头函数中已经按照词法作用域绑定了，所以，用call()或者apply()调用箭头函数时，无法对this进行绑定，即传入的第一个参数被忽略：