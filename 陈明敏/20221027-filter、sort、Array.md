# 过滤器 filter
1. 用于把Array的某些元素过滤掉，然后返回剩下的元素。
2. filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。
```javascript
let arr=[1,2,3,4,5,6,7,89,98];

let newArr = arr.filter(function(x){
    return x % 2 === 0;
})

console.log(newArr); //2,4,6,98
```

## 练习
一个数组，包含全国所有的省、自治区、直辖市
1. 请帮我找出所有华东地区的省和东北的省
2. 请帮我找简称是鄂的那个省
3. 请帮我找到首府是榕城或者是乌鲁木齐的省

```javascript
//数据
var arr=[{
    province:'河北省',
    abbreviation:'冀',
    procapital:'石家庄',
    area:'华北地区'
    
},{
    province:'山西省',
    abbreviation:'晋',
    procapital:'太原',
    area:'华北地区'
    
},{
    province:'辽宁省',
    abbreviation:'辽',
    procapital:'沈阳',
    area:'东北地区'
},{
    province:'吉林省',
    abbreviation:'吉',
    procapital:'长春',
    area:'东北地区'
},{
    province:'黑龙江省',
    abbreviation:'黑',
    procapital:'哈尔滨',
    area:'东北地区'

},{
    province:'江苏省',
    abbreviation:'苏',
    procapital:'南京',
    area:'华东地区'

},{
    province:'浙江省',
    abbreviation:'浙',
    procapital:'杭州',
    area:'华东地区'

},{
    province:'安徽省',
    abbreviation:'皖',
    procapital:'合肥',
    area:'华东地区'

},{
    province:'福建省',
    abbreviation:'闽',
    procapital:'福州',
    area:'华东地区'

},{
    province:'江西省',
    abbreviation:'赣',
    procapital:'南昌',
    area:'华东地区'

},{
    province:'山东省',
    abbreviation:'鲁',
    procapital:'济南',
    area:'华东地区'

},{
    province:'河南省',
    abbreviation:'豫',
    procapital:'郑州',
    area:'华中地区'

},{
    province:'湖北省',
    abbreviation:'鄂',
    procapital:'武汉',
    area:'华中地区'

},{
    province:'湖南省',
    abbreviation:'湘',
    procapital:'长沙',
    area:'华中地区'

},{
    province:'广东省',
    abbreviation:'粤',
    procapital:'广州',
    area:'华南地区'

},{
    province:'海南省',
    abbreviation:'琼',
    procapital:'海口',
    area:'华南地区'

},{
    province:'四川省',
    abbreviation:'川',
    procapital:'成都',
    area:'西南地区'

},{
    province:'贵州省',
    abbreviation:'贵',
    procapital:'贵阳',
    area:'西南地区'

},{
    province:'云南省',
    abbreviation:'云',
    procapital:'昆明',
    area:'西南地区'

},{
    province:'陕西省',
    abbreviation:'陕',
    procapital:'西安',
    area:'西北地区'

},{
    province:'甘肃省',
    abbreviation:'甘',
    procapital:'兰州',
    area:'西北地区'

},{
    province:'青海省',
    abbreviation:'青',
    procapital:'西宁',
    area:'西北地区'

},{
    province:'台湾省',
    abbreviation:'台',
    procapital:'台北',
    area:'华东地区'

},{
    province:'内蒙古自治区',
    abbreviation:'蒙',
    procapital:'呼和浩特',
    area:'华北地区'

},{
    province:'广西壮族自治区',
    abbreviation:'桂',
    procapital:'南宁',
    area:'华南地区'

},{
    province:'西藏自治区',
    abbreviation:'藏',
    procapital:'拉萨',
    area:'西南地区'

},{
    province:'宁夏回族自治区',
    abbreviation:'宁',
    procapital:'银川',
    area:'西北地区'

},{
    province:'新疆维吾尔自治区',
    abbreviation:'新',
    procapital:'乌鲁木齐',
    area:'西北地区'

},{
    province:'北京市',
    abbreviation:'京',
    procapital:'北京',
    area:'华北地区'

},{
    province:'天津市',
    abbreviation:'津',
    procapital:'天津',
    area:'华北地区'

},{
    province:'上海市',
    abbreviation:'沪',
    procapital:'上海',
    area:'华东地区'

},{
    province:'重庆市',
    abbreviation:'渝',
    procapital:'重庆',
    area:'西南地区'

},{
    province:'香港特别行政区',
    abbreviation:'港',
    procapital:'香港',
    area:'华南地区'

},{
    province:'澳门特别行政区',
    abbreviation:'澳',
    procapital:'澳门',
    area:'华南地区'

}]

//1.找出所有华东地区的省和东北的省
console.log(arr.filter(function(x){
    return x.area ==='华东地区' || x.area==='东北地区';
    
}))

//2.找简称是鄂的省
console.log(arr.filter(function(x){
    return x.abbreviation==='鄂';
}));


//3.找到首府是榕城或者是乌鲁木齐的省
console.log(arr.filter(function(x){
    return x.procapital==='福州'||x.procapital==='乌鲁木齐';
}));

```

# sort 排序算法
要按数字大小排序，我们可以这么写：

```javascript
var arr = [10, 20, 1, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return -1;
    }
    if (x > y) {
        return 1;
    }
    return 0;
});
console.log(arr); // [1, 2, 10, 20]
```
如果要倒序排序，我们可以把大的数放前面：
```javascript
var arr = [10, 20, 1, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return 1;
    }
    if (x > y) {
        return -1;
    }
    return 0;
}); // [20, 10, 2, 1]
```

sort()方法会直接对Array进行修改，它返回的结果仍是当前Array

# every

every()方法可以判断数组的所有元素是否满足测试条件。
```javascript
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.every(function (s) {
    return s.length > 0;
})); // true, 因为每个元素都满足s.length>0

console.log(arr.every(function (s) {
    return s.toLowerCase() === s;
})); // false, 因为不是每个元素都全部是小写
```

# find
find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined

# findIndex
findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1

# forEach
forEach()和map()类似，它也把每个元素依次作用于传入的函数，但不会返回新的数组。forEach()常用于遍历数组，因此，传入的函数不需要返回值