# HTML表单的输入控件

文本框，对应的`<input type="text">`，用于输入文本；

口令框，对应的`<input type="password">`，用于输入口令；

单选框，对应的`<input type="radio">`，用于选择一项；

复选框，对应的`<input type="checkbox">`，用于选择多项；

下拉框，对应的`<select>`，用于选择一项；

隐藏文本，对应的`<input type="hidden">`，用户不可见，但表单提交时会把隐藏文本发送到服务器。
```html
<input type="text" />文本输入框<br />
<input type="submit"  value="提交按钮" /><br />
<input type="reset" value="重置按钮" /><br />
<input type="radio" />单选按钮<br />
<input type="password" />密码输入框<br />
<input type="image" />图像<br />
<input type="hidden" />隐藏域<br />
<input type="file" />文件提交<br />
<input type="checkbox" />复选框<br />
<input type="button" />普通按钮<br />

<select>
<option>选择列表</option>
<optgroup><option>下拉子菜单</option></optgroup>
</select>
<textarea>文本区域</textarea>
<label>标签</label>
<fieldset>分组</fieldset>
<legend>描述元素，必填信息</legend>
```

`<form>` 定义供用户输入的表单
`<input>` 定义输入域
`<textarea>` 定义文本域（一个多行的输入控件）
`<label>` 定义了`<input>`元素的标签，一般为输入标题
`<fieldset>` 定义了`<fieldset>`元素的标题
`<select>` 定义了下拉选项列表
`<optgroup>` 定义选项组
`<option>`定义下拉列表中的选项
`<button>` 定义一个点击按钮
`<datalist>` 指定一个预先定义的输入控件选项列表
`<keygen>`定义了表单的密钥对生成器字段
`<output>` 定义一个计算结果


# 获取值
如果我们获得了一个`<input>`节点的引用，就可以直接调用value获得对应的用户输入值：

//` <input type="text" id="email">`
```js
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
这种方式可以应用于text、password、hidden以及select。但是，对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：
```html
// <label><input type="radio" name="weekday" id="monday" value="1"> Monday</label>
// <label><input type="radio" name="weekday" id="tuesday" value="2"> Tuesday</label>
```
```js
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
mon.value; // '1'
tue.value; // '2'
mon.checked; // true或者false
tue.checked; // true或者false
```

# 设置值
设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：

// `<input type="text" id="email">`
```js
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
对于单选框和复选框，设置checked为true或false即可。

#  HTML5控件
HTML5新增了大量标准控件，常用的包括date、datetime、datetime-local、color等，它们都使用`<input>`标签：
```html
<input type="date" value="2015-07-01">

2015/07/01
<input type="datetime-local" value="2015-07-01T02:03:04">

2015/07/01 02:03:04
<input type="color" value="#ff0000">
```
不支持HTML5的浏览器无法识别新的控件，会把它们当做type="text"来显示。支持HTML5的浏览器将获得格式化的字符串。例如，type="date"类型的input的value将保证是一个有效的YYYY-MM-DD格式的日期，或者空字符串。