## 什么是数组？
```
数组是一种特殊的变量，它能够一次存放一个以上的值。
var cars = ["Saab", "Volvo", "BMW"];
```
## 空格和折行并不重要。声明可横跨多行：
```sql
实例
var cars = [
    "Saab",
    "Volvo",
    "BMW"
];
```
## 使用 JavaScript 关键词 new
```sql
下面的例子也会创建数组，并为其赋值：

实例
var cars = new Array("Saab", "Volvo", "BMW");
```
## 访问数组元素
```sql
我们通过引用索引号（下标号）来引用某个数组元素。

这条语句访问 cars 中的首个元素的值：

var name = cars[0];
这条语句修改 cars 中的首个元素：

cars[0] = "Opel";
```
## 改变数组元素
```sql
这条语句修改了 cars 中第一个元素的值：

cars[0] = "Opel";
实例
var cars = ["Saab", "Volvo", "BMW"];
cars[0] = "Opel";
document.getElementById("demo").innerHTML = cars[0];
```
## 访问完整数组
```sql
通过 JavaScript，可通过引用数组名来访问完整数组：

实例
var cars = ["Saab", "Volvo", "BMW"];
document.getElementById("demo").innerHTML = cars; 
```
## length 属性
```
length 属性返回数组的长度（数组元素的数目）。

实例
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.length;   // fruits 的长度是 4
```
## 访问最后一个数组元素
```sql
实例
fruits = ["Banana", "Orange", "Apple", "Mango"];
var last = fruits[fruits.length - 1];
```
## 遍历数组元素
```sql
遍历数组的最安全方法是使用 "for" 循环：

实例
var fruits, text, fLen, i;

fruits = ["Banana", "Orange", "Apple", "Mango"];
fLen = fruits.length;
text = "<ul>";
for (i = 0; i < fLen; i++) {
     text += "<li>" + fruits[i] + "</li>";
} 
```
## 添加数组元素
```sql
向数组添加新元素的最佳方法是使用 push() 方法：

实例
var fruits = ["Banana", "Orange", "Apple", "Mango"];
fruits.push("Lemon");     // 向 fruits 添加一个新元素(Lemon)     
```sql
# 数组方法大全
- join()：返回值为用指定的分隔符将数组每一项拼接的字符串
- push()：向数组的末尾添加元素，返回值是当前数组的length(修改原数组)
- pop()：删除数组末尾的元素,返回值是被删除的元素（修改原数组）
- shift()：删除首位元素，返回值是被删除的元素（修改原数组）
- unshift()：向首位添加元素，返回值是数组的长度（修改原数组）
- slice()：返回数组中指定起始下标和结束下标之间的（不包含结束位置）元素组成的新数组
- splice()：对数组进行增删改（修改原数组）
- fill()：使用特定值填充数组中的一个或多个元素(修改原数组)
- filter()：过滤,数组中的每一项运行给定函数，返回满足过滤条件组成的数组
- concat()：用于连接两个或多个数组
- indexOf()：返回当前值在数组中第一次出现位置的索引
- lastIndexOf()：返回查找的字符串最后出现的位置，如果没有找到匹配字符串则返回 -1。
- every()：判断数组中每一项是否都符合条件
- some()：判断数组中是否存在满足的项
- includes()：判断一个数组是否包含指定的值
- sort()：对数字的元素进行排序(修改原数组)
- reverse()：对数组进行倒叙(修改原数组)
- forEach()：循环遍历数组每一项（没有返回值）
- map()：循环遍历数组的每一项（有返回值）
- copyWithin(): 从数组的指定位置拷贝元素到数组的另一个指定位置中（修改原数组）
- find(): 返回第一个匹配的值，并停止查找
- findIndex(): 返回第一个匹配值的索引，并停止查找
- toLocaleString()、toString():将数组转换为字符串
- flat()、flatMap()：扁平化数组
- entries() 、keys() 、values():遍历数组   