# 11.23笔记
# jQuery
jQuery可以将它当成一个超级大的函数。
## 使用jQuery
- 查找jQuery cdn, 引入即可。

![](./Imgs/jQuery2.png)
```JS
 <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
```
- 进入jQuery官网，选择jQuery-3.6.1.js(压缩或未压缩都可以)将链接另存为在自己的文件中。然后引用即可。

![](./Imgs/jQuery.png)
```JS
 <script src="./js/jquery-3.6.1.js"></script>
```
## $符号
它是变量jQuery的别名。
```JS
$ === jQuery; // true
```
## 选择器
选择器是jQuery的核心。

一个选择器写出来类似$('#dom-id')。
### 按ID查找
```JS
<!--HTML-->
<div id="AA">jj</div>

 let aa = $('#AA')
console.log(AA); 
```
### 按tag查找
按tag查找只需要写上tag名称即可
```JS
<!--HTML-->
   <div id="AA">
        <div class="aa">
            今天学jQuery
        </div>
    </div>

let div =$('#div')
```
### 按class查找
按class查找注意在class名称前加一个.。
```JS
<!--HTML-->
   <div id="AA">
        <div class="aa">
            今天学jQuery
        </div>
    </div>

let aa = $('.aa')
console.log(aa)
```
### 按属性查找
一个DOM节点除了id和class外还可以有很多属性，很多时候按属性查找会非常方便.
```JS
<!--HTML-->
   <div id="AA" class="woman" name="Jane" nice="good">
        今天学jQuery
    </div>

let Jane = $([nice="good"])
console.log(Jane)
```
## 组合查找 
组合查找就是把选择器组合起来使用。
```JS
let tr = $('tr.red'); // 找出<tr class="red ...">...</tr>
```
.：并且

，：或者
## 多项选择器
多项选择器就是把多个选择器用,组合起来一块选。
```JS
$('p,div'); // 把<p>和<div>都选出来
$('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来
```
