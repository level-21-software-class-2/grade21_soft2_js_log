# 10.19笔记
# 定义函数
```JS
第一种：
function fn(x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
}
第二种：
var fn = function (x) {
    if (x >= 0) {
        return x;
    } else {
        return -x;
    }
};
```
# 调用函数
**调用函数时，按顺序传入参数即可**
```JS
fn(10); // 返回10
fn(-9); // 返回9
```
# arguments
**利用arguments，只在函数内部起作用，并且永远指向当前函数的调用者传入的所有参数。可以获得调用者传入的所有参数。即使函数不定义任何参数，还是可以拿到参数的值**
```JS
function foo(x) {
    console.log('x = ' + x); // 10
    for (var i=0; i<arguments.length; i++) {
        console.log('arg ' + i + ' = ' + arguments[i]); // 10, 20, 30
    }
}
foo(10, 20, 30);
```
```JS
function fn(){
    console.log(arguments[1]);
}
fn('sfdsf', 'aaaa', 'bbbb')//aaaa
```
# rest参数
**获取除了已定义参数之外的参数。rest参数只能写在最后，前面用...标识**
```JS
function fn(a,b, ...test){
    console.log('测试');
    console.log(test);
}
fn(1, 2, 3)//[3]
```