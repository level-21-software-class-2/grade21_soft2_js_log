# 10.14笔记
# 数组
**要取得Array的长度，直接访问length属性**
```JavaScript
var arr = [1, 2, 3.14, 'Hello', null, true];
arr.length; // 6
```
- 直接给数组的length赋一个新的值会导致数组大小的变化：
```JavaScript
var arr = [1, 2, 3];
arr.length; // 3
arr.length = 6;
arr; // arr变为[1, 2, 3, undefined, undefined, undefined]
arr.length = 2;
arr; // arr变为[1, 2]
```
数组可以通过索引把对应的元素修改为新的值，因此，对数组的索引进行赋值会直接修改这个数组
```JavaScript
var arr = ['A', 'B', 'C'];
arr[1] = 99;
arr; // arr现在变为['A', 99, 'C']
```
- 如果通过索引赋值时，索引超过了范围，同样会引起Array大小的变化：
```JavaScript
var arr = [1, 2, 3];
arr[5] = 'x';
arr; // arr变为[1, 2, 3, undefined, undefined, 'x']
```
## 1. indexOf
**与String类似，数组也可以通过indexOf()来搜索一个指定的元素的位置**
```JavaScript
var arr = [10, 20, '30', 'xyz'];
arr.indexOf(10); // 元素10的索引为0
arr.indexOf(20); // 元素20的索引为1
arr.indexOf(30); // 元素30没有找到，返回-1
arr.indexOf('30'); // 元素'30'的索引为2
```
## 2. includes
**是否包含某个字符或字符串，只返回布尔值**
```JavaScript
var arr = [1,2,3,4,5]
var x = arr.includes(1)//是否包含1
console.log(x)
```
## 3. findindex
**findindex函数接收一个函数f,这个函数将应用数据中的每一个元素当中，如果f在某个元素上的处理结果返回true，那么就直接返回当前元素的下标**
```JavaScript
 var arr = [1,2,3,4,5]
    console.log(arr)
    console.log(arr.findIndex(function(val){
        return val === 3
    }))
```
## 4. slice
**截取数组的部分元素，然后返回一个新的数组**
```JavaScript
 var arr = [1,2,3,4,5]
console.log(arr)
var x = arr.slice(0,2)//(开始的位置，截取的长度)
console.log(x)
```
## 5. push和pop
**push()向数组的末尾添加元素，pop()则把数组的最后一个元素删除掉**
```JavaScript
 var arr = ['小王']
 console.log(arr)
arr.push('小李')
console.log(arr)
var x = arr.pop()
console.log(x)
```
## 6. unshift和shift
**如果要往数组的头部添加元素，使用unshift()方法，shift()方法则把数组的第一个元素删掉**
```JavaScript
var arr = ['小王']
console.log(arr)
arr.unshift('小李')
console.log(arr)
var x = arr.shift()
console.log(x)
```
## 7. sort
**sort()可以对当前数组进行排序**
```JavaScript
var arr = [1,2,3,4,5,8,6,7,9,10,11,22]
console.log(arr)
var x = arr.sort()
console.log(x)
```
排序后的数组是这样：
![图裂了](./Imgs/%E6%8E%92%E5%BA%8F%E9%97%AE%E9%A2%98.png)
可以用这个方法解决:
```JavaScript
var arr = [1,2,3,4,5,8,6,7,9,10,11,22]
console.log(arr)
var x = arr.sort(function(a,b){
    return a-b;
    })
console.log(x)
```
就可以了:
![图裂了](./Imgs/%E6%8E%92%E5%BA%8F%E9%97%AE%E9%A2%98%E8%A7%A3%E5%86%B3.png)
## 8.reverse
**reverse()把整个数组的元素反转**
```JavaScript
var arr = [1,2,3,4,5,8,6,7,9,10,11,22]
console.log(arr)
var x = arr.reverse()
console.log(x)
```
## 9. splice
**splice()方法是修改数值的“万能方法”，它可以从指定的索引开始删除元素，然后再从该位置添加元素**
```JavaScript
删除元素：
var arr = [1,2,3,4,5]
console.log(arr)
var x = arr.splice(0,3)//（开始的位置，截取的长度）
console.log(x)
添加元素：
var arr = [1,2,3,4,5]
console.log(arr)
var x = arr.splice(0,0,'2','4')
console.log(arr)//注意：添加的元素在原数组，并不会返回新数组
```
## 10. concat
**concat()方法把当前的数组和另一个数组连接起来，并返回一个新的数组**
```JavaScript
****
    var arr = [1,2,3,4,5]
    console.log(arr)
    var x = arr.concat(1)
    console.log(x)
```
## 11. join
**join()方法是一个非常实用的方法，它把当前数组的每个元素都用指定的字符串连接起来，然后返回连接后的字符串**
```JavaScript
   var arr = [1,2,3,4,5]
   console.log(arr)
   var x = arr.join(',')
   console.log(x)
```
