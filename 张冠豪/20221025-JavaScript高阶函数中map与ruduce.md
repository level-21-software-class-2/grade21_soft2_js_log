# 10.25笔记
# JavaScript高阶函数
## 概念
**一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数**
## map
- map()是将传入的函数依次作用到序列的每个元素，每个元素都是独自被函数“作用”一次

-   map函数的第一个参数是函数，函数的参数可以是1个或者多个
```JS
let arr = [1,2,3,4,5,6]
funicon f(x){
    return x+x
}
let newArr = arr.map(f)
```
利用map将数字转成字符串
```JS
let arr = [1, 2, 3, 4, 5, 6];
arr.map(String); 
// ['1', '2', '3', '4', '5', '6', '7',8','9']
```
## reduce
- reduce()是将传入的函数作用在序列的第一个元素得到结果后，把这个结果继续与下一个元素作用（累积计算）
```JS
[x1, x2, x3, x4].reduce(f) = f(f(f(x1, x2), x3), x4)

举个例子，方便理解：
let arr = [1,2,3,4]
function f(x,y){
    return x*y
}
let newArr = arr.reduce(f)
console.log(newArr)
//24
```
- reduce只能接受2个参数

