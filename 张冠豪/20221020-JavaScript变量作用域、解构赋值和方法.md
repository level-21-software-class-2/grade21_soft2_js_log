# 10.20笔记
# 变量作用域
**如果一个变量在函数体内部申明，则该变量的作用域为整个函数体，在函数体外不可引用该变量**
```JS
'use strict';

function foo() {
    var x = 1;
    x = x + 1;
}

x = x + 2; // ReferenceError! 无法在函数体外引用变量x
```
**如果两个不同的函数各自申明了同一个变量，那么该变量只在各自的函数体内起作用。换句话说，不同函数内部的同名变量互相独立，互不影响**
```JS
'use strict';

function foo() {
    var x = 1;
    x = x + 1;
}

function bar() {
    var x = 'A';
    x = x + 'B';
}
```
**由于JavaScript的函数可以嵌套，此时，内部函数可以访问外部函数定义的变量，反过来则不行**
```JS
'use strict';

function foo() {
    var x = 1;
    function bar() {
        var y = x + 1; // bar可以访问foo的变量x!
    }
    var z = y + 1; // ReferenceError! foo不可以访问bar的变量y!
}
```
**内部可以访问外部，机制：函数使用变量，遵循就近原则，首先在内部查找变量，如果没有找到，则继续向外查找，如果还没有找到，则继续到全局变量window查找，最终没有找到的话，报未定义错误**
```JS
function foo() {
    var x = 1;
    function bar() {
        var x = 'A';
        console.log('x in bar() = ' + x); // 'A'
    }
    console.log('x in foo() = ' + x); // 1
    bar();
}

foo();


x in foo() = 1
x in bar() = A
```

# 变量提升
**JavaScript的函数定义有个特点，它会先扫描整个函数体的语句，把所有申明的变量“提升”到函数顶部**
```JS
'use strict';


function foo() {
    var x = 'Hello, ' + y;
    console.log(x);
    var y = 'Bob';
}

foo();


Hello, undefined
```
虽然是strict模式，但语句var x = 'Hello, ' + y;并不报错，原因是变量y在稍后申明了。但是console.log显示Hello, undefined，说明变量y的值为undefined。这正是因为JavaScript引擎自动提升了变量y的声明，但不会提升变量y的赋值

**由于JavaScript的这一怪异的“特性”，我们在函数内部定义变量时，请严格遵守“在函数内部首先申明所有变量”这一规则。最常见的做法是用一个var申明函数内部用到的所有变量
把需要用到的变量在开始先定义好**
# 全局作用域
**JavaScript默认有一个全局对象window，全局作用域的变量实际上被绑定到window的一个属性。window并不是专有名词**
```JS
'use strict';

var course = 'Learn JavaScript';
alert(course); // 'Learn JavaScript'
alert(window.course); // 'Learn JavaScript'
```
直接访问全局变量course和访问window.course是完全一样的

**由于函数定义有两种方式，以变量方式var foo = function () {}定义的函数实际上也是一个全局变量，因此，顶层函数的定义也被视为一个全局变量，并绑定到window对象**
```JS
'use strict';

function foo() {
    alert('foo');
}

foo(); // 直接调用foo()
window.foo(); // 通过window.foo()调用
```
**因此，直接调用的alert()函数其实也是window的一个变量**
```JS
use strict';

window.alert('调用window.alert()');
// 把alert保存到另一个变量:
var old_alert = window.alert;
// 给alert赋一个新函数:
window.alert = function () {}
```
**说明JavaScript实际上只有一个全局作用域。任何变量（函数也视为变量），如果没有在当前函数作用域中找到，就会继续往上查找，最后如果在全局作用域中也没有找到，则报ReferenceError错误**
# 名字空间
**全局变量会绑定到window上，不同的JavaScript文件如果使用了相同的全局变量，或者定义了相同名字的顶层函数，都会造成命名冲突，并且很难被发现**

**减少冲突的一个方法是把自己的所有变量和函数全部绑定到一个全局变量中**

```JS
// 唯一的全局变量MYAPP:
var MYAPP = {};

// 其他变量:
MYAPP.name = 'myapp';
MYAPP.version = 1.0;

// 其他函数:
MYAPP.foo = function () {
    return 'foo';
};
```
**把自己的代码全部放入唯一的名字空间MYAPP中，会大大减少全局变量冲突的可能**
# 局部作用域
**var不具有块级作用域**

**为了解决块级作用域，ES6引入了新的关键字let，用let替代var可以申明一个块级作用域的变量**

**能用let就用let定义变量**
# 常量
**常量始终不变**

**我们通常用全部大写的变量来表示“这是一个常量，不要修改它的值”**

**ES6标准引入了新的关键字const来定义常量，const与let都具有块级作用域**
# 解构赋值
**从ES6开始，JavaScript引入了解构赋值，可以同时对一组变量进行赋值**

**传统做法**
```javascript
var array = ['hello', 'JavaScript', 'ES6'];
var x = array[0];
var y = array[1];
var z = array[2];
```
**现在可以使用解构赋值，直接对多个变量同时赋值**
```javascript
let arr = ['hello','JavaScript','ES6']；
let [a,b,c,d] = arr;
console.log(a);//hello
console.log(b);//JavaScript
console.log(c);//ES6
```
**如果数组本身还有嵌套，也可以通过下面的形式进行解构赋值，注意嵌套层次和位置要保持一致**
```JS
let [x, [y, z]] = ['hello', ['JavaScript', 'ES6']];
x; // 'hello'
y; // 'JavaScript'
z; // 'ES6'
```
**解构赋值还可以忽略某些元素**
```JS
let [, , z] = ['hello', 'JavaScript', 'ES6']; // 忽略前两个元素，只对z赋值第三个元素
z; // 'ES6'
```
**如果需要从一个对象中取出若干属性，也可以使用解构赋值，便于快速获取对象的指定属性**
```JS
'use strict';

var person = {
    name: '小明',
    age: 20,
    gender: 'male',
    passport: 'G-12345678',
    school: 'No.4 middle school'
};
var {name, age, passport} = person;
```
**对一个对象进行解构赋值时，同样可以直接对嵌套的对象属性进行赋值，只要保证对应的层次是一致的**
```JS
var person = {
    name: '小明',
    age: 20,
    gender: 'male',
    passport: 'G-12345678',
    school: 'No.4 middle school',
    address: {
        city: 'Beijing',
        street: 'No.1 Road',
        zipcode: '100001'
    }
};
var {name, address: {city, zip}} = person;
name; // '小明'
city; // 'Beijing'
zip; // undefined, 因为属性名是zipcode而不是zip
// 注意: address不是变量，而是为了让city和zip获得嵌套的address对象的属性:
address; // Uncaught ReferenceError: address is not defined
```
**使用解构赋值对对象属性进行赋值时，如果对应的属性不存在，变量将被赋值为undefined，这和引用一个不存在的属性获得undefined是一致的。如果要使用的变量名和属性名不一致，可以用下面的语法获取**
```JS
var person = {
    name: '小明',
    age: 20,
    gender: 'male',
    passport: 'G-12345678',
    school: 'No.4 middle school'
};

// 把passport属性赋值给变量id:
let {name, passport:id} = person;
name; // '小明'
id; // 'G-12345678'
// 注意: passport不是变量，而是为了让变量id获得passport属性:
passport; // Uncaught ReferenceError: passport is not defined
```
**解构赋值还可以使用默认值，这样就避免了不存在的属性返回undefined的问题**
```javascript
var person = {
    name: '小明',
    age: 20,
    gender: 'male',
    passport: 'G-12345678'
};

// 如果person对象没有single属性，默认赋值为true:
var {name, single=true} = person;
name; // '小明'
single; // true
```
**有些时候，如果变量已经被声明了，再次赋值的时候，正确的写法也会报语法错误**
```JS
// 声明变量:
var x, y;
// 解构赋值:
{x, y} = { name: '小明', x: 100, y: 200};
// 语法错误: Uncaught SyntaxError: Unexpected token =
```
**这是因为JavaScript引擎把{开头的语句当作了块处理，于是=不再合法。解决方法是用小括号括起来**
```JS
({x, y} = { name: '小明', x: 100, y: 200});
```
# 使用场景
**快速获取当前页面的域名和路径**
```JS
var {hostname:domain, pathname:path} = location;
```