# 11.18笔记
# Promise
因为js代码都是单线程的，所以js所有操作都是异步执行

由于把回调函数写到AJAX操作里不利于代码复用，所以我们用到Promise

示例：生成一个0-2之间的随机数，如果小于1，则等待一段时间后返回成功，否则返回失败：
```JS   
function test(resolve, reject) {
let timeOut = Math.random() * 2;
    log('set timeout to: ' + timeOut + ' seconds.');
    setTimeout(function () {
        if (timeOut < 1) {
            log('call resolve()...');
            resolve('200 OK');
        }
        else {
            log('call reject()...');
            reject('timeout in ' + timeOut + ' seconds.');
        }
    }, timeOut * 1000);
}
```
定义一个Promise对象:
```JS
let p1 = new promise(test);
p1.then(function (res)){
    console.log('成功');
}.catch(function(err)){
    console.log('失败');
}
```
如果有若干个异步执行，要**串行**这些异步任务：
先做任务1，如果成功后再做任务2,以此类推。任何任务失败则不再继续并执行错误处理函数
```JS
p1.then(p2).then(p3).catch(handleError)
```

Promise还可以**并行**执行异步任务：
```JS
let p1 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 500, 'P1');
});
let p2 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 600, 'P2');
});
// 同时执行p1和p2，并在它们都完成后执行then:
Promise.all([p1, p2]).then(function (results) {
    console.log(results); // 获得一个Array: ['P1', 'P2']
});
```