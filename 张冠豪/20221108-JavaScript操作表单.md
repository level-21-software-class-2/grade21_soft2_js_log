# 11.8笔记
# 操作表单
用JavaScript操作表单和操作DOM是类似的，因为表单本身也是DOM树

HTML表单的输入控件主要有以下几种：
```js
 文本框，对应的<input type="text">，用于输入文本

 口令框，对应的<input type="password">，用于输入口令

 单选框，对应的<input type="radio">

 复选框，对应的<input type="checkbox">，用于选择多项

 下拉框，对应的<select>，用于选择一项

 隐藏文本，对应的<input type="hidden">，用户不可见，但表单提交时会把隐藏文本发送到服务器
```
## 获取值

如果我们获得了一个< input >节点的引用，对于text、password、hidden以及select可以直接调用value获得对应的用户输入值：

```JS
// <input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
这种方式可以应用于text、password、hidden以及select。但是，对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：
```JS
// <label><input type="radio" name="weekday" id="monday" value="1"> Monday</label>
// <label><input type="radio" name="weekday" id="tuesday" value="2"> Tuesday</label>
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
mon.value; // '1'
tue.value; // '2'
mon.checked; // true或者false
tue.checked; // true或者false
```
## 设置值

设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：
```JS
// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
对于单选框和复选框，设置checked为true或false即可

## HTML5控件

HTML5新增了大量标准控件，常用的包括date、datetime、datetime-local、color等
```JS
多行文本框:
<textarea name="" id="" cols="30" rows="10"></textarea>

出生日期：
<input type="date">

活动日期：
<input type="datetime">
```

## 提交表单

表单提交的两种常见方式：

1. form的原始提交，即给表单的action指定的提交路径，然后将按钮的类型修改为submit类型，当点击这个按钮的时候，表单就会被提交
```JS
<!-- HTML -->
<form id="test-form">
    <input type="text" name="test">
    <button type="button" onclick="doSubmitForm()">Submit</button>
</form>

<script>
function doSubmitForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 提交form:
    form.submit();
}
</script>
```
2. 利用js函数进行提交，即去掉表单的action路径，然后将按钮的类型修改为普通的button，设置按钮的onclik事件绑定一个函数（或者若干个），则当点击这个按钮的时候，事件被触发，时间就会执行绑定的函数
```JS
<!-- HTML -->
<form id="test-form" onsubmit="return checkForm()">
    <input type="text" name="test">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    return true;
}
</script>   
```