# 11.24笔记
# 操作DOM
### 修改text和html
```JS
text();//修改文本内容，无参数获取文本
html();//修改标签，无参数获取带标签的文本
```
## 修改css
```JS
css();//获取css属性
css('color','red')//设置属性
css('color','')//清除属性
addclass();//增加class属性，不覆盖原有
```
## 显示和隐藏DOM
```JS
hide();//隐藏
show();//显示
```