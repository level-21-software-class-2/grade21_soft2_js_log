# 10.18笔记
# 对象
**JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成**

**JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开**
```JavaScript
var arr = {
    name: '小明',
    ago: 18,
    sex: '男'，
    school: 'No.1 Middle School',
    height: 1.70,
    weight: 65,
};
```
**获取属性，访问属性是通过.操作符完成**
```JavaScript
arr.name;//'小明'
```
**如果属性名包含特殊字符，就必须用''括起来**
```JavaScript
var xiaohong = {
    name: '小明',
    'middle-school': 'No.1 Middle School'
};
```
**当属性名不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问**
```JavaScript
arr['middle-school']; // 'No.1 Middle School'
arr['name']; // '小明'
arr.name; // '小明'
```
**访问一个不存在的属性，并不会不报错，而是返回undefined**
**如果我们要检测arr是否拥有某一属性，可以用in操作符。返回的是布尔值**
```JavaScript
var arr = {
    name: '小明',
    ago: 18,
    sex: '男'，
    school: 'No.1 Middle School',
    height: 1.70,
    weight: 65,
};
'name' in arr; // true
'grade' in arr; // false
```
# 判断
## 条件判断
**JavaScript使用if () { ... } else { ... }来进行条件判断**
```JavaScript
var age = 20;
if (age >= 18) { // 如果age >= 18为true，则执行if语句块
    alert('adult');
} else { // 否则执行else语句块
    alert('teenager');
}
```
## 多行条件判断
**多个if...else...的组合，进行更加细致地判断条件**

**如果符合第一个条件，后面就不会执行**
```JavaScript
var age = 3;
if (age >= 18) {
    alert('adult');
} else if (age >= 6) {
    alert('teenager');
} else {
    alert('kid');
}
```
# 循环
## for循环
**通过初始条件、结束条件和递增条件来循环执行语句块**

**for循环最常用的地方是利用索引来遍历数组**
```JavaScript
var stu={
    name:小明，
    age：18
}
for(var item in stu){
    console.log(item);//name,age
    console.log(stu[item]);//小明，18   
}
```
## while循环
**只有一个判断条件，条件满足，就不断循环，条件不满足时则退出循环**

**比如我们要计算100以内所有奇数之和**
```JavaScript
var x = 0;
var n = 99;
while (n > 0) {
    x = x + n;
    n = n - 2;
}
x; // 2500
```