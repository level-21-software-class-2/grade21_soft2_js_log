# 10.13笔记
# 字符串
- 双引号可以包含单引号，单引号也可以包含双引号
- 双引号中如果想要包含双引号，需要使用转义字符"\"(单引号也一样)
## 多行字符串
**``**

```JavaScript
var x =`
        这是一个
        多行
        字符串
       `
console.log(x)
```

## 模板字符串
**${}**

```JavaScript
var z = '不是'
var x =`
        这是一个${z}
        多行
        字符串
       `
console.log(x)
```
## 两个方法
- toUpperCase

toUpperCase()把一个字符串全部变为大写：

```JavaScript
var s = 'Hello';
s.toUpperCase(); // 返回'HELLO'
```
- toLowerCase

toLowerCase()把一个字符串全部变为小写：

```JavaScript
var s = 'Hello';
var lower = s.toLowerCase(); // 返回'hello'并赋值给变量lower
lower; // 'hello'
```
## indexOf
`indexOf("指定字符"[,开始查找的位置])    string`

**返回指定字符在字符串中第一次出现处的索引**

## lastdexOf
`lastIndexOf("指定字符"[,开始查找的位置])   string`

**从字符的最后面开始查找，返回指定字符的下标**

## substring
`substring("开始查找的位置", "结束查找的位置")  string`

**substring(2,4):表示从下标为2的字符开始显示，但不显示下标为4的字符**

# 两个问题
1. 利用 indexOf() 和 lastIndexOf 判断字符或字符串是否重复出现

![图裂了](./Imgs/%E5%88%A4%E6%96%AD%E6%98%AF%E5%90%A6%E9%87%8D%E5%A4%8D.png)

2. 写一段js语句， 实现一个效果，输入一个数字(长度不定)，返回大写的汉字数字大写，如给定一个1，返回"壹", 给定329，返回"叁贰玖"

![图裂了](./Imgs/%E8%BF%94%E5%9B%9E%E5%A4%A7%E5%86%99.png)