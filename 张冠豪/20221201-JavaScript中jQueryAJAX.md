# 12.1笔记
# AJAX
### ajax
jQuery在全局对象jQuery（也就是$）绑定了ajax()函数，可以处理AJAX请求。ajax(url, settings)函数需要接收一个URL和一个可选的settings对象
## 常用的选项如下：
### async
是否异步执行AJAX请求，默认为true，千万不要指定为false

### method
发送的Method，缺省为'GET'，可指定为'POST'、'PUT'等

### contentType
发送POST请求的格式，默认值为'application/x-www-form-urlencoded; charset=UTF-8'，也可以指定为text/plain、application/json

### data
发送的数据，可以是字符串、数组或object。如果是GET请求，data将被转换成query附加到URL上，如果是POST请求，根据contentType把data序列化成合适的格式

### headers
发送的额外的HTTP头，必须是一个object；

### dataType
接收的数据格式，可以指定为'html'、'xml'、'json'、'text'等，缺省情况下根据响应的Content-Type猜测

### GET
GET 用于从指定资源请求数据

### POST
POST 用于将数据发送到服务器来创建/更新资源

- POST 请求不会被缓存
- POST 请求不会保留在浏览器历史记录中
- POST 不能被收藏为书签
- POST 请求对数据长度没有要求
### getJSON
getJSON() 方法使用 AJAX 的 HTTP GET 请求获取 JSON 数据
```JS
$(selector).getJSON(url,data,success(data,status,xhr))
```