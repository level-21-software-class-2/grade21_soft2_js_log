# 10.27
# filter(过滤器)
 **filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素**
 ```JS
去掉数组中的偶数：
let arr = [1, 2, 3,4,5,6,7,8,9];
let newArr = arr.filter(function (x) {
    return x % 2 !== 0;
});
console.log(newArr); 
// [1，3，5，7，9]
 ```
# 回调函数
**filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身**
 ```JS
let arr = ['a','s','d']

let newArr = arr.filter(function(element,index,self){
    console.log(element)//依次打印'a', 's', 'd'
    console.log(index)// 依次打印0, 1, 2
    console.log(self)// self就是变量arr
    return true
})
/*
 a
 0
 ['a', 's', 'd']
 s
 1
 ['a', 's', 'd']
 d
 2
 ['a', 's', 'd']
*/
```
# sort(排序)
**对于两个元素x和y，如果认为x < y，则返回-1，如果认为x == y，则返回0，如果认为x > y，则返回1，这样，排序算法就不用关心具体的比较过程，而是根据比较结果直接排序**

**sort()方法也是一个高阶函数，它还可以接收一个比较函数来实现自定义的排序**
```JS
按数字大小排序：
let arr = [1,2,3,4,5,6,12,34,56]

console.log(arr.sort(function(x,y){
    if(x>y){
        return -1
    }
    if(x<y){
        return 1
    }
    if(x===y){
        return 0
    }
}))
```
# Array
**对于数组，除了map()、reduce、filter()、sort()这些方法可以传入一个函数外，Array对象还提供了很多非常实用的高阶函数**

## every
**every()方法可以判断数组的所有元素是否满足测试条件**
```JS
判断是否都是数字：
console.log(allIsNamber)

let IsOk = arr.every(function(x){
    return typeof(x) ==="number"
})
console.log(IsOk)
//false
```
## find
**find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined**
```JS
找出大于6的数：
let arr = [1,2,3,'4',5,6,'7',8,9]

let newArr = arr.find(function(x){
    return x>6
})
console.log(newArr)
```
## findIndex
**findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引（下标），如果没有找到，返回-1**
```JS
找出大于9的数：
let arr = [1,2,3,'4',5,6,'7',8,9]

let newArr = arr.findIndex(function(x){
    return x>9
})
console.log(newArr)
//-1
```
## forEach
**forEach()和map()类似，它也把每个元素依次作用于传入的函数，但不会返回新的数组。forEach()常用于遍历数组，因此，传入的函数不需要返回值**
```JS
判断是否都是数字：
let arr = [1,2,3,'4',5,6,'7',8,9]
let allIsNamber = true
arr.forEach(x =>{
    if(typeof(x) !=='number'){
        allIsNamber = false;
        return
    }
})
//false
```
