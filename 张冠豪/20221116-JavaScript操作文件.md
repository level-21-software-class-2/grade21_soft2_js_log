# 11.16笔记
# 操作文件

在表单中上传文件的控件是：
```JS
<input type='file'>
```
预览图片：
```JS
获取控件<input type="file"><img>的id
let file = document.getElementById("file");
let photo = document.getElementById("photo");

file.addEventListener('change',function(){
    //获取文件
    let newfile = file.files[0];
    //读取文件
    let reader = new FileReader();
    reader.onload = function(x){
        let data = x.target.result;
        photo.style.backgroundImage=`url(${data})`;
    }
    reader.readAsDataURL(newfile);
})
```
