# 10.28笔记
# 闭包
## 概念
函数作为返回值

高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回
```JS
function fn1(){
    function fn(){

    }
    return fn;
}
```
## 理解闭包
- 闭包是一个函数
- 闭包是一个被函数返回的函数
- 被函数A返回的函数B，并且B函数使用了A函数的参数或者局部变量，我们称之为闭包
```JS
function add(x){
    return x;
}
function lazy_add(x){
    let add=function (){
        return x;
    }
    return add;
}
let fn= lazy_add(8);
console.log(fn());
```

## 两个看起来一模一样的函数其实并不一样
**函数和函数的相等性，就是指这两个函数在内存中的地址是否一样，换句话说，就是这两个函数是否是同一个内存地址，在换句话说，这些变量是否指向同一个地址，如果是，我们说这两个函数相等，否则不相等**

**借助闭包，可以封装一个私有变量。我们用JavaScript创建一个计数器：**
```JS
function counter(initNum){
    let num = initNum || 0;
    let obj={
        increment : function (){
            console.log('打印在加1前：${num}');
            num+ = 1;
            console.log('打印在加1前：${num}');
        }
    }
    return obj;
}
```
**返回的对象，封装了一个变量num，这个变量从此孤独终老，他没有被任何外部办法，函数，任何手段直接调用，只能由这个对象的increment这个属性的对应的方法调用**