# 11.30笔记
## 修改DOM结构
### 添加DOM
append()
```JS
"use strict"
let ul = $('ul')

//字符串
// let li = '<li>哈哈哈哈</li>' 

//DOM节点（自己定义）
// let li = document.createElement('li')
// li.textContent = '嘿嘿嘿嘿'    

//DOM节点（直接获取）
// let li = document.getElementsByTagName('li')[0]

let  arr = ['哈哈哈哈','嘿嘿嘿嘿','吼吼吼吼','呵呵呵呵','桀桀桀桀']
let rndIndex = Math.floor(Math.random() *arr.length);
let li = `<li>${arr[rndIndex]}</li>`;

ul.append(li) 
```
### 删除节点
remove()
```JS
"use strict"
let ul = $('ul')

//字符串
// let li = '<li>哈哈哈哈</li>' 

//DOM节点（自己定义）
// let li = document.createElement('li')
// li.textContent = '嘿嘿嘿嘿'    

//OM节点（直接获取）
let li = document.getElementsByTagName('li')[0]

// let  arr = ['哈哈哈哈','嘿嘿嘿嘿','吼吼吼吼','呵呵呵呵','桀桀桀桀']
// let rndIndex = Math.floor(Math.random() *arr.length);
// let li = `<li>${arr[rndIndex]}</li>`;
***********
li.remove()
*********** 
//获取的节点被删除
```
## 事件
时间绑定含义：就是给指定的事件，绑定一个函数，当事件发生的时候，也就是指click事件发生的时候，绑定的对应函数，将被调用。

on方法用来绑定一个事件，我们需要传入事件名称和对应的处理函数。

```JS
a.on('click', function () {
    alert('Hello!');
})
等价于

a.click(function(){
    alter('Hello'); 
})
```
### 鼠标事件
- click:鼠标单击是触发
- dblclick:鼠标双击时触发
- mouseenter：鼠标进入时触发
- mouseleave：鼠标移出时触发
- mousemove：鼠标在DOM内部移动时触发
- hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave
### 键盘事件 
- keydown：键盘按下时触发；
- keyup：键盘松开时触发；
- keypress：按一次键后触发。 其他事件
- focus：当DOM获得焦点时触发；
- blur：当DOM失去焦点时触发；
- change：当< input>、< select>或< textarea>的内容改变时触发；
- submit：当< form>提交时触发；
- ready：当页面被载入并且DOM树完成初始化后触发。 其中，ready仅作用于document对象。由于ready事件在DOM完成初始化后触发，且只触发一次，所以非常适合用来写其他的初始化代码。假设我们想给一个< form>表单绑定submit事件，下面的代码没有预期的效果：
### 事件参数
```JS
获取鼠标位置和按键的值:
.text('pageX = ' + e.pageX + ', pageY = ' + e.pageY);
```
### 取消绑定
off('click', function)
```JS
function hello() {
    alert('hello!');
}

a.click(hello); // 绑定事件

// 10秒钟后解除绑定:
setTimeout(function () {
    a.off('click', hello);
}, 10000);
```
### 事件触发条件
```JS
input.change(); // 触发change事件
```