## 对象

typeof —区分对象类型
```js
typeof 123; //'number'
typeof NaN; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof Math.abs; // 'function'
typeof null; // 'object'
typeof []; // 'object'
typeof {}; // 'object'
```

注：<code>null</code>的类型是<code>object</code>，<code>Array</code>的类型也是<code>object</code>


1. 不要使用new Number()、new Boolean()、new String()创建包装对象；
2. 用parseInt()或parseFloat()来转换任意类型到number；
3. 用String()来转换任意类型到string，或者直接调用某个对象的toString()方法；
4. 通常不必把任意类型转换为boolean再判断，因为可以直接写if (myVar) {...}；
5. typeof操作符可以判断出number、boolean、string、function和undefined；
6. 判断Array要使用Array.isArray(arr)；
7. 判断null请使用myVar === null；
8. 判断某个全局变量是否存在用typeof window.myVar === 'undefined'；
9. 函数内部判断某个变量是否存在用typeof myVar === 'undefined'。

## Date
Date对象用来表示日期和时间,要获取系统当前时间:
```js
var now = new Date();
now; // Wed Jun 24 2015 19:49:22 GMT+0800 (CST)
now.getFullYear(); // 2015, 年份
now.getMonth(); // 5, 月份，注意月份范围是0~11，5表示六月
now.getDate(); // 24, 表示24号
now.getDay(); // 3, 表示星期三
now.getHours(); // 19, 24小时制
now.getMinutes(); // 49, 分钟
now.getSeconds(); // 22, 秒
now.getMilliseconds(); // 875, 毫秒数
now.getTime(); // 1435146562875, 以number形式表示的时间戳
```
## 时区
### 时间戳
在JavaScript中需要进行时区转换，但是只要传递的是一个number类型的时间戳，我们就不用关心时区转换

时间戳是一个自增的整数，它表示从1970年1月1日零时整的GMT时区开始的那一刻，到现在的毫秒数 从1970年01月-1日-0时0分0秒0豪秒到现在为止的数值之和.

时间戳可以精确地表示一个时刻，并且与时区无关

要获取当前时间戳，可以用：
```js
'use strict';
if (Date.now) {
    console.log(Date.now()); // 老版本IE没有now()方法
} else {
    console.log(new Date().getTime());
}
```