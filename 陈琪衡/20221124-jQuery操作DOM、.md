# jQuery

## 操作DOM

### 修改Text 和HTML

```html
<!--Html结构-->
<ul id="test-ul">
    <li class="js">JavaScript</li>
    <li name ="book">Java &amp; JavaScrpit</li>
</ul>
```

???



### 修改CSS

```html
<!-- HTML结构 -->
<ul id="test-css">
    <li class="lang dy"><span>JavaScript</span></li>
    <li class="lang"><span>Java</span></li>
    <li class="lang dy"><span>Python</span></li>
    <li class="lang"><span>Swift</span></li>
    <li class="lang dy"><span>Scheme</span></li>
</ul>
```

```js
'use strict'
$('#test-css li.dy>span').css('background-color', 'blue').css('color', 'red')
```