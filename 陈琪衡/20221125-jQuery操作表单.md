# 操作表单
对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性： 无参：获取值 有参：设置值

```html
    <input id="test-input" name="email" value="">
    <select id="test-select" name="city">
        <option value="BJ" selected>Beijing</option>
        <option value="SH">Shanghai</option>
        <option value="SZ">Shenzhen</option>
    </select>
    <textarea id="test-textarea">Hello</textarea>
```
```js
var
    input = $('#test-input'),
    select = $('#test-select'),
    textarea = $('#test-textarea');

input.val(); // 'test'
input.val('abc@example.com'); // 文本框的内容已变为abc@example.com

select.val(); // 'BJ'
select.val('SH'); // 选择框已变为Shanghai

textarea.val(); // 'Hello'
textarea.val('Hi'); // 文本区域已更新为'Hi'
```
## attr()、prop()、is()
attr()和prop()对于属性checked处理有所不同：

```js
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
```
prop()返回值更合理一些。不过，用is()方法判断更好：
```js
var radio = $('#test-radio');
radio.is(':checked'); // true
```
类似的属性还有selected，处理时最好用is(':selected')。