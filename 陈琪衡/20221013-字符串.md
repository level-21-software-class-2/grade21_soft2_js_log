## 字符串

JS的字符串用<code>' '</code>或<code>" "</code>表示
<code></code>

当使用''来表示字符串时，其内部可以包含"
比如"I'm OK"包含的字符是<code> I </code>,<code> ' </code>,<code> m </code>,<code> 空格 </code>,<code> O </code>,<code> K </code>这6个字符。

### 转义字符 \
如果字符串内部既包含'又包含"怎么办？可以用转义字符<code>\ </code>来标识，比如：
```
'I\'m\"OK"\"!';
```

转义字符\可以转义很多字符
比如<code>\ n</code>表示换行
    <code>\ t</code>表示制表符
字符\本身也要转义，所以<code>\ \ </code>表示的字符就是<code>\ </code>。

### 多行字符串`
用反引号``表示
```
`
这是一个
多行
字符串
`
```
## 数组
JS的Array可以包含任意数据类型，并通过索引来访问每个元素

1. 要取得Array的长度，可以直接访问<code>length</code>属性
```
var arr=[1,2,3];
arr.length;         //length=6
```

2. 可以对数组的索引进行赋值
```
var arr=['A','B','C']
arr[2]=99;
arr;    //arr变为['A','B',99]
```

3. 如果对索引赋值超过范围，会引起Arr大小的变化
```
var arr=[1,2,3]
arr[5]='a'
arr;    //arr=[1,2,3,underfined,underfined,'a']
```

4. 数组arr[]从0开始计数
```
var arr=[1,3,2,5,6]
console.log(arr[0])     //1
console.log(arr[3])     //5
```
5. 直接给Array的length赋一个新的值会导致Array大小的变化
``` 
var arr = [1, 2, 3];
arr.length;             // 3
arr.length = 6;
arr;                    // arr变为[1, 2, 3, undefined, undefined, undefined]
arr.length = 2;
arr;                    // arr变为[1, 2]
```
### indexOf()
Array可以通过indexOf()来搜索一个指定元素的位置
```
var arr = [10, 20, '30', 'xyz'];
arr.indexOf(10); // 元素10的索引为0
arr.indexOf(20); // 元素20的索引为1
arr.indexOf(30); // 元素30没有找到，返回-1
arr.indexOf('30'); // 元素'30'的索引为2
```
### slice()
它截取Array的部分元素，然后返回一个新的Array：
```
var arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
arr.slice(0, 3);    // 从索引0开始，到索引3结束，但不包括索引3: ['A', 'B', 'C']
arr.slice(3);       // 从索引3开始到结束: ['D', 'E', 'F', 'G']
```

