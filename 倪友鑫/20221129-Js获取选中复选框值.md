# 练习
用jQuery编程实现获取选中复选框值的函数abc

```html
<div>
        <input type="checkbox" name="aa" value="0" />0
        <input type="checkbox" name=" aa " value="1" />1
        <input type="checkbox" name=" aa " value="2" />2
        <input type="checkbox" name=" aa " value="3" />3
        <input type="button" onclick="abc()" value="提 交" />
        <div id="allselect"></div>
    </div>
```

```js
function abc() {
    var obj =$("div>input");
    for (let i = 0; i < obj.length; i++) {
        if(obj[i].checked){
            console.log(obj[i].value);
        }
    }
}
```

也可以定义一个数组然后把他们装在里面获取值