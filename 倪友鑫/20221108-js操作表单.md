# 操作文件
HTML表单的输入控件主要有以下几种：
文本框，对应的<input type="text">，用于输入文本；

口令框，对应的<input type="password">，用于输入口令；

单选框，对应的<input type="radio">，用于选择一项；

复选框，对应的<input type="checkbox">，用于选择多项；

下拉框，对应的<select>，用于选择一项；

隐藏文本，对应的<input type="hidden">，用户不可见，但表单提交时会把隐藏文本发送到服务器。
## 获取值
```js
如果我们获得了一个<input>节点的引用，就可以直接调用value获得对应的用户输入值：

// <input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
## 设置值
设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：
```js
// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
## 提交表单

```js
<!-- HTML -->
<form id="test-form" onsubmit="return checkForm()">
    <input type="text" name="test">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    return true;
}
</script>

```