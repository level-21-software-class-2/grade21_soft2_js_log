# 转义字符
1. \n       //换行
2. \t       //制表

# 多行字符串
1. 例：
```
`
这是一个
多行字符串
`
```

# 模板字符串
1. 例：
```
var name = 'xiaoming';
var age = 20;
var message = '你好，' + name + ',你今年' + age + '岁了！';
Console.log(message);
```
2. 或：
```
var message = `你好，${name},你今年${age}岁了！`;
```

# 操作字符串
1. 获取字符串的长度：
```
var s = 'Hello';
s.length;       //5
```
2. 索引获取下标
```
s[0];           //H
```

# toUpperCase
1. 把一个字符串全部变成大写：
```
s.toUpperCase();        //HELLO
```

# toLowerCase
1. 把一个字符串全部变成小写：
```
s.toLowerCase();        //hello
```

# indexOf
1. 会搜索指定索引的位置
```
s.indexOf('Hello');     //0
```

# substring
1. 返回指定索引内的内容
```
s.substring(0,1)        //从索引0开始到1(不包括1)，返回'H'
s.substring(1);         //从索引1开始到结束，返回'ello'
```