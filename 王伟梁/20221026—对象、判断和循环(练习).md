/*
    练习
    小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
*/

```
var height = parseFloat(prompt('请输入身高(m):'));
var weight = parseFloat(prompt('请输入体重(kg):'));
var bmi = weight / (height * height);

if(bmi > 32){
    console.log('严重肥胖');
}else if(bmi >= 28){
    console.log('肥胖');
}else if(bmi >= 25){
    console.log('过重');
}else if(bmi >= 18.5){
    console.log('正常');
}else{
    console.log('过轻');
}
```

/*
    练习
    利用for循环计算1 * 2 * 3 * ... * 10的结果：
*/

```
var x = 1;
var i;

for (i = 1; i <= 10; i++){
        x = x * i;
}
console.log(x);
```

/*
    练习
    请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
*/

//for循环正序

```
var arr = ['Bart', 'Lisa', 'Adam'];
var x = 0;

for (var i = 0; i < arr.length; i++){
    x = arr[i];
    console.log('hello' + x + '!');
}
```

//for循环倒序

```
var arr = ['Bart', 'Lisa', 'Adam'];
var x = 0;

for (var i = 0; i < arr.length; i++){
    arr.reverse();      //倒序
    x = arr[i];
    console.log('hello' + x + '!');
}
```

//while循环正序

```
var arr = ['Bart', 'Lisa', 'Adam'];
var n = 0;

while(n < arr.length){
    console.log('hello,' + arr[n] + '!');
    n = n + 1;
}
```

//while循环倒序

```
var arr = ['Bart', 'Lisa', 'Adam'];
var n = 0;

while(n < arr.length){
    arr.reverse();
    console.log('hello,' + arr[n] + '!');
    n = n + 1;
}
```