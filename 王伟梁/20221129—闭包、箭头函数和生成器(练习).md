/*
    练习 请使用箭头函数简化排序时传入的函数：
*/

```
var arr = [10, 20, 1, 2];
arr.sort((x, y) => {
    return x - y;
});
console.log(arr);
```