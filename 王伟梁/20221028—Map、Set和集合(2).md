# iterable
1. for...of遍历数组：
```
var a = [1,2,3];
var s = new Set([1,2,3]);
var m = new Map([['xiaoming',100],['xiaohong',99],['dahuang',80]]);

for (var x of a){
    console.log(x);
}
```
2. for...of遍历Set：
```
for (var x of s){
    console.log(x);
}
```
3. for...of遍历Map：
```
for (var x of m){
    console.log(x[0] + '=' + x[1]);
}
```
4. forEach方法：
```
var a = [1,2,3];
a.forEach(function(element,index,array){
    //element:指向当前元素的值
    //index：指向当前索引
    //array：指向array对象本身
    console.log(element + ', index = ' + index);
});
```
5. Set的forEach方法：       //forEach回调函数
```
//Set与数组类似，但是Set没有索引，所以前两个参数都是元素本身
var s = new Set({1,2,3});
s.forEach(function(element,sameElement,Set){
    cosole.log(element);
});
```
6. Map的forEach方法：
```
//Map的回调函数参数依次为value、key和map本身：
var m = new Map([['xiaoming',100],['xiaohong',99],['dahuang',80]]);
m.forEach(function(value,key,map){
    cosole.log(value);
});
```