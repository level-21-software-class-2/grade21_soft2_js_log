# JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成。

JavaScript的对象用于描述现实世界中的某个对象。例如，为了描述“小明”这个淘气的小朋友，我们可以用若干键值对来描述他：

    var xiaoming = {
        name: '小明',
        birth: 1990,
        school: 'No.1 Middle School',
        height: 1.70,
        weight: 65,
        score: null
    };
JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开。注意，最后一个键值对不需要在末尾加,，如果加了，有的浏览器（如低版本的IE）将报错。

然后通过xiaoming来获取小明的属性

    xiaoming.name; // '小明'
    xiaoming.birth; // 1990
访问属性是通过.操作符完成的，但这要求属性名必须是一个有效的变量名。如果属性名包含特殊字符，就必须用''括起来：

    var xiaohong = {
        name: '小红',
        'middle-school': 'No.1 Middle School'
    };
xiaohong的属性名middle-school不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问：

    xiaohong['middle-school']; // 'No.1 Middle School'
    xiaohong['name']; // '小红'
    xiaohong.name; // '小红'
也可以用xiaohong['name']来访问xiaohong的name属性，不过xiaohong.name的写法更简洁。我们在编写JavaScript代码的时候，属性名尽量使用标准的变量名，这样就可以直接通过object.prop的形式访问一个属性了。实际上JavaScript对象的所有属性都是字符串，不过属性对应的值可以是任意数据类型。

实际上JavaScript对象的所有属性都是字符串，不过属性对应的值可以是任意数据类型。

如果访问一个不存在的属性会返回什么呢？JavaScript规定，访问不存在的属性不报错，而是返回undefined：

由于JavaScript的对象是动态类型，你可以自由地给一个对象添加或删除属性：

    var xiaoming = {
        name: '小明'
    };
    xiaoming.age; // undefined
    xiaoming.age = 18; // 新增一个age属性
    xiaoming.age; // 18
    
    delete xiaoming.age; //使用delete 删除age属性
    xiaoming.age; // undefined
    
    delete xiaoming['name']; // 删除name属性注意这里是方括号
    xiaoming.name; // undefined
    delete xiaoming.school; // 删除一个不存在的school属性也不会报错
如果我们要检测xiaoming是否拥有某一属性，可以用in操作符：

    var xiaoming = {
        name: '小明',
        birth: 1990,
        school: 'No.1 Middle School',
        height: 1.70,
        weight: 65,
        score: null
    };
    'name' in xiaoming; // true
    'grade' in xiaoming; // false
不过要小心，如果in判断一个属性存在，这个属性不一定是xiaoming的，它可能是xiaoming继承得到的：

    'toString' in xiaoming; // true
因为toString定义在object对象中，而所有对象最终都会在原型链上指向object，所以xiaoming也拥有toString属性。

要判断一个属性是否是xiaoming自身拥有的，而不是继承得到的，可以用hasOwnProperty()方法：

    var xiaoming = {
        name: '小明'
    };
    xiaoming.hasOwnProperty('name'); // true
    xiaoming.hasOwnProperty('toString'); // false
 # 条件判断
JavaScript使用if () { ... } else { ... }来进行条件判断。例如，根据年龄显示不同内容，可以用if语句实现如下(尽量将大括号都带上)：

    var age = 20;
    if (age >= 18) { // 如果age >= 18为true，则执行if语句块
        alert('adult');
    } else { // 否则执行else语句块
        alert('teenager');
    }
 ## 多行条件判断
如果还要更细致地判断条件，可以使用多个if...else...的组合：

        var age = 3;
        if (age >= 18) {
            alert('adult');
        } else if (age >= 6) {
            alert('teenager');
        } else {
            alert('kid');
        }
# 循环
## for
JavaScript的循环有两种，一种是for循环，通过初始条件、结束条件和递增条件来循环执行语句块，for循环最常用的地方是利用索引来遍历数组，for循环的3个条件都是可以省略的，如果没有退出循环的判断条件，就必须使用break语句退出循环，否则就是死循环。

## for in
for循环的一个变体是for ... in循环，它可以把一个对象的所有属性依次循环出来（类似于java的for each）：

在对象中for in 浏览的是key值 在数组中流量序号值

    var o = {
        name: 'Jack',
        age: 20,
        city: 'Beijing'
    };
    for (var key in o) {
        console.log(key); // 'name', 'age', 'city'
    }
要过滤掉对象继承的属性，用hasOwnProperty()来实现：

    var o = {
        name: 'Jack',
        age: 20,
        city: 'Beijing'
    };
    for (var key in o) {
        if (o.hasOwnProperty(key)) {
            console.log(key); // 'name', 'age', 'city'
        }
    }
由于Array也是对象，而它的每个元素的索引被视为对象的属性，因此，for ... in循环可以直接循环出Array的索引：

    var a = ['A', 'B', 'C'];
    for (var i in a) {
        console.log(i); // '0', '1', '2'
        console.log(a[i]); // 'A', 'B', 'C'
    }
