## 基本语句
```
var cars = ["Saab","Volvo","BMw"]
```

## 数组的概念
数组是一种特殊的变量，它能够一次存放一个以上的值。

列：
若有一个项目清单，在单个变量中存储汽车品牌应是这样
```
    var car1 = "Saab";
    var car2 = "Volvo";
    var car3 = "BMw";
```
但当值很多时，就靠数组解决，数组可以用一个单一的名称存放很多值，并且还可以通过引用索引号来访问这些值。

## length属性
length属性返回数组的长度（数组元素的数目）
```
var fruits = ["Banana","Orange","Apple","Mango"]
fruits.length;          //fruits长度是4
```
length属性始终大于最高数组索引

![图裂了!](./ings/JavaScriptshuxin.png)

## indexOf
与String类似，数组也可以通过indexOf来搜索一个指定的元素的位置q
```
var arr =  [10,20,"30","xyz"];
arr.indexOf(10);  //元素10索引为0
arr.indexOf(20);  //元素20索引为1
arr.indexOf(30);  //元素30没有找到，返回-1
arr.indexOf('30');  //元素'30'索引为2
```

## slice
slice就是对应string的substring版本，它截取数组的部分元素，然后返回一个新的数组
如果不给slice传递如何参数，它就会从头到尾截取所有的元素，利用这一点，可以很容易的复制一个数组

## Pop和Push
Pop:从数组中弹出项目--删除最后一个元素

Push:向数组中推入项目--尾部添加若干元素

## unshift和shift
unshift:向数组头部添加若干元素

shift:删除数组头部第一个元素

## sort
可以对当前数组进行排序，它会直接修改当前数组的元素位置，直接调用时，按照默认顺序排序

## concat
cocat方法把当前的数组和另一个数组连接起来，并返回一个新的数组；
```
    var arr = ['A','B','C']
    var added = arr.concat([1,2,3]);
    added;  // ['A','B','C',1,2,3]
    arr; // ['A','B','C']
```
concat方法并没有修改当前数组，而是返回了一个新的数组
实际上，concart方法可以接受任意个元素和数组，并且自动把数组拆开，然后全部添加到新的数组里

## join
join方法是一个非常实用的方法，它把当前数组的每个元素都用指定的字符串连接起来，然后返回连接后的字符串
```
var arr = ['A','B','C',1,2,3]
arr.join('-');  //'A,B,C,1,2,3'
```
如果数组的元素不是字符串，将自动转换为字符串后再连接

## 多维数组
如果数组的某个元素又是同一个名称，可以形成多为数组
```
var arr = [[1,2,3],[400,500,600],'-']
```
实例中数组包含3个元素，其中头两个元素本身也是数组
\ No newline at end of file