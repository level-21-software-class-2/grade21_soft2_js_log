# Promise 补充
Promise对象是一个构造函数，用来生成Promise实例。
```  javascript
const promise = new Promise(function(resolve, reject) {
  // ... some code
 
  if (/* 异步操作成功 */){
    resolve(value);
  } else {
    reject(error);
  }
});
```

resolve和reject这两个回调可以在生成Promise实例后,调用then方法定义
``` javascript
promise.then(function(value) {
  // success
}, function(error) {
  // failure
});
```

``` javascript
let promise = new Promise(function(resolve, reject) {
  console.log('Promise');
  resolve();
});
 
promise.then(function() {
  console.log('resolved.');
});
 
console.log('Hi!');
// Promise
// Hi!
// resolved
/*
Promise 新建后立即执行，所以首先输出的是Promise。然后，then方法指定的回调函数，将在当前脚本所有同步任务执行完才会执行，所以resolved最后输出。
*/
```