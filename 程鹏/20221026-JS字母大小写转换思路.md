# javascript将大写字母转为小写的方法：
+ 1、利用toLowerCase()，语法“string.toLowerCase()”；
+ 2、用toLocaleLowerCase()，语法“string.toLocaleLowerCase()”。

1. toLowerCase() 方法用于把字符串转换为小写。
``` sql
 string.toLowerCase()
 
var str="HELLO WORLD!"

document.write(str.toLowerCase())
 ```

 2. 方法2：利用toLocaleLowerCase()
 ```sql

string.toLocaleLowerCase()
var str="HELLO WORLD!"

console.log(str.toLocaleLowerCase());
 ```