## JS对象
# JavaScript Array对象
   Array对象用于在单个变量中储存多个值
1. 创建Array对象的语法
var arr1 = new Array(); // 创建空数组
var arr2 = new Array(5); // 创建长度5 
var arr2 = new Array("8"); // 长度是1,[8]
var arr3 = new Array(1,2,3,4,5,7); // 创建长度为6 内容为1,2,3,4,5,7的数组
// arr2和arr3同样的定义方式,由于内容不同,所有代表的含义也不同
// arr3不等价于arr4 因为有new Array的存在
var arr4 = new Array[1,2,3,4,5]; // 这种定义是错误的,new Array是new一个对象出来

var arr5 = [1,2,3,4,7]; 
console.log(arr5.length);  //结果:5
console.log(arr5); // 打印结果是1,2,3,4,5 
// 不等价于
var arr6 = (1,2,3,4,7); // 这种定义方式是我比较出来的,自己也说不准这是啥,反正不是数组
console.log(arr6.length);  //结果:underfined
console.log(arr6);  // 结果:7


console.log(arr1,arr2,arr3,arr4); // 分别把这些数组中的内容打印出来

// 属性 length 数组的长度
console.log(arr2.length);

方法
push/pop
push/pop 对数组末尾添加/删除元素
push():向数组的末尾添加一个或更多元素,并返回新的长度
var newLength = arr5.push(6,7,8);
console.log(arr5); //结果:[4,5,6,7,8]

pop():删除并返回数组的最后一个元素
var num = arr5.pop();
console.log(arr5.length,num);// 结果:4,8

unshift()/shift()
unshift():向数组的开头添加一个或更多元素,并返回新的长度
var b = arr5.unshift(1,2,3);
console.log(b); //结果:7

shift():删除并返回数组的第一个元素
var c = arr5.shift();
console.log(c); //结果:1

sort()
如果调用该方法时没有使用参数,将按字母顺序对数组中的元素进行排序,说的精确点,是按照字符编码的顺序进行排序.要实现这一点,首先应把数组的元素都换成字符串,以便进行比较
例1:
<script type="text/javascript">
    var arr = new Array(6);
    arr[0] = "George";
    arr[1] = "John";
    arr[2] = "Thomas";
    arr[3] = "James";
    arr[4] = "Adrew";
    arr[5] = "Martin";

    console.log(arr);
    console.log(arr.sort());
</script>

结果:
George,John,Thomas,James,Adrew,Martin
Adrew,George,James,John,Martin,Thomas

例2:
<script type="text/javascript">

var arr = new Array(6);
arr[0] = "10";
arr[1] = "5";
arr[2] = "40";
arr[3] = "25";
arr[4] = "1000";
arr[5] = "1";

console.log(arr);
console.log(arr.sort());
</script>

结果:
10,5,40,25,1000,1
1,10,1000,25,40,5

例3:
<script type="css/javascript">
    var arr = new Array(6);
    arr[0] = 10;
    arr[1] = 5;
    arr[2] = 40;
    arr[3] = 25;
    arr[4] = 1000;
    arr[5] = 1;

    console.log(arr);
    console.log(arr.sort())
</script>

结果:
10 5 40 25 1000 1
1 5 10 25 40 1000

如果要对上面的数进行倒序排列或者上面的数字都是字符串,然后根据数字大小进行排列,代码如下:

function sortNum(a,b){
    return a-b;
}
arr.sort(sortNum);

或者:
arr.sort(function(a,b){
    return a - b;
});

splice
arrayObject.splice(index,howmany,item1,....,itemX);
说明:splice()方法可删除从index处开始的零个或者多个元素,并且用参数列表声明的一个或多个值来替换哪些被删除的元素,howmany的值与item的数量不需要一定保持一致.
如果从arrayObject中删除了元素,则返回的是含有被删除元素的数组.
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style type="text/css">
        </style>
    </head>
    <body>

    </body>
    <script type="text/javascript">
        var arr = new Array(5);
        arr[0] = "ds";
        arr[1] = "er";
        arr[2] = "ty";
        arr[3] = "uq";
        arr[4] = "yt";

        var array = arr.splice(1,0,"qq","ww");
        console.log(array);
        console.log(arr);
    </script> 
</html>

结果:["ds", "qq", "ww", "er", "ty", "uq", "yt"]


join
arrayObject.join(separator)
返回一个字符串,该字符串是通过把 arrayObject 的每个元素转换为字符串，然后把这些字符串连接起来，在两个元素之间插入 separator 字符串而生成的。
separator 可选,指定要使用的分隔符。如果省略该参数，则使用逗号作为分隔符。

打印长度,打印的是数组中内容的长度
var arr3 = new Array(1,2,3,4,7);
console.log(arr3.length);  // 结果:5
console.log(arr3);  // 结果:[1,2,3,4,7]

打印长度,转换为字符串,打印的是内容加逗号的长度
var arr6 = new Array(1,2,3,4,7).toString();
console.log(arr6.length); // 结果:9
console.log(arr6);  // 结果:1,2,3,4,7

打印长度,转换为字符串,用逗号连接数组元素时,相当于toString()方法,打印的是内容加逗号的长度
var arr4 = new Array(123,47).join(",");
console.log(arr4.length); // 结果: 6
console.log(arr4);  // 结果: 123,47

打印长度,转换为字符串,join括号中的内容为空,这时也是相当于toString()方法
var arr9 = new Array(1,2,3,4,7).join();
console.log(arr9.length);  // 结果:9
console.log(arr9); // 结果:1,2,3,4,7

不用任何内容连接数组中的元素
var arr7 = new Array(123,47).join("");
console.log(arr7.length); // 结果: 5
console.log(arr7); // 结果:12347

用+=将数组中的内容连接
var arr8 = new Array(1,2,3,4,7).join("+=");
console.log(arr8.length); // 结果: 13
console.log(arr8); // 结果: 1+=2+=3+=4+=7

JavaScript Math对象
Math对象并不像Date和String那样是对象的类,因此没有构造函数Math(),像Math.sin()这样的函数只是函数,不是某个对象的方法.
你无需创建它,通过把Math作为对象使用就可以调用其所有属性和方法.

<script type="text/javascript">
    // 取π的值
    var pi_value = Math.PI;
    console.log(pi_value);  // 结果:3.141592653589793
    // 求一个数的平方根
    var sqrt_value = Math.sqrt(15);
    console.log(sqrt_value);  // 结果:3.872983346207417
    // 求一个数的绝对值
    var abs_value = Math.abs(-5);
    console.log(abs_value);  // 结果:5
    // 向上取整
    console.log(Math.ceil(5.1));  // 6
    console.log(Math.ceil(5.5));  // 6
    console.log(Math.ceil(5.9));  // 6
    console.log(Math.ceil(5));  // 5
    console.log(Math.ceil(-5.1));  // -5
    console.log(Math.ceil(-5.9));  // -5
    // 向下去整 floor(x)
    // 把数四舍五入为最接近的整数 Math.round(x)
    // Math.random() 返回0-1直接的随机数
</script>  

2. JavaScript Date对象
<script type="text/javascript">
    var date1 = new Date("2016-9-1");
    var date2 = new Date("2016-9-30");
    // 两个日期相减得到的时间差,单位是毫秒
    var res = date2 - date1;
    console.log(res/1000/3600/24 + 1); //30
    // 从Date对象中,以四位数字返回年份
    var time = date1.getFullYear();
    console.log(time); //2016
    // 从Date对象中返回一个月中的那一天
    var d = date1.getDate(); // 结果:1,date1中定义的是哪一天,就打印哪一天
    date1.setDate(d + 5); // 第6天
    console.log(date1); //结果:Tue Sep 06 2016 00:00:00 GMT+0800 (CST)
    // 根据世界时，把 Date 对象转换为字符串
    var date3 = new Date();
    console.log(date3);  //结果:Fri Mar 09 2018 20:19:46 GMT+0800 (CST)
    console.log(date3.toUTCString()); //结果:Fri, 09 Mar 2018 12:19:46 GMT
</script> 

3. JavaScript String对象
<script type="text/javascript">
    // anchor()创建HTML锚
    var txt = "hello World";
    console.log(txt.anchor("text"));  // 结果:<a name="text">hello World</a>
    var str="Hello world!"
    console.log(str.length); //结果:12
</script> 

## 正则运算符
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style type="text/css">
        </style>
    </head>
    <body>

    </body>
<script type="text/javascript">
    // reg = /正则部分(定义的规则)/正则属性
    // g 全局匹配 加i 就不区分大小写
    var reg = /test/g;
    var str = " hellotesthowareTestyoutest";
    console.log(str.match(reg)); // 结果:["test", "test"]
    var reg = /test/gi;
    console.log(str.match(reg)); //结果:["test", "Test", "test"]

    // \d代表数字
    var reg = /\d/g;
    var str = "sdafsdgdf15dfbgsd566189safg4ag";
    console.log(str.match(reg)); //结果:["1", "5", "5", "6", "6", "1", "8", "9", "4"]

    // \w代表单词字符:数字 字母 下划线
    var reg = /\w/g;
    var str = "dsf457__fgbd_1";
    console.log(str.match(reg)); // 结果:["d", "s", "f", "4", "5", "7", "_", "_", "f", "g", "b", "d", "_", "1"]

    // \.代表除去换行符的任意字符 (\n换行  \t 大空格 \b 退格符 \\代表是一个反斜杠)
    var reg4 = /./g;
    var str4 = "\n\teH3_-=+_*/,.`() *&....%$#@!!^&\\";
    //结果:[" ", "e", "H", "3", "_", "-", "=", "+", "_", "*", "/", ",", ".", "`", "(", ")", " ", "*", "&", ".", ".", ".", ".", "%", "$", "#", "@", "!", "!", "^", "&", "\"]
    console.log(str4.match(reg4));

    //\s只要出现空白就匹配 \S只要不出现空白就匹配
    var reg5 = /\s/g;
    console.log(str4.match(reg5)); //结果: ["↵", "    ", " "]

    //var reg6 = /\w{6,12}/g; 可以作为用户名规则 6-12位
     var reg6 = /\d{2,3}/g;
     var str6 = "3409800ru2h404";
     console.log(str6.match(reg6));

    // 小括号 代表 只匹配 括号中的元素.
    // var reg9 = /(34)|(32)/g
    // var reg9 = /3(4)|3(2)/g = var reg9 = /3[24]/g 他们是等价的
     var  reg9 = /3(4)|3(2)/g;
         var str9 = "34876543323752677";
     console.log(str9.match(reg9));

     //开头和结尾  只找第一位元素  如果找不到就回返回空
     var reg10 = /^34/g;
     var str10 = "434349852342323";
     console.log(str10.match(reg10));

     //开头和结尾  只找最后一位元素  如果找不到就回返回空
     var reg10 = /34$/g;
     var str10 = "34349852342323";
     console.log(str10.match(reg10));

     //开头和结尾  同时用就代表 str10只能是34,否则就返回空.
     var reg10 = /^34$/g;
     var str10 = "34";
     console.log(str10.match(reg10));

     //限制字符串长度
     var reg10 = /^\d{11}$/g;
     var str10 = "34349852342";
     console.log(str10.match(reg10));

     // 匹配 3  到 6  长度的字符串 
     var reg10 = /^\d{3,6}$/g;
     var str10 = "343498";
     console.log(str10.match(reg10));

     //11位 纯数字的电话号码. 1开头 第二位是3-8 之间的数字.

     var reg11 = /^1(3[10379]|4[7]|5[0258]|6[1]|7[378]|8[029])\d{8}$/g;
     var str11 = "14755123198";
     console.log(str11.match(reg11));

     //[a-z]所有的小写字母
     //[A-Z]所有的大写字母

     //邮箱
     //(4-10为单词字符 , 开头必须是字母)@
     //@qq @163. cn  com   
     //XXX@qq.com.cn

    </script>
</html>
