## promise模板
```
function fn(a,b){
    //这里有一系列的命令或者代码
    if(1){
        a();
    }else{
        b();
    }
}
let p =new Promise(fn);

p.then(function(){
    console.log('我成功了');
}).catch(function(){
    console.log('我失败了');
})
```
## 极简
```
new Promise(function(a,b){

}).then().catch()
```


![图裂了](./img/promise.JPG)