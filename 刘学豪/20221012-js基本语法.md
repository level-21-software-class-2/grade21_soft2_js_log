# JavaScript 使用
## 内部使用
脚本可被放置与 HTML 页面的 <body> 或 <head> 部分中，或兼而有之。把脚本置于 <body> 元素的底部，可改善显示速度，因为脚本编译会拖慢显示。
## 外部使用
外部脚本很实用，如果相同的脚本被用于许多不同的网页。
JavaScript 文件的文件扩展名是 .js。
如需使用外部脚本，请在script标签的 src (source) 属性中设置脚本的名称。
### 外部 JavaScript 的优势
在外部文件中放置脚本有如下优势：

1. 分离了 HTML 和代码
2. 使 HTML 和 JavaScript 更易于阅读和维护
3. 已缓存的 JavaScript 文件可加速页面加载

## 语句
###  分号 ;　：分号分隔 JavaScript 语句。请在每条可执行的语句之后添加分号.
### 字符串:是文本，由双引号或单引号包围
 ### null和undefined
 在javascript中还有一个类似于 null 的值：undefined

 null 表示为空；  
 undefined 表示"未定义"
  ### Math
 `Math.random()`生成一个 0~1 的随机数

 ## parseInt()
 将里面的数据转化为Int类型
