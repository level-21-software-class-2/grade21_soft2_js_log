11.22笔记
Promise第二讲
回调可能导致的问题
代码不美观
容易造成回调地狱
Promise可以让异步任务达到三个方面的效果
串行执行
并行执行
更好的保证任务的容错性
串行
每一个由promise封装的任务都顺序执行，即上一个执行完成后再执行下一个

function Mult(x) {
    return new Promise(function(resolve,reject){
        let result = x*x
        console.log(`这里进行乘法运算，传入值为：${x},返回值为：${result}`);
        setTimeout(resolve,800,result);
    })
}

function Plus(x){
    return new Promise(function(resolve,reject){
    let result = x+x;
    console.log(`这里进行加法运算，传入值为：${x},返回值为：${result}`);
    setTimeout(resolve,500,result)
    })
}

function Subt(x) {
    return new Promise(function(resolve,reject) {
        let result = x-x;
        console.log(`这里进行减法运算，传入值为：${x},返回值为：${result}`)
        setTimeout(resolve,500,result)
    })
}

function Divi(x) {
    return new Promise(function(resolve,reject) {
        let result = x/x;
        console.log(`这里进行除法运算，传入值为：${x},返回值为：${result}`);
        setTimeout(resolve,500,result)
    })
}
let Num = new Promise(function(resolve,reject){
    setTimeout(() => {
        resolve(3)
    }, 1500);
})
***********************************************
Num.then(Mult).then(Mult).then(Divi).then(Plus).then(function(x){
    console.log(x);
})
***********************************************
并行
互相独立的promise, 并每一个promise执行之后的状态（已完成或拒绝）存放在数组中

并行中存在all和race两种方法

all会把所有promise对象resolve的数据传递到then中，race只传递最先返回的那个promise resolve的值

all
let Num1 = new Promise(function(resolve,reject) {
    setTimeout(() => {
        console.log('必做任务1');
        resolve();
    }, 1000);
})

let Num2 = new Promise(function(resolve,reject) {
    setTimeout(() => {
        console.log('必做任务2')
        resolve();
    }, 1000);
})
***********************************************
Promise.all([Num1,Num2]).then(function(x) {
    console.log('任务完成');
}).catch(function(x) {
    console.log('任务失败');
})
***********************************************
容错并行
race(赛跑，哪个结果返回的快返回哪个)
Num.race([Num1,Num2]).then()