获取DOM信息
prop()
attr()
<!--HTML-->
<input id="test-radio" type="radio" name="test" checked value="1">

var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
操作表单
对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性。

<!--HTML-->
    <select id="test-select" name="city">
        <option value="BJ" selected>Beijing</option>
        <option value="SH">Shanghai</option>
        <option value="SZ">Shenzhen</option>
    </select>
    
let select = $('#test-select')
select.val(); // 'BJ'
select.val('SH'); // 选择框已变为Shanghai