## JS中的数组
+ JS中的数组可以包括任意数组类型，并通过索引访问元素
```JS
var arr=[1,2,3.14,'Hello',null,true];
```
+ arr.ength():获取数组长度
```JS
var arr=[1,2,3];
console.log(arr.length);//3
```
+ 直接给任意数组arr的length赋一个新值，会导致arr的长度改变，但是非必要时不建议修改数组的长度
```JS
var arr=[1,2,3];
arr.length=5;//改变数组arr的length
arr=[1,2,3,undefined,undefined];//此时arr中的元素
arr[4]=5;//给arr中下标为4的元素赋值为5
arr=[1,2,3,undefined,5];//此时arr中的元素
```
+ arr.insexOf(元素)：找到元素的话，返回找到的第一个元素的下标，没有找到的话返回-1
```JS
var arr=[1,2,3];
console.log(1);//控制台输出0
console.log(4);//控制台输出-1
```
+ arr.includes(元素)：找到元素返回true，找不到返回false
```JS
var arr=[1,2,3];
console.log(1);//控制台输出true
console.log(4);//控制台输出false
```
+ findIndex函数接受了一个函数f，这个函数将应用在数据中的每一个元素，如果f在某个元素上的处理结果返回的是true，则直接返回当前元素所在的下标，如果无返回值，则会显示-1
```JS
var arr=[1,2,3];
console.log(arr.findIndex(function(val){reyurn val=3;}));//控制台输出2
console.log(arr.findIndex(function(val){reyurn val=4;}));//控制台输出-1
```
+ arr.slice(起始位置，截取长度)：对应substring()版本，截取数组arr中的部分元素，然后返回一个新的元素(就是被截取的文字)
```JS
var arr=[1,2,3];
var new_arr=arr.slice(1，2);
console.log(new_arr);//控制台输出1，2
```
+ push和pup
```JS
var arr=[1,2,3];
arr.push('A','B');
console.log(arr);//控制台输出1,2,3,'A','B'

arr.pop();//从arr数组中拿掉最后的一个元素
console.log(arr);//控制台输出1,2,3,'A'
```
+ reverse()方法反转数组
```JS
var arr=[1,2,3];
console.log(arr.reverse);//控制台输出3,2,1
```
+ unshift和shift()
```JS
var arr=[1,2,3];
arr.unshift('A','B');
console.log(arr);//控制台输出'A','B'，1,2,3

arr.shift();//从arr数组中拿掉最前面的一个元素
console.log(arr);//控制台输出'B'，1,2,3
```
+ reverse()方法反转数组
```JS
var arr=[1,2,3];
console.log(arr.reverse);//控制台输出3,2,1
```
+ arr.splice(开始索引，从开始索引截取几个元素,添加的值，添加的值):
```JS
//删除+添加
var arr=[1,2,3,4,5];
arr.splice(2,2,'3','4')
console.log(arr);//控制台输出1，2,'3','4',5

//不删除+添加
var arr=[1,2,3,4,5];
arr.splice(5,0,6,7);
console.log(arr);//控制台输出1,2,3,4,5,6,7

//删除+不添加arr.splice(开始索引，从开始索引截取几个元素,添加的值，添加的值):
var arr=[1,2,3,4,5];
arr.splice(2,2)
console.log(arr);//控制台输出1，2,5
```
+ arr.concat():当前数组合并一个数组，并返回一个新的数组：
```JS
var arr=[1,2,3];
var arr_new=arr.concat([4,5]);
console.log(arr_new);//控制台会输出 1,2,3,4,5
```
+ arr.join():把当前数组Arr的每个元素都用指定的字符串连接起来，然后返回连接后的字符串，如果Array的元素不是字符串，将自动转换为字符串后再连接
```JS
var arr=[1,2,3];
var arr_new=arr.join('-');

console.log(arr);//控制台会输出 1,2,3
console.log(arr_new);//控制台会输出 1-2-3
```
+ 多维数组：多维数组中的元素数据类型要一致
```JS
var arr=[[1,2,3],[4,5,6],[7,8,9]];
//取到8的值

var arr1=arr[2][1];
console.log(arr1);
```