# 操作DOM
## 修改Text和HTML
+ jQuery对象的text()和html()方法分别获取节点的文本和原始HTML文本，例如，如下的HTML结构：
```JS
<!-- HTML结构 -->
<ul id="test-ul">
    <li class="js">JavaScript</li>
    <li name="book">Java &amp; JavaScript</li>
</ul>
//分别获取文本和HTML：
$('#test-ul li[name=book]').text(); // 'Java & JavaScript'
$('#test-ul li[name=book]').html(); // 'Java &amp; JavaScript'
```
## 修改CSS
```JS
//高亮显示动态语言，调用jQuery对象的css('name', 'value')方法，我们用一行语句实现：
'use strict';
$('#test-css li.dy>span').css('background-color', '#ffd351').css('color', 'red');
//jQuery对象的css()方法可以这么用：
var div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性
//css()方法将作用于DOM节点的style属性，具有最高优先级。如果要修改class属性，可以用jQuery提供的下列方法：
//css的执行优先级关系：从高到低：行内样式>内部样式>外部样式
var div = $('#test-div');
div.hasClass('highlight'); // false， class是否包含highlight
div.addClass('highlight'); // 添加highlight这个class
div.removeClass('highlight'); // 删除highlight这个class
```
## 显示和隐藏DOM（jQuery hide() 和 show()）
+ 要隐藏有个dom节点，可以设置CSS的display属性为none，利用css()方法就可以实现。不过，要显示这个DOM就需要恢复原有的display属性，这就得先记下来原有的display属性到底是block还是inline还是别的值。注意，隐藏DOM节点并未改变DOM树的结构，它只影响DOM节点的显示。这和删除DOM节点是不同的。
```JS
var a = $('a[target=_blank]');
a.hide(); // 隐藏
a.show(); // 显示
```
## 获取DOM信息
+ 利用jQuery对象的若干方法，我们直接可以获取DOM的高宽等信息，而无需针对不同浏览器编写特定代码：
```JS
// 浏览器可视窗口大小:
$(window).width(); // 800
$(window).height(); // 600

// HTML文档大小:
$(document).width(); // 800
$(document).height(); // 3500

// 某个div的大小:
var div = $('#test-div');
div.width(); // 600
div.height(); // 300
div.width(400); // 设置CSS属性 width: 400px，是否生效要看CSS是否有效
div.height('200px'); // 设置CSS属性 height: 200px，是否生效要看CSS是否有效
```
+ attr()和removeAttr()方法用于操作DOM节点的属性：
```JS
// <div id="test-div" name="Test" start="1">...</div>
var div = $('#test-div');
div.attr('data'); // undefined, 属性不存在
div.attr('name'); // 'Test'
div.attr('name', 'Hello'); // div的name属性变为'Hello'
div.removeAttr('name'); // 删除name属性
div.attr('name'); // undefined
```
+ prop()方法和attr()类似，但是HTML5规定有一种属性在DOM节点中可以没有值，只有出现与不出现两种，例如：
```JS
<input id="test-radio" type="radio" name="test" checked value="1">
//等价于：
<input id="test-radio" type="radio" name="test" checked="checked" value="1">
```
```JS
//attr()和prop()对于属性checked处理有所不同：
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
//prop()返回值更合理一些。不过，用is()方法判断更好：
 var radio = $('#test-radio');
radio.is(':checked'); // true
//类似的属性还有selected，处理时最好用is(':selected')。
```