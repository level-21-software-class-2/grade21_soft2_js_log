+ 高阶函数：JavaScript的函数其实都指向某个变量。既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。简而言之就是接受一个函数B作为的函数A，称A为高阶函数。
```JS
'use strict'
function add(x,y,fn) {
    return fn(x)+fn(y);
}
var x=add(5,-5,Math.abs);
console.console.log(x);
// 
```
+ map()：JS中的一个高阶函数，map()就是可以把一个自定义的函数运用在原数组Arrary中的每一个元素中，并产生一个新的数组。
```JS
 var arr=[1,2,3,4,5,6,7,8,9,10];
 function pingfang(x){
    return x*x;
 }
 var newArr=arr.map(pingfang);
```
+ reduce():Array的reduce()可以把自定义的一个函数作用在这个Array的[x1,x2,x3,x4]上，这个函数必须接收两个函数，reduce()把结果继续和序列的下一个元素累积计算。（reduce返回一个全新的数组。reduce接受一个函数作为参数，这个函数要有两个形参，代表数组中的前两项，reduce会将这个函数的结果与数组中的第三项再次组成这个函数的两个形参以此类推进行累积操作。）
```JS
var arr=[1,2,3,4,5];
var arr1=arr.reduce((a,b)=>a+b);
console.log(arr1);
```