# 操作表单
```
1、用JavaScript操作表单和操作DOM是类似的，因为表单本身也是DOM树。
2、不过表单的输入框、下拉框等可以接收用户输入，所以用JavaScript来操作表单，可以获得用户输入的内容，或者对一个输入框设置新的内容。
3、在JS中HTML表单的输入控件主要有以下几种：
    a、文本框，对应的<input type="text">，用于输入文本；
    b、口令框，对应的<input type="password">，用于输入口令；
    c、单选框，对应的<input type="radio">，用于选择一项；
    d、复选框，对应的<input type="checkbox">，用于选择多项；
    e、下拉框，对应的<select>，用于选择一项；
    f、隐藏文本，对应的<input type="hidden">，用户不可见，但表单提交时会把隐藏文本发送到服务器。
    ……
```
### 获取值
+ 如果我们获得了一个input节点的引用，就可以直接调用value获得对应的用户输入值：
```JS
<input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
+ 上面这种方式可以应用于text、password、hidden以及select这类单选框。对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：
```JS
// <label><input type="radio" name="weekday" id="monday" value="1"> Monday</label>
// <label><input type="radio" name="weekday" id="tuesday" value="2"> Tuesday</label>
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
mon.value; // '1'
tue.value; // '2'
mon.checked; // true或者false
tue.checked; // true或者false
```

### 设置值
+ 设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：
```JS
// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
+ 对于单选框和复选框，设置checked为true或false即可。
### HTML5控件
+ HTML5新增了大量标准控件，常用的包括date、datetime、datetime-local、color等，它们都使用input标签：
```JS
<input type="date" value="2015-07-01">

2015/07/01
<input type="datetime-local" value="2015-07-01T02:03:04">

2015/07/01 02:03:04
<input type="color" value="#ff0000">
```
+ 不支持HTML5的浏览器无法识别新的控件，会把它们当做type="text"来显示。支持HTML5的浏览器将获得格式化的字符串。例如，type="date"类型的input的value将保证是一个有效的YYYY-MM-DD格式的日期，或者空字符串。
### 提交表单
+ 表单提交常见的有两种方式：
```
1、form的原始提交，即给表单中指定提交的路径，然后将按钮类型修改为submit类型，当点击这个按钮是表单就会被提交；
2、利用JS函数提交，即去掉表单的action路径，然后将按钮的类型修改为简单的button，设置按钮的click事件绑定一个函数或若干个函数，则当点击这个按钮时，事件被触发，事件就会执行被绑定的函数。
```
