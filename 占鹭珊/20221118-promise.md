## Promise 
+ 是抽象的异步处理对象，以及对其进行各种操作的组件。我知道这样解释你肯定还是不明白 Promise 是什么东西，你可以把 Promise 理解成一个 容器，里面装着将来才会结束的一个事件的结果，这个事件通常是一个异步操作。而Promise则是把类似的异步处理对象和处理规则进行规范化， 并按照采用统一的接口来编写，而采取规定方法之外的写法都会出错。
```JS
//使用 Promise 处理异步操作:
var promise = loginByPromise("http://www.r9it.com/login.php"); 
promise.then(function(result){
  // 登录成功时处理
}).catch(function(error){
  // 登录失败时处理
});
```
+ 有了Promise对象，就可以将异步操作以同步操作的流程表达出来。 这样在处理多个异步操作的时候还可以避免了层层嵌套的回调函数（后面会有演示）。 此外，Promise对象提供统一的接口，必须通过调用 Promise#then 和 Promise#catch 这两个方法来结果，除此之外其他的方法都是不可用的，这样使得异步处理操作更加容易。
+ Promise有三种状态，分别是：Pending（进行中），Resolved(已完成)，Rejected(已失败)。Promise从Pending状态开始，如果成功就转到成功态，并执行resolve回调函数；如果失败就转到失败状态并执行reject回调函数。下面是Promise的规范图解
### 基本用法
+  1、构造方法：Promise 构造函数接受一个函数作为参数，该函数的两个参数分别是 resolve 和 reject。它们是两个函数，由 JavaScript 引擎提供，不用自己部署。
```JS
let promies = new Promise((resolve, reject) => {
 resolve(); //异步处理 
});

```
+ 2、通过 Promise 实例的方法，Promise#then 方法返回的也是一个 Promise 对象
```JS
promise.then(onFulfilled, onRejected);
```
+ 3、通过 Promise 的静态方法，Promise.resolve()，Promise.reject()
```JS
var p = Promise.resolve();
p.then(function(value) {
 console.log(value); 
});
```
+ Promise 的执行流程

new Promise构造器之后，会返回一个promise对象;
为 promise 注册一个事件处理结果的回调函数(resolved)和一个异常处理函数(rejected);