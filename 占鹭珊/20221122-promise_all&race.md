+ 静态资源：通常指JS、CSS、imgs，包括所有上传的文件、音频等。
+ 回调函数可能导致的问题：
```
1、代码不够美观；
2、容易形成回调地狱(代码错误不好找)
```
+ promise可以让异步任务达到3个方面的效果
```JS
//A 任务的顺序执行(串形执行)
p.then().then();

//B 任务的并行执行
p.all([p1,p2]).then();

//C 更好的保证任务的容错性(容错并行)
p.race([p1,p2]).then();
```
+ setTimeOut书写方式
```JS
// 第一种
setTimeout(() => {
    resolve(x*x);        
        }, 800);
// 第二种
setTimeout(resolve,800,x*x);
```
# 并行执行任务all
Promise的all方法提供了并行执行异步操作的能力，在all中所有异步操作结束后才执行回调。
```JS
function p1(){
    var promise1 = new Promise(function(resolve,reject){
        console.log("p1的第一条输出语句");
        console.log("p1的第二条输出语句");
        resolve("p1完成");
    })
    return promise1;
}

function p2(){
    var promise2 = new Promise(function(resolve,reject){
        console.log("p2的第一条输出语句");
        setTimeout(()=>{console.log("p2的第二条输出语句");resolve("p2完成")},2000);

    })
    return promise2;
}

function p3(){
    var promise3 = new Promise(function(resolve,reject){
        console.log("p3的第一条输出语句");
        console.log("p3的第二条输出语句");
        resolve("p3完成")
    });
    return  promise3;
}

Promise.all([p1(),p2(),p3()]).then(function(data){
    console.log(data);
})
//其运行结果为下图：这里可以看到p2的resolve放到一个setTimeout中，最后的.then也会等到所有Promise完成状态的改变后才执行。
```
[![z86ePK.png](https://s1.ax1x.com/2022/11/23/z86ePK.png)](https://imgse.com/i/z86ePK)
# 容错并行race
在all中的回调函数中，等到所有的Promise都执行完，再来执行回调函数，race则不同它等到第一个Promise改变状态就开始执行回调函数。将上面的`all`改为`race`,得到
```JS
Promise.race([p1(),p2(),p3()]).then(function(data){
    console.log(data);
})
//其运行结果为下图：红框中圈出了"p1完成"字样，说明当执行then()方法时，只有第一个promise的状态改变了。
```
[![z86Qrd.png](https://s1.ax1x.com/2022/11/23/z86Qrd.png)](https://imgse.com/i/z86Qrd)8