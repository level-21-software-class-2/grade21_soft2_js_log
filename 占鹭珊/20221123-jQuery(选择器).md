# jQuery概述
## jQuery概述
jQuery是一个快速，简洁的JavaScript库，其宗旨是“write less，do more”，即提倡写更少的代码，做更多的事情。j就是JavaScript，query是查询。就是把JS中DOM操作做了封装，我们可以快速查询使用里面的功能。jQuery封装了JavaScript常用的功能代码，优化了DOM操作、事件处理、动画设计和Ajax交互。学习jQuery的本质：就是学习调用这些函数。
## jQuery优点
```
1 轻量级：核心文件才即几十kb；
2 跨浏览器兼容。基本兼容主流浏览器；
3 链式编程、隐式迭代；
4 对事件、样式、动画支持，大大简化了DOM操作；
5 支持插件扩展开发。有着丰富的第三方插件，例如：树形菜单、日期控件、轮播图等；
6 免费、开源。
```
## 使用jQuery
```
使用jQuery只需要在页面的

引入jQuery文件即可：
<html>
<head>
    <script src="./static/js/jQuery.min.js"></script>
	...
</head>
<body>
    ...
</body>
</html>
```
### $符号
```
本质上就是一个函数，但是函数也是对象，于是 除了可以直接调用外，也可以有很多其他属性。

window.jQuery; // jQuery(selector, context)
window.$; // jQuery(selector, context)
$ === jQuery; // true
typeof($); // 'function'
```
### 选择器
选择器是jQuery的核心。一个选择器写出来类似$('#dom-id')。jQuery的选择器就是帮助我们快速定位到一个或多个DOM节点。
```JS
//1 按ID查找
// 查找<div id="abc">:
var div = $('#abc');

//2 按tag查找,按tag查找只需要写上tag名称就可以了：
var ps = $('p'); // 返回所有<p>节点
ps.length; // 数一数页面有多少个<p>节点

//3 按class查找,按class查找注意在class名称前加一个.：
var a = $('.red'); // 所有节点包含`class="red"`都将返回
// 例如:
//<div class="red">...</div>
//<p class="green red">...</p>
//通常很多节点有多个class，我们可以查找同时包含red和green的节点：
var a = $('.red.green'); // 注意没有空格！
// 符合条件的节点：
// <div class="red green">...</div>
// <div class="blue green red">...</div>

//4 按属性查找,一个DOM节点除了id和class外还可以有很多属性，很多时候按属性查找会非常方便，比如在一个表单中按属性来查找：
var email = $('[name=email]'); // 找出<??? name="email">
var passwordInput = $('[type=password]'); // 找出<??? type="password">
var a = $('[items="A B"]'); // 找出<??? items="A B">
//当属性的值包含空格等特殊字符时，需要用双引号括起来。按属性查找还可以使用前缀查找或者后缀查找：
var icons = $('[name^=icon]'); // 找出所有name属性值以icon开头的DOM
// 例如: name="icon-1", name="icon-2"
var names = $('[name$=with]'); // 找出所有name属性值以with结尾的DOM
// 例如: name="startswith", name="endswith"
// 这个方法尤其适合通过class属性查找，且不受class包含多个名称的影响：
var icons = $('[class^="icon-"]'); // 找出所有class包含至少一个以`icon-`开头的DOM
// 例如: class="icon-clock", class="abc icon-home"
```
#### 组合查找
组合查找就是把上述简单选择器组合起来使用。如果我们查找$('[name=email]')，很可能把表单外的div name="email"也找出来，但我们只希望查找input，就可以这么写：
```JS
var emailInput = $('input[name=email]'); // 不会找出<div name="email">
```
同样的，根据tag和class来组合查找也很常见：
```JS
var tr = $('tr.red'); // 找出<tr class="red ...">...</tr>
```
#### 多项选择器
多项选择器就是把多个选择器用,组合起来一块选：
```JS
$('p,div'); // 把<p>和<div>都选出来
$('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来
```
要注意的是，选出来的元素是按照它们在HTML中出现的顺序排列的，而且不会有重复元素。例如，<p class="red green">不会被上面的$('p.red,p.green')选择两次。
