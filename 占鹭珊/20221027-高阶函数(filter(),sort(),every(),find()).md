+ filter():不会改变原数组，创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素,把原数组中的某些元素过滤掉，然后返回剩下的未被过滤掉的元素。
```JS
let arr=[12,1,56,78];
        let newarr=arr.filter(function get(x){return x>50});
        console.log(newarr);//控制台输出56，78

```
+ sort()：该方法会改变原数组,能够根据一定条件对数组元素进行排序。如果调用 sort() 方法时没有传递参数，则按字母顺序对数组中的元素进行排序。
```JS
let arr=[1,23,31,45,56,67,78,88,98,101];
        //sort()中没有参数排序,按照字符的Unicode位点进行排序
        arr.sort();
        console.log(arr);//输出[1,101,23,31,45,56,67,78,88,98]
        //sort(a,b),返回a-b,小到大排序，升序
        arr.sort(function(a,b){
            return a-b;
        });
        console.log(arr);
       //sort(b,a),返回a-b,小到大排序，升序
       arr.sort(function(b,a){
        return a-b;
       });
       console.log(arr);
```
+ every():不会对空数组进行检测,不会改变原始数组,用于检测数组中的所有元素是否都满足指定条件。every()方法会遍历数组的每一项，如果有一项不满足条件,则返回false,剩余的项将不会再执行检测。如果遍历完数组后，每一项都符合条，则返回true。
```JS
  let arr=[1,2,3,4,5,6,7,8];
        let newarr=arr.every(function getdayuo(x){
            return x-60>0 || x-60<0
        });
        console.log(newarr);//控制台输出true
```
+ find()：不会改变数组的原始值，获取数组中符合需求的的第一个元素，并返回第一个元素本身，find() 方法为数组中的每个元素都调用一次函数执行：当数组中的元素在测试条件时返回 true 时, find() 返回符合条件的元素，之后的值不会再调用执行函数，如果没有符合条件的元素返回 undefined， find() 对于空数组，函数是不会执行的。
```JS
 let arr=[1,21,13,4,5,66,7,8];
        let newarr=arr.find(function(x){
            return x>=10
        });
        console.log(newarr);//控制台输出21
```
+ 快捷键
```
ctrl+shift+k ————>删除一行
ctrl+shift+向下 ————>向下复制
```