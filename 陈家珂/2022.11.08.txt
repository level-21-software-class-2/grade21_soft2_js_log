获取值
如果我们获得了一个<input>节点的引用，就可以直接调用value获得对应的用户输入值：

// <input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'


设置值
设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：

// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新


提交表单
<!-- HTML -->
<form id="test-form" onsubmit="return checkForm()">
    <input type="text" name="test">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    return true;
}
</script>