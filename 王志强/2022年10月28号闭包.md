## 闭包
1. 闭包是一个函数
2. 闭包是个被函数返回的函数(闭包并不一定需要被返回)
3. 闭包是个被函数A返回的函数B，并且B函数使用了A函数的参数或者局部变量，我们称之为闭包。
```
 function fn(y){
    let x = y || 10 ;
    let inc = {
          newintc:function(){
               console.log(x);
          }        
    }
    return inc;
}
let fnn = fn(12);
fnn.newintc();
```
## 闭包的作用
1. 是将局部变量转换为全局变量。  
2. 用来封装私有变量，在内存中维持一个变量但是用都多了占内存