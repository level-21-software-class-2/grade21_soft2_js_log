## filter
filter用于把数组的某些元素过滤掉，然后返回剩下的元素  
和map()类似，数组的filter()也接收一个函数，和map()不同的是，filter()把  
传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素
删掉偶数保留奇数
```
var arr = [1,2,4,5,6,9,10,15];
var r = arr.filter(function(x){
    return x % 2 !==0;
});
r;//[1,5,9,15]
```
回调函数
filter()接收的回调函数，其实可以有多个参数，通常我们仅使用第一个参数，表示  
数组的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身
```
var arr = ['A','B','C'];
var r = arr.filter(function(element,index,self{
    console.log(element);//依次打印'A','B','C'
    console.log(index);//依次打印0,1,2
    console.log(self);//self就是变量arr
    return true;
}))
```
## sort(排序算法)
对于两个元素x和y，如果认为x<y，则返回-1  
如果认为x==y，则返回0，  
如果认为x>y，则返回1
数字大小排序
```
var arr = [10,20,1,2]
arr.sort(function(x,y){
    if(x<y){
        return -1;
    }
    if(x>y){
        return 1;
    }
    return 0;
});
console.log(arr);//[1,2,10,20]
```
## every
every()方法可以判断数组的所有元素是否满足测试条件
```
var arr = ['aBc','def','ghI']
console.log(arr.every(function(s){
    return s.length>0
}))//true,因为每个元素都满足s.length>0

console.log(arr.every(function(s){
    return s.tolowerCase() === s;
}))//false,因为不是每个元素都全是小写
```
## find
find()方法用于查找符合条件的第一个元素，如果找到了  
返回这个元素，不会再往下查找，没找到undefined
## findIndex
findIndex()和find()类似，也是查找符合条件的第一个  
元素，不同在于findIndex()会返回索引，没找到返回-1