## 操作表单
HTML表单的输入控件主要有以下几种：
```
文本框，对应的<input type="text">，用于输入文本
口令框，对应的<input type="password">，用于输入口令；
单选框，对应的<input type="radio">，用于选择一项；
复选框，对应的<input type="checkbox">，用于选择多项；
下拉框，对应的<select>，用于选择一项；
```
1. 获取值
如果我们获得了一个<input>节点的引用，就可以直接调用value获得对应的用户输入值：
```
// <input type="text" id="email">
var input = document.getElementById('email');
input.value; // '用户输入的值'
```
这种方式可以应用于text、password、hidden以及select。但是，对于单选框和复选框，
value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：
```
// <label><input type="radio" name="weekday" id="monday" value="1"> Monday</label>
// <label><input type="radio" name="weekday" id="tuesday" value="2"> Tuesday</label>
var mon = document.getElementById('monday');
var tue = document.getElementById('tuesday');
mon.value; // '1'
tue.value; // '2'
mon.checked; // true或者false
tue.checked; // true或者false
```
2. 设置值
与获取值类似
```
// <input type="text" id="email">
var input = document.getElementById('email');
input.value = 'test@example.com'; // 文本框的内容已更新
```
3. HTML5控件
HTML5新增了大量标准控件，常用的包括date、datetime、datetime-local、color等，它们都使用<input>标签
4. 提交表单
表单提交的两种常见方式：
方式一是通过<form>元素的submit()方法提交一个表单，例如，响应一个<button>的click事件，在JavaScript代码中提交表单：
```
<!-- HTML -->
<form id="test-form">
    <input type="text" name="test">
    <button type="button" onclick="doSubmitForm()">Submit</button>
</form>

<script>
function doSubmitForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 提交form:
    form.submit();
}
</script>
```
第二种方式是响应<form>本身的onsubmit事件，在提交form时作修改：
```
<!-- HTML -->
<form id="test-form" onsubmit="return checkForm()">
    <input type="text" name="test">
    <button type="submit">Submit</button>
</form>

<script>
function checkForm() {
    var form = document.getElementById('test-form');
    // 可以在此修改form的input...
    // 继续下一步:
    return true;
}
</script>
```