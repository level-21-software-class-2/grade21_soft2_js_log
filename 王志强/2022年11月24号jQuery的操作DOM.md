# jQuery的操作DOM

## 修改text和html

jQuery对象的text()和html()方法分别获取节点的文本和原始HTML文本

```js
//HTML结构
<ul id="test-ul">
    <li class="js">JavaScript</li>
    <li name="book">Java &amp; JavaScript</li>
</ul>
//分别获取文本和HTML
 console.log($('#test-ul [name=book]').text());//Java & JavaScript
 console.log($('#test-ul [name=book]').html());//Java &amp; JavaScript
```

## 修改CSS

```js
//HTML结构
<ul id="test-css">
    <li class="lang dy"><span>JavaScript</span></li>
    <li class="lang"><span>Java</span></li>
    <li class="lang dy"><span>Python</span></li>
    <li class="lang"><span>Swift</span></li>
    <li class="lang dy"><span>Scheme</span></li>
</ul>
//把class="lang dy"的CSS样式背景颜色换成红色
$('.dy').css('background-color','red')

var div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性
```

## 隐藏和显示DOM

可以用hide()和show()方法分别隐藏和显示DOM

## 获取DOM信息

```js
// 浏览器可视窗口大小:
$(window).width(); // 800
$(window).height(); // 600

// HTML文档大小:
$(document).width(); // 800
$(document).height(); // 3500

// 某个div的大小:
var div = $('#test-div');
div.width(); // 600
div.height(); // 300
div.width(400); // 设置CSS属性 width: 400px，是否生效要看CSS是否有效
div.height('200px'); // 设置CSS属性 height: 200px，是否生效要看CSS是否有效
attr()和removeAttr()方法用于操作DOM节点的属性：

//prop()方法和attr()类似，但是HTML5规定有一种属性在DOM节点中可以没有值，只有出现与不出现两种，例如：
<input id="test-radio" type="radio" name="test" checked value="1">
等价于：
<input id="test-radio" type="radio" name="test" checked="checked" value="1">

//attr()和prop()对于属性checked处理有所不同：
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
```
