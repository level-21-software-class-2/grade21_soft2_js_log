# 箭头函数
```js
x => x * x
上面的箭头函数相当于：

function (x) {
    return x * x;
}
```