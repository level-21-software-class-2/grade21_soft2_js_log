有些时候，多个异步任务是为了容错。比如，同时向两个URL读取用户的个人信息，只需要获得先返回的结果即可。这种情况下，用Promise.race()实现：
```js
var p1 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 500, 'P1');
});
var p2 = new Promise(function (resolve, reject) {
    setTimeout(resolve, 600, 'P2');
});
Promise.race([p1, p2]).then(function (result) {
    console.log(result); // 'P1'
});
```


另外今天的小收获
```js
//产生一个[0，1)之间的随机数。
Math.random()：
 
//返回指定范围的随机数(m-n之间)的公式:
Math.random()*(n-m)+m；
或者
Math.random()*(n+1-m)+m
```