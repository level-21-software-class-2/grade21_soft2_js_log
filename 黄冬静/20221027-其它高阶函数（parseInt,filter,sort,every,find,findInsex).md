# parseInt()
* parseInt的参数？
parseInt(string，radix）
parseInt的参数包含两个，一个是字符串string，另一个是被解析值的进制radix

string 若传入的string不是字符串，则会默认使用tostring()函数来将传入的内容转化为字符串。
radix 该参数可以选择传入（其默认值为10），其值应在2~36之间。

* 使用方法
1. 基本用法（不传入radix）
此时parseInt的返回值只有两种可能：1、一个十进制整数 2、NaN

2. 字符串以数字开头。
```js
parseInt("123");//123<br>
parsrInt("-123");//-123<br>
```
3. 若字符串前有空格，空格会被去除。
```js
parseInt("  123");//123  (r）<br>
```
4. 字符串转为整数的时候，是一个个字符依次转换，如果遇到不能转为数字的字符，就不再进行下去，返回已经转好的部分。
```js
parseInt("123CSUST")//123<br>
parseInt("123CSUST123")//123 <br>
```
5. 若传入的string不是字符串，则会默认使用tostring()函数来将传入的内容转化为字符串。
若字符串以0x或0X开头则以16进制解析。
若字符串以0开头则以10进制解析。
若传入的为数字，且开头为0，则以八进制解析。
若传入值以0b或0B开头（不加引号），则以二进制解析。
```js
parseInt("0x11");//17
parseInt("011");//11
parseInt(011);//9
parseInt(0b11);//3
```
6. 科学计数法表示的数会被当成字符串来解析
```js
parseInt("0.05");//5
parseInt("5e-2");//5
```
7. 若传入字符串不一数字开头，则返回NaN。
```js
parseInt("CSUST666")//NaN
```
* 进制转换
传入的string将按照radix值进行解析，方法如上。
如果第二个参数不是数值，会被自动转为一个整数。这个整数只有在2到36之间，才能得到有意义的结果，超出这个范围，则返回NaN。如果第二个参数是0、undefined和null，则直接忽略。
```js
parseInt("123",37);//NaN
parseInt("123", null); // 123
parseInt("123", undefined); // 123
parseInt("123", 0); // 123
```
# filter()
1. 定义和用法
filter用于对数组进行过滤。
filter() 方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素。

2. 注意：
filter() 不会对空数组进行检测；不会改变原始数组
3. 返回值：
返回数组，包含了符合条件的所有元素。如果没有符合条件的元素则返回空数组。
```js
实例1. 返回数组nums中所有大于5的元素。

let nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
 
let res = nums.filter((num) => {
  return num > 5;
});
 
console.log(res);  // [6, 7, 8, 9, 10]

实例2. 对数组进行过滤，筛选出年龄大于 18岁的数据

const arr = [
	{
        name: 'tom1',
        age: 23
    },
    {
        name: 'tom2',
        age: 42
    },
    {
        name: 'tom3',
        age: 17
    },
    {
        name: 'tom4',
        age: 13
    },
]
const res = arr.filter(item => item.age > 18);
console.log(res);  //[{name: 'tom1',age: 23},{name: 'tom2',age: 42}]
console.log(arr);
```
# sort()
比较两个对象的大小，但两个对象要指向同一个路径
如果是比较数字的大小，是比较数字的开头
# find()
1. find() 方法返回通过测试（函数内判断）的数组的第一个元素的值。
2. 如果没有符合条件的元素返回 undefined
3. find() 对于空数组，函数是不会执行的。
4. find() 并没有改变数组的原始值。
array.find(function(currentValue, index, arr),thisValue)，其中currentValue为当前项，index为当前索引，arr为当前数组
# findIndex()
订阅专栏
findIndex()方法 返回传入一个测试条件（函数）符合条件的数组第一个元素位置

findIndex()方法为数组中的每个元素都调用一次函数执行：

            当数组中的元素在测试条件时返回true时，findIndex()返回符合条件的元素的索引位置，

之后的值不会再调用执行函数

            如果没有符合条件的元素返回-1

注意：findIndex()对于空数组，函数时不会执行的

          findIndex()并没有改变数组的原始值
```js
arr.findIndex function(currentValue,index,arr ),thisValue  
```
currentValue    必需，当前元素

index   可选   当前元素的索引

arr   可选   当前元素所属的数组对象

thisValue   可选  传递给函数的值一般用this值。如果这个参数为空，undefined会传递给this值
# every()
every()方法用于检测数组中的所有元素是否都满足指定条件。
every()方法会遍历数组的每一项，如果有一项不满足条件，则返回false,剩余的项将不会再执行检测。
如果遍历完数组后，每一项都符合条，则返回true。 