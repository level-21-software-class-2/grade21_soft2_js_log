# 一、filter过滤器

filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素。

和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。

例如，在一个Array中，删掉偶数，只保留奇数，可以这么写：

```javascript
var arr = [1, 2, 4, 5, 6, 9, 10, 15];
var r = arr.filter(function (x) {
    return x % 2 !== 0;
});
r; // [1, 5, 9, 15]
```

把一个Array中的空字符串删掉，可以这么写：

```javascript
var arr = ['A', '', 'B', null, undefined, 'C', '  '];
var r = arr.filter(function (s) {
    return s && s.trim(); // 注意：IE9以下的版本没有trim()方法
});
r; // ['A', 'B', 'C']
```

可见用filter()这个高阶函数，关键在于正确实现一个“筛选”函数。

## 回调函数

filter()接收的回调函数，其实可以有多个参数。通常我们仅使用第一个参数，表示Array的某个元素。回调函数还可以接收另外两个参数，表示元素的位置和数组本身：

```javascript
var arr = ['A', 'B', 'C'];
var r = arr.filter(function (element, index, self) {
    console.log(element); // 依次打印'A', 'B', 'C'
    console.log(index); // 依次打印0, 1, 2
    console.log(self); // self就是变量arr
    return true;
});
```

利用filter，可以巧妙地去除Array的重复元素：

```javascript
'use strict';

var
    r,
    arr = ['apple', 'strawberry', 'banana', 'pear', 'apple', 'orange', 'orange', 'strawberry'];
r = arr.filter(function (element, index, self) {
    return self.indexOf(element) === index;
});

console.log(r.toString());
```

去除重复元素依靠的是indexOf总是返回第一个元素的位置，后续的重复元素位置与indexOf返回的位置不相等，因此被filter滤掉了。

## 练习

请尝试用filter()筛选出素数：

```javascript
'use strict';

function get_primes(arr) {
        return arr.filter(function(x){
            var a = x;
            var c = 0;
            for (var b = 2; b < a; b++) {
                if (a % b !== 0) {
                    c++;
                };
            };
            if(c === (a - 2)) {
                return true;
            }else {
                return false;
            }
        });
    }
// 测试:
var
    x,
    r,
    arr = [];
for (x = 1; x < 100; x++) {
    arr.push(x);
}
r = get_primes(arr);
if (r.toString() === [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97].toString()) {
    console.log('测试通过!');
} else {
    console.log('测试失败: ' + r.toString());
}
```

# 二、sort排序函数

## 排序算法

排序也是在程序中经常用到的算法。无论使用冒泡排序还是快速排序，排序的核心是比较两个元素的大小。如果是数字，我们可以直接比较，但如果是字符串或者两个对象呢？直接比较数学上的大小是没有意义的，因此，比较的过程必须通过函数抽象出来。通常规定，对于两个元素x和y，如果认为x < y，则返回-1，如果认为x == y，则返回0，如果认为x > y，则返回1，这样，排序算法就不用关心具体的比较过程，而是根据比较结果直接排序。

JavaScript的Array的sort()方法就是用于排序的，但是排序结果可能让你大吃一惊：

```javascript
// 看上去正常的结果:
['Google', 'Apple', 'Microsoft'].sort(); // ['Apple', 'Google', 'Microsoft'];

// apple排在了最后:
['Google', 'apple', 'Microsoft'].sort(); // ['Google', 'Microsoft", 'apple']

// 无法理解的结果:
[10, 20, 1, 2].sort(); // [1, 10, 2, 20]
```

第二个排序把apple排在了最后，是因为字符串根据ASCII码进行排序，而小写字母a的ASCII码在大写字母之后。

第三个排序结果是什么鬼？简单的数字排序都能错？

这是因为Array的sort()方法默认把所有元素先转换为String再排序，结果'10'排在了'2'的前面，因为字符'1'比字符'2'的ASCII码小。

## douwo

如果不知道sort()方法的默认排序规则，直接对数字排序，绝对栽进坑里！

幸运的是，sort()方法也是一个高阶函数，它还可以接收一个比较函数来实现自定义的排序。

要按数字大小排序，我们可以这么写：

```javascript
'use strict';

var arr = [10, 20, 1, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return -1;
    }
    if (x > y) {
        return 1;
    }
    return 0;
});
console.log(arr); // [1, 2, 10, 20]
```

 如果要倒序排序，我们可以把大的数放前面：

```javascript
var arr = [10, 20, 1, 2];
arr.sort(function (x, y) {
    if (x < y) {
        return 1;
    }
    if (x > y) {
        return -1;
    }
    return 0;
}); // [20, 10, 2, 1]
```

默认情况下，对字符串排序，是按照ASCII的大小比较的，现在，我们提出排序应该忽略大小写，按照字母序排序。要实现这个算法，不必对现有代码大加改动，只要我们能定义出忽略大小写的比较算法就可以：

```javascript
var arr = ['Google', 'apple', 'Microsoft'];
arr.sort(function (s1, s2) {
    x1 = s1.toUpperCase();
    x2 = s2.toUpperCase();
    if (x1 < x2) {
        return -1;
    }
    if (x1 > x2) {
        return 1;
    }
    return 0;
}); // ['apple', 'Google', 'Microsoft']
```

忽略大小写来比较两个字符串，实际上就是先把字符串都变成大写（或者都变成小写），再比较。

从上述例子可以看出，高阶函数的抽象能力是非常强大的，而且，核心代码可以保持得非常简洁。

最后友情提示，sort()方法会直接对Array进行修改，它返回的结果仍是当前Array：

```javascript
var a1 = ['B', 'A', 'C'];
var a2 = a1.sort();
a1; // ['A', 'B', 'C']
a2; // ['A', 'B', 'C']
a1 === a2; // true, a1和a2是同一对象
```

# 三、Array

对于数组，除了map()、reduce、filter()、sort()这些方法可以传入一个函数外，Array对象还提供了很多非常实用的高阶函数。

## every

every()方法可以判断数组的所有元素是否满足测试条件。

例如，给定一个包含若干字符串的数组，判断所有字符串是否满足指定的测试条件：

```javascript
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.every(function (s) {
    return s.length > 0;
})); // true, 因为每个元素都满足s.length>0

console.log(arr.every(function (s) {
    return s.toLowerCase() === s;
})); // false, 因为不是每个元素都全部是小写
```



## find

find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined：

```javascript
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.find(function (s) {
    return s.toLowerCase() === s;
})); // 'pear', 因为pear全部是小写

console.log(arr.find(function (s) {
    return s.toUpperCase() === s;
})); // undefined, 因为没有全部是大写的元素
```



## findIndex

findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1：

```javascript
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); // 1, 因为'pear'的索引是1

console.log(arr.findIndex(function (s) {
    return s.toUpperCase() === s;
})); // -1
```



## forEach

forEach()和map()类似，它也把每个元素依次作用于传入的函数，但不会返回新的数组。forEach()常用于遍历数组，因此，传入的函数不需要返回值：

```javascript
'use strict';
var arr = ['Apple', 'pear', 'orange'];
arr.forEach(console.log); // 依次打印每个元素
```

# 今日份快乐代码练习

```javascript
'use strict'
//fillter过滤器
let cityList =[
    { province: '北京', jc: '京', area: '华北地区', city: '北京市' },
    { province: '天津', jc: '津', area: '华北地区', city: '天津市' },
    { province: '河北省', jc: '冀', area: '华北地区', city: '石家庄市' },
    { province: '山西省', jc: '晋', area: '华北地区', city: '太原市' },
    { province: '内蒙古自治区', jc: '内蒙古', area: '华北地区', city: '呼和浩特市'},
    { province: '辽宁省', jc: '辽', area: '东北地区', city: '沈阳市'},
    { province: '吉林省', jc: '吉', area: '东北地区', city: '长春市'},
    { province: '黑龙江省', jc: '黑', area: '东北地区', city: '哈尔滨市'},
    { province: '上海', jc: '沪', area: '华东地区', city: '上海市' },
    { province: '江苏省', jc: '苏', area: '华东地区', city: '南京市'},
    { province: '浙江省', jc: '浙', area: '华东地区', city: '杭州市'},
    { province: '安徽省', jc: '皖', area: '华东地区', city: '合肥市' },
    { province: '福建省', jc: '闽', area: '华东地区', city: '福州市'},
    { province: '江西省', jc: '赣', area: '华中地区', city: '南昌市' },
    { province: '山东省', jc: '鲁', area: '华东地区', city: '济南市'},
    { province: '河南省', jc: '豫', area: '华中地区', city: '郑州市'},
    { province: '湖北省', jc: '鄂', area: '华中地区', city: '武汉市' },
    { province: '湖南省', jc: '湘', area: '华中地区', city: '长沙市'},
    { province: '广东省', jc: '粤', area: '华南地区', city: '广州市' },
    { province: '广西壮族自治区', jc: '桂', area: '华南地区', city: '南宁市' },
    { province: '海南省', jc: '琼', area: '华南地区', city: '海口市'},
    { province: '重庆', jc: '渝', area: '西南地区', city: '重庆市' },
    { province: '四川省', jc: '川', area: '西南地区', city: '成都市'},
    { province: '贵州省', jc: '黔', area: '西南地区', city: '贵阳市'},
    { province: '云南省', jc: '云', area: '西南地区', city: '昆明市'},
    { province: '西藏自治区', jc: '藏', area: '西南地区', city: '拉萨市'},
    { province: '陕西省', jc: '陕', area: '西北地区', city: '西安市'},
    { province: '甘肃省', jc: '甘', area: '西北地区', city: '兰州市'},
    { province: '青海省', jc: '青', area: '西北地区', city: '西宁市'},
    { province: '宁夏回族自治区', jc: '宁', area: '西北地区', city: '银川市'},
    { province: '新疆维吾尔自治区', jc: '新', area: '西北地区', city: '乌鲁木齐市'},
    { province: '台湾', jc: '台', area: '台港澳地区', city: '台北市'},
    { province: '香港特别行政区', jc: '港', area: '台港澳地区', city: '香港岛'},
    { province: '澳门特别行政区', jc: '澳', area: '台港澳地区', city: '澳门'}
]
//1，找出华东和华北的省份
let r1 = cityList.filter(function (s) {
    return s.area === '华东地区' || s.area === '华北地区';
});
console.log(r1);
//2，找到简称是鄂的身份
let r2 = cityList.filter(function (s) {
    return s.jc === '鄂';
});
console.log(r2);
//3,找到省份首府是乌鲁木齐市或者是福州市的省份
let r3 = cityList.filter(function (s) {
    return s.city === '福州市';
})
console.log(r3);

//array
//判断一台pc主机常见配件：
let pcpj = [
    { name: '机箱', money: 200, scd: '中国生产' ,ppmc:'中国机箱'},
    { name: '显卡', money: 2000, scd: '中国台湾生产' ,ppmc:'台湾显卡'},
    { name: 'cpu', money: 190, scd: '美国生产' ,ppmc:'美国cpu'},
    { name: '散热器', money: 500, scd: '中国生产' ,ppmc:'中国散热器'},
    { name: '内存条', money: 200, scd: '英国生产' ,ppmc:'英国内存条'},
    { name: '固态硬盘', money: 600, scd: '中国生产' ,ppmc:'中国固态硬盘'},
    { name: '电源', money: 200, scd: '俄罗斯生产' ,ppmc:'俄罗斯电源'}
];
//1，是否所有配件都是中国生产
console.log(pcpj.every(function(s){
    return s.scd==='中国生产';
}));
//2，是否所有价格大于等于20块钱
console.log(pcpj.every(function(s){
    return s.money>=20;
}));
//3，查找第一个是中国台湾生产的显卡品牌名称
console.log(pcpj.find(function (s) {
    return s.scd==='中国台湾生产';
})); 
```

