## 正则的声明方法

　　1）var reg = /abc/; "这个叫对象直接量方式"；
　　2）var reg = new RegExp("abc") 这个叫构造函数方式；

 

## 正则方法

1. exec() 方法用于检索字符串中的正则表达式的匹配。 
返回一个数组，其中存放匹配的结果。如果未找到匹配，则返回值为 null。
　　返回的数组中 第0个元素为 0 匹配到的文本
　　第1个元素为 index 匹配到的文本出现的位置
　　第2个元素为 input 被匹配的字符串
　　第3个元素为 length 匹配到的文本的个数

　　使用while可以匹配多次
　　while (result = reg.exec(str)) {
　　　　console.log(result)
　　}
2. test 方法用于检测一个字符串是否匹配某个模式.
　　reg.test(str) //返回true或者false

3. match()

　　将字符串中匹配的内容捕获出来
　　若加上量词g则一次性捕获所有匹配的内容放到数组里返回，若不加则与exec()的返回值一样
　　用法：string.match(regexp)

　　match 方法可在字符串内检索指定的值，或找到一个或多个正则表达式的匹配。 将字符串中匹配的内容捕获出来
　　var reg = /abc/g;
　　var str = "11abc00abc"
　　console.log(str.match(reg));//返回一个数组

　　注：如果没有g(不是全局匹配) 则与exec类似
　　如果有g(全局匹配) 找到匹配的所有文本返回一个数组

4. replace 方法用于在字符串中用一些字符替换另一些字符，或替换一个与正则表达式匹配的子串。
　　str.replace(reg,"44")
　　返回值是一个被替换过的新的字符串,不改边原来字符串

5. search() 返回值是相匹配的子串的起始位置。 方法不执行全局匹配，它将忽略标志 g。
　　var str="Visit W3School!"
　　document.write(str.search(/W3School/)) // 6

6. split() 字符串的分割 ，返回一个数组。正则可以匹配它的分割方式

 ```js
    let arr = "jhbjhsddd286485sbhvhgsd6584sahgskuy5"
    arr = arr.replace(/[^0-9]/ig,"")
    console.log(arr*1);


    let arr = "aaabbbbccc"
    arr = arr.replace(/(.).*\1/g,"$1")
    console.log(arr);
 ```