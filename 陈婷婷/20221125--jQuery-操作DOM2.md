# 获取DOM信息

prop()方法和attr()类似，但是HTML5规定有一种属性在DOM节点中可以没有值，只有出现与不出现两种，例如：

```js
<input id="test-radio" type="radio" name="test" checked value="1">
等价于：

<input id="test-radio" type="radio" name="test" checked="checked" value="1">
attr()和prop()对于属性checked处理有所不同：

var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
prop()返回值更合理一些。不过，用is()方法判断更好：

var radio = $('#test-radio');
radio.is(':checked'); // true
```

类似的属性还有selected，处理时最好用is(':selected')。


## 获取值
如果我们获得了一个<input>节点的引用，就可以直接调用value获得对应的用户输入值：
```js
var input = doc.get('aaa');
input.value；//用户输入的值
```

这种方式可以应用于text、password、hidden以及select。但是，对于单选框和复选框，value属性返回的永远是HTML预设的值，而我们需要获得的实际是用户是否“勾上了”选项，所以应该用checked判断：

```js
var m =doc.get('aaa');
var t=doc.get('bbb');
m.value;
t.value;
m.checked;//true或者false
t.checked;//true或者false
```

## 设置值
设置值和获取值类似，对于text、password、hidden以及select，直接设置value就可以：

```js
var input = doc.get('aaa')
input.value='aaa@qq.com';//文本框内容已更新
```
对于单选框和复选框，设置checked为true或false即可。


# 操作表单

对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性：

/*
    <input id="test-input" name="email" value="">
    <select id="test-select" name="city">
        <option value="BJ" selected>Beijing</option>
        <option value="SH">Shanghai</option>
        <option value="SZ">Shenzhen</option>
    </select>
    <textarea id="test-textarea">Hello</textarea>
*/

```js
var
    input = $('#test-input'),
    select = $('#test-select'),
    textarea = $('#test-textarea');

input.val(); // 'test'
input.val('abc@example.com'); // 文本框的内容已变为abc@example.com

select.val(); // 'BJ'
select.val('SH'); // 选择框已变为Shanghai

textarea.val(); // 'Hello'
textarea.val('Hi'); // 文本区域已更新为'Hi'
```
可见，一个val()就统一了各种输入框的取值和赋值的问题。