# 数组

## filter过滤器

filter也是一个常用的操作，它用于把Array的某些元素过滤掉，然后返回剩下的元素。

和map()类似，Array的filter()也接收一个函数。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是true还是false决定保留还是丢弃该元素。

例子：

```js
let arr=[1,2,3,4,5,6,89,98];
let newArr = arr.filter(function(x){
    return x % 1 ! ==0;
})
console.log(arr);
console.log(newArr);
console.log(stus.filter(function(x){
    return x.age>20;
}
));
```

## sort

## 排序算法

排序也是在程序中经常用到的算法。无论使用冒泡排序还是快速排序，排序的核心是比较两个元素的大小。如果是数字，我们可以直接比较，但如果是字符串或者两个对象呢？直接比较数学上的大小是没有意义的，因此，比较的过程必须通过函数抽象出来。通常规定，对于两个元素x和y，如果认为x < y，则返回-1，如果认为x == y，则返回0，如果认为x > y，则返回1，这样，排序算法就不用关心具体的比较过程，而是根据比较结果直接排序。

例子：

```js
let stus=[{
    name:'aaa'
    aeg:18
    },{
        name:'bbb'
    aeg:18
    },{
        name:'ccc'
    aeg:18
    },{
        name:'ddd'
    aeg:18
    }
    ,{
        name:'eee'
    aeg:18
    }
    ,{
        name:'fff'
    aeg:18
}]
console.log(arr.sort(function|(x,y){
    return y - x;
}));
```

## every 

every()方法可以判断数组的所有元素是否满足测试条件。

循环，设置一个标识符true，只有不符合条件，就设置false，然后退出循环。

```js
let allIsnUMBER = true;
arr.forEach(x=>{
     if(typeOf(x!=='number'){
         allIsNumber=false;
         return;
     }
     })
     console.log(allIsNumber);

let isOK = arr.every(function(x){
    retrun typeof(x) ==='number';
})
console.log(isOK);
```

## find

find()方法用于查找符合条件的第一个元素，如果找到了，返回这个元素，否则，返回undefined：

```js
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.find(function (s) {
    return s.toLowerCase() === s;
})); // 'pear', 因为pear全部是小写

console.log(arr.find(function (s) {
    return s.toUpperCase() === s;
})); // undefined, 因为没有全部是大写的元素
```

## findIndex

findIndex()和find()类似，也是查找符合条件的第一个元素，不同之处在于findIndex()会返回这个元素的索引，如果没有找到，返回-1：

```js
'use strict';
var arr = ['Apple', 'pear', 'orange'];
console.log(arr.findIndex(function (s) {
    return s.toLowerCase() === s;
})); // 1, 因为'pear'的索引是1

console.log(arr.findIndex(function (s) {
    return s.toUpperCase() === s;
})); // -1
```

## forEach

forEach()和map()类似，它也把每个元素依次作用于传入的函数，但不会返回新的数组。forEach()常用于遍历数组，因此，传入的函数不需要返回值：


```js
'use strict';
var arr = ['Apple', 'pear', 'orange'];
arr.forEach(console.log); // 依次打印每个元素
```