# Canvas

## 创建画布

使用属性节点设置宽高，画布的默认大小是 300*150，如果通过 css 放大画布会连同画布中所有内容一起放大。所以不要通过 CSS 去设置画布的大小。一个 html 中可以有多个画布。

```js
<canvas width = "500" height = "500" id = "cvs"></canvas>　
```
## 设置画笔

canvas 不像 svg 通过 html 属性画图，而是通过 js ，步骤：

1.添加 canvas 元素

2.在 js 中获取到 canvas 元素节点 

3.获取到上下文创建 context 对象    

4.使用 js 绘制

## 绘制直线

首先需要创建上下文 context 对象：

```js
let cvs = document.getElementById("canvas");// 获取元素
let ctx = cvs.getContext("2d"); // 创建上下文 context 对象 便于理解也可以取名叫 pen
```
　　ctx = cvs.getContext("2d") 没有 3d 的写法，如果想要开启 3D 世界的大门，则可以写 canvas.getContext('webgl')。然而 WebGL 是基于 OpenGL ES 2.0 的一套标准，在此不做讨论。

指定画笔移动到某一点，然后告诉画笔需要从 a 点画到 b 点。可以让画笔多次移动、绘制，最后统一输出到屏幕上 stroke()

```js
// 第一条线
ctx.moveTo(10, 10); // 落笔点
ctx.lineTo(50, 10); // 从落笔点画一条线（路径）到指定位置
// 第二条线
ctx.moveTo(10, 20);
ctx.lineTo(50, 20);
// 描绘
ctx.stroke();   // 通过路径列表描边
```
## 线段颜色 strokeStyle

```js
ctx.moveTo(20, 20);
ctx.lineTo(100, 20);
ctx.moveTo(20, 50);
ctx.lineTo(100, 50);
ctx.strokeStyle = "red";
ctx.stroke();　
```

## 线段的宽度 lineWidth

```js
ctx.moveTo(10, 10);
ctx.lineTo(50, 10);
ctx.lineWidth = "10";   // 设置线段宽度
ctx.stroke();
```
## 线条交界处的样式

线条交界处的样式使用 lineJoin 控制，它的值：

1.miter [ˈmaitə] 直角 默认
2.round 圆角
3.bevel [ˈbevl] 斜切角

```js
ctx.moveTo(100, 100);
ctx.lineTo(200, 100);
ctx.lineTo(50, 150);
ctx.lineWidth = 10;
ctx.lineJoin = "round";
ctx.stroke();
```
## 线条转折样式 lineCap

线帽，控制线条转折样式：
 
1. butt 默认值
2. square 方形
3. round　

# 绘制文本

绘制文本就是在指定的位置输出文本，可以设置文本的字体、样式、阴影等，与CSS完全一致：

```js
'use strict';

var
    canvas = document.getElementById('test-text-canvas'),
    ctx = canvas.getContext('2d');
ctx.clearRect(0, 0, canvas.width, canvas.height);
ctx.shadowOffsetX = 2;
ctx.shadowOffsetY = 2;
ctx.shadowBlur = 2;
ctx.shadowColor = '#666666';
ctx.font = '24px Arial';
ctx.fillStyle = '#333333';
ctx.fillText('带阴影的文字', 20, 40);
```

# AJAX

AJAX不是JavaScript的规范，它只是一个哥们“发明”的缩写：Asynchronous JavaScript and XML，意思就是用JavaScript执行异步网络请求。

在现代浏览器上写AJAX主要依靠XMLHttpRequest对象：

```js
'use strict';
function success(text) {
    var textarea = document.getElementById('test-response-text');
    textarea.value = text;
}

function fail(code) {
    var textarea = document.getElementById('test-response-text');
    textarea.value = 'Error code: ' + code;
}

var request = new XMLHttpRequest(); // 新建XMLHttpRequest对象

request.onreadystatechange = function () { // 状态发生变化时，函数被回调
    if (request.readyState === 4) { // 成功完成
        // 判断响应结果:
        if (request.status === 200) {
            // 成功，通过responseText拿到响应的文本:
            return success(request.responseText);
        } else {
            // 失败，根据响应码判断失败原因:
            return fail(request.status);
        }
    } else {
        // HTTP请求还在继续...
    }
}

// 发送请求:
request.open('GET', '/api/categories');
request.send();

alert('请求已发送，请等待响应...');
响应结果：

对于低版本的IE，需要换一个ActiveXObject对象：

'use strict';
function success(text) {
    var textarea = document.getElementById('test-ie-response-text');
    textarea.value = text;
}

function fail(code) {
    var textarea = document.getElementById('test-ie-response-text');
    textarea.value = 'Error code: ' + code;
}

var request = new ActiveXObject('Microsoft.XMLHTTP'); // 新建Microsoft.XMLHTTP对象

request.onreadystatechange = function () { // 状态发生变化时，函数被回调
    if (request.readyState === 4) { // 成功完成
        // 判断响应结果:
        if (request.status === 200) {
            // 成功，通过responseText拿到响应的文本:
            return success(request.responseText);
        } else {
            // 失败，根据响应码判断失败原因:
            return fail(request.status);
        }
    } else {
        // HTTP请求还在继续...
    }
}

// 发送请求:
request.open('GET', '/api/categories');
request.send();

alert('请求已发送，请等待响应...');
```
