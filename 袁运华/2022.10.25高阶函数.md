# 高阶函数
一个函数就接收另一个函数作为参数称之为高阶函数
~~~JavaScript
    function A(){}

    function B(x, y, A){}
    // 函数B可以接收函数A作为参数, 函数B被称为高阶函数
~~~




# Map
`Map()` 这个方法使数组里的每一个元素都执行一次方法里面的函数，并将结果放入一个新的数组

**函数的三个参数**  
`currentValue` : callbackFn 数组中正在处理的当前元素。  
`index` : callbackFn 数组中正在处理的当前元素的索引。  
`array` : map 方法调用的数组。  


# Reduce
`Reduce()` 方法对数组中的每个元素按序执行一个由您提供的 reducer 函数，每一次运行 reducer 会将先前元素的计算结果作为参数传入，最后将其结果汇总为单个返回值。

**函数的四个参数：**  
`previousValue` ：上一次调用 callbackFn 时的返回值。在第一次调用时，若指定了初始值 initialValue，其值则为 initialValue，否则为数组索引为 0 的元素 array[0]。  
`currentValue` ：数组中正在处理的元素。在第一次调用时，若指定了初始值 initialValue，其值则为数组索引为 0 的元素 array[0]，否则为 array[1]。  
`currentIndex` ：数组中正在处理的元素的索引。若指定了初始值 initialValue，则起始索引号为 0，否则从索引 1 起始。  
`array` ：用于遍历的数组。



# 练习
1. 将字符串里面的数字拿到数组里
~~~JavaScript
'use strict'

var myString = '13579';
var myArray = [];
var result = myString  * 1;

for (let i = myString.length - 1; i >= 0; i--) {
    myArray[i] = (result % 10)
    result -= result % 10
    result /= 10
}

console.log(myArray);
~~~

2. 把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa','Bart']。
~~~JavaScript
    'use strict'

var myString = ['adam', 'LISA', 'barT'];
var result = myString.map(myFunction)

function myFunction(x){
    x = x.toLowerCase()
    var middle = x[0].toUpperCase();
    for(let i = 1; i < x.length; i++){        
        middle += x[i]
    }
    return middle
}

console.log(result);
~~~

3. ["1", "2", "3"].map(parseInt), 我们期望输出 [1, 2, 3], 而实际结果是 [1, NaN, NaN]. 
~~~JavaScript
'use strict'
// 通常情况下，map 方法中的 callbackFn 函数只需要接受一个参数，就是正在被遍历的数组元素本身。但这并不意味着 map 只给 callbackFn 传了一个参数。
// parseInt 经常被带着一个参数使用，但是这里接受两个。第一个参数是一个表达式而第二个是回调函数的基，map() 方法传递 3 个参数，第三个参数被 parseInt 忽视了，但不是第二个，例：
parseInt("1", 0); // 这个方法接收的是第一个元素的属性和下标，输出结果是 1
parseInt("2", 1); // 这个方法接收的是第二个元素的属性和下班，输出结果是 NaN
parseInt("3", 2); // 这个方法接收的是第三个元素的属性和下班，输出结果是 NaN
// 根据上面的例子，我们知道了只有下标是为 0 的元素输出了属性
// 查看API文档发现 parseInt() 方法有两个参数，第一个(string)是要被解析的值，第二个(radix)是设置几进制，默认十进制，只能输入2~36的整数，否则输出 NaN


// 修改方法: 设置进制
["1", "2", "3"].map((x) => parseInt(x, 10))
~~~