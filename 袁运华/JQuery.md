引入jquery: `<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>`
jquery文档: https://www.runoob.com/manual/jquery/ 或者 https://www.jquery123.com/

# 选择器
`id` 使用 `#` ;  
`class` 使用 `.` ;  
`tag` 直接使用标签名;  
`属性` 只用 `[属性名称 = value]` 中;  

## 组合查找
`$(input[name:email])` 查找标签为 input , name 属性, 值为 email

## 多项查找
`#(p.red, p.green)` 查找标签为 p 并且 class 为 red 或 green 的数据



# DOM操作
`JQuery.text()` 如果输入了内容, 则将内容插入; 如果没有内容, 则返回元素内的所有文本  
`JQuery.html()` 如果输入了标签和内容, 则将标签内容插入; 如果没有内容, 则返回元素内的标签及文本  

`JQuery.hasClass()` 判断 JQuery 元素是否存在某个 class 属性
`JQuery.addClass()` 给 JQuery 元素添加一个 class 属性
`JQuery.removeClass()` 删除 JQuery 元素中的 class 属性

`JQuery.show()` 显示元素
`JQuery.hide()` 隐藏元素

`JQuery.attr()` 判断 JQuery 元素中是否存在某个属性, 如果存在，则会将属性的只返回; 如果不存在, 返回undefined. 如果在属性后在加一个值, 则是将前面的属性替换为新的属性
`JQuery.removeAttr()` 删除 JQuery 元素中的一个属性



## 注意
如果选中的 JQuery 对象内包含另一个对象, 修改 text 的时候会直接将里面的全部内容转化为输入的文本  
~~~js
<body>
    <!-- HTML结构 -->
    <div id="test">
        fjsdlfjsl
        <div id="test1">
            fsdlfjs
        </div>
    </div>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
    <script>
        // 选中所有 div 的元素, 将元素内的文本全部替换为 test
        $('div').text('test');
    </script>
</body>
// 例如上面的例子, 在网页上div只剩一个了: <div id="test">test</div>
~~~



# 层级选择器
## 层级选择器
如果两个DOM元素具有层级关系，就可以用$('ancestor descendant')来选择，层级之间用空格隔开. 例如  
~~~js
    <div class="big">
        <ul class="small">
            <li class="one">这里是1</li>
            <li class="two">这里是2</li>
            <li class="three">这里是3</li>
        </ul>
    </div>

// 如果需要选出 1 , 以下两种都可以获得
$('div.big li.one');
$('ul.small li.one');
~~~


## 子选择器
子选择器$('parent>child')类似层级选择器，但是限定了层级关系必须是父子关系，就是 `<child>` 节点必须是 `<parent>` 节点的直属子节点。
~~~js
// 还是以上面为例, 下面的这个就无法找到需要的元素, 因为两个元素不构成'父子'关系
$('div.big > li.one');
// 而下面这个就可以正常的找到
$('ul.small > li.one');
~~~


## 过滤器
过滤器一般不单独使用，它通常附加在选择器上，帮助我们更精确地定位元素。观察过滤器的效果：
~~~js
$('ul.small li'); // 选出JavaScript、Python和Lua 3个节点

$('ul.small li:first-child'); // 仅选出one
$('ul.small li:last-child'); // 仅选出three
$('ul.small li:nth-child(2)'); // 选出第N个元素，N从1开始
$('ul.small li:even'); // 选出索引为偶数的元素, 从0开始计数
$('ul.small li:odd'); // 选出索引为奇数的元素, 从0开始计数
~~~
其他可以参考文档: https://www.jquery123.com/category/selectors/basic-filter-selectors/  
**注意: even 和 :odd 会无视它们的父元素，而仅仅把元素列表中的元素每隔一个过滤出来。而 :nth-child 则会计算子元素在各自父元素中的索引值.**  


## 特殊的选择器
针对表单元素，jQuery还有一组特殊的选择器：
~~~js
:input：可以选择<input>，<textarea>，<select>和<button>；

:file：可以选择<input type="file">，和input[type=file]一样；

:checkbox：可以选择复选框，和input[type=checkbox]一样；

:radio：可以选择单选框，和input[type=radio]一样；

:focus：可以选择当前输入焦点的元素，例如把光标放到一个<input>上，用$('input:focus')就可以选出；

:checked：选择当前勾上的单选框和复选框，用这个选择器可以立刻获得用户选择的项目，如$('input[type=radio]:checked')；

:enabled：可以选择可以正常输入的<input>、<select> 等，也就是没有灰掉的输入；

:disabled：和:enabled正好相反，选择那些不能输入的。
~~~
此外，jQuery还有很多有用的选择器，例如，选出可见的或隐藏的元素：
~~~js
$('div:visible'); // 所有可见的div
$('div:hidden'); // 所有隐藏的div
~~~



# 查找
`JQuery.find()` 根据某些条件获取需要的子节点  
`JQuery.parent()` 获取当前子节点的上一个节点  
`JQuery.next()` 获取同一层级, 当前节点的上一个节点, 可以添加选择条件  
`JQuery.prev()` 获取同一层级, 当前节点的下一个节点, 可以添加选择条件  



# 过滤
`JQuery.filter()` 根据输入的条件筛选出需要的对象, 可以传入函数  
`JQuery.map()` 根据输入的函数对DOM元素进行操作  
`JQuery.slice()` 类似于数组的 slice 的方法  



# 修改DOM结构
## 添加DOM
`JQuery.append()` 添加一个传入的 HTML 片段, 也可以传入 DOM 对象, JQuery对象和函数对象. 把对象添加到后面  
`JQuery.    end()` 和上面的方法类似, 但是它把对象添加到前面  

`JQuery.after()` 类似上面的 append 方法, 但 after 方法是添加到节点后面, 而 append 方法是添加到子节点里面  
`JQuery.before()` 类似 after 方法, 它将元素添加到节点的后面, prepend 方法是将元素添加到子节点里面  


## 删除DOM
`JQuery.remove()` 删除指定的对象, 如果对象有子节点, 则将子节点一并删除  




# 事件
因为JavaScript在浏览器中以单线程模式运行，页面加载后，一旦页面上所有的JavaScript代码被执行完后，就只能依赖触发事件来执行JavaScript代码。  
下面就演示给 `<a>` 标签绑定事件
~~~js
<body>
    <a id="test-link" href="#">点我试试</a>


    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>
    <script>
        'use strict';

        var a = $('#test-link');

        a.click(function(){
            alert('Hello')
        });

    </script>
</body>
~~~


除了上面演示的, 还有其他的绑定方式
~~~js
a.on('事件类型', 执行函数)
~~~
这个方法和上面的效果是一样的


## 鼠标事件
- click: 鼠标单击时触发；  
- dblclick：鼠标双击时触发；  
- mouseenter：鼠标进入时触发；  
- mouseleave：鼠标移出时触发；  
- mousemove：鼠标在DOM内部移动时触发；  
- hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave。


## 键盘事件
- keydown：键盘按下时触发；
- keyup：键盘松开时触发；
- keypress：按一次键后触发。 其他事件
- focus：当DOM获得焦点时触发；
- blur：当DOM失去焦点时触发；
- change：当 `<input>`、`<select>` 或 `<textarea>` 的内容改变时触发；
- submit：当 `<form>` 提交时触发；
- ready：当页面被载入并且DOM树完成初始化后触发。 其中，ready仅作用于document对象。由于ready事件在DOM完成初始化后触发，且只触发一次，所以非常适合用来写其他的初始化代码。假设我们想给一个 `<form>` 表单绑定submit事件，


## 事件参数
所有事件都会传入 Event 作为参数, 可以从 Event 对象上获取到我们需要的信息; 如: 'pageX' 和 'pageY'



## 事件取消绑定
一个以及绑定的事件可以通过 `off('事件', 函数)` 取消绑定; 直接使用 `off()` 可以直接取消所有绑定的事件, 使用 `off('事件')` 直接取消指定事件

~~~js
// 绑定事件
$('id').click(test);
// 取消绑定
$('id').off('click', test);
~~~

但是有种特殊的情况需要注意
~~~js
// 绑定事件
$('id').click(function(){
    alert('Hello');
});
// 错误的取消绑定
$('id').off('click', function(){
    alert('Hello');
});
// 两个匿名函数虽然看起来相同, 但却是不同的函数对象
~~~


## 事件的触发条件
事件的触发总是由用户操作引发的, 直接用js代码使目标达到事件的条件是不会触发事件的, 但我们可以直接调用的无参的事件来触发事件



## 注意
因为js是从上向下一行一行执行的, 如果将js引用写在叫上面的位置, 就会出现DOM上面还没有加载完成的时候就已经读取完js, 导致js中的选择器绑定不到相应的元素, 所以我们需要在js的 `document` 对象绑定 `ready` 事件, 保证DOM的初始化完成后才会开始执行.  

我们可以使用下面的代码来为 document 添加 ready 事件
~~~js
$(document).on('ready', function (){
    // 函数体
});

// 简化
$(document).ready(function(){
    // 函数体
});

// 这一种写法最常见, 由于ready事件使用非常普遍，所以可以这样简化
$(function(){
    // 函数体
});
~~~



# 动画
## show()/hide()
直接无参形式调用会显示或隐藏DOM元素. 如果输入一个事件参数进去, 他就会变成一个动画, 让DOM元素从右下角开始到左上角逐渐消失/出现, 例如: 
~~~js
$('#id').show(1000);// 经过1秒后显示

$('#id').hide(1000);// 经过1秒后隐藏
~~~


## slideUp()/slideDown()
让DOM元素从下向上逐渐消失/出现, 而 slideToggle() 会根据元素显示/隐藏的状态进行判断
~~~js
$('#id').slideDown(1000);// 经过1秒后显示

$('#id').slideUp(1000);// 经过1秒后隐藏

$('#id').slideToggle(1000);// 如果元素没有隐藏, 则经过1秒后隐藏; 如果隐藏, 则1秒后显示
~~~


## fadeIn()/fadeOut()
让DOM元素逐渐淡出/显示
~~~js
$('#id').fadeIn(1000);// 在1秒内逐渐显示

$('#id').fadeOut(1000);// 在1秒内逐渐淡出        

$('#id').fadeToggle(1000);// 如果元素没有隐藏, 则在1内逐渐后淡出; 如果隐藏, 在1秒内逐渐显示
~~~


## 自定义动画: animate()  
语法: `animate({参数}[, 动画需要的时间[, 动画结束后调用的函数]]);`  
其中的参数就是CSS属性, 下面是 animate() 使用的示例
~~~js
$('#id').animate({
    width: '500px',
    heigth: '250px',
    opacity: 0.5
}, 3000);
~~~


## 串行动画
`delay()` 设置一个延时来推迟执行队列中后续的项。  
~~~js
// 动画效果：slideDown - 暂停 - 放大 - 暂停 - 缩小
$('#id').slideDown(2000)
   .delay(1000)
   .animate({
       width: '256px',
       height: '256px'
   }, 2000)
   .delay(1000)
   .animate({
       width: '128px',
       height: '128px'
   }, 2000);
~~~


## 为什么有的动画没有效果
你可能会遇到，有的动画如slideUp()根本没有效果。这是因为jQuery动画的原理是逐渐改变CSS的值，如height从100px逐渐变为0。但是很多不是block性质的DOM元素，对它们设置height根本就不起作用，所以动画也就没有效果。

此外，jQuery也没有实现对background-color的动画效果，用animate()设置background-color也没有效果。这种情况下可以使用CSS3的transition实现动画效果。

