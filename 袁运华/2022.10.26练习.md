# typeof
用与判断变量的类型：`typeof 需要判断的变量`



测试一：将字符串中的数字提取出来
~~~JavaScript
'use strict'

// 判断字符是否为数字的方法：
// 1. 使用取余运算符：将需要判断的变量%1，如果结果为0，则是数字
// 2. 

var newString = '1a2b3c4.;:,[]{}'

// 使用循环
function myFunction(str){
    var num = '';
    for (let i = 0; i < str.length; i++) {
        if(str[i]%1===0)
            num += str[i]
    }    
    return num * 1
}

console.log(myFunction(newString));


// 使用map
var myArray = newString.split('');
var newArray = myArray.map((str) => { if (str % 1 === 0) { return str } else { return '' } })

console.log(newArray.join('') * 1);
~~~