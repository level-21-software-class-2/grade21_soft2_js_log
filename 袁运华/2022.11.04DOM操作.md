# 更新DOM
拿到一个DOM节点后，我们可以对它进行更新。

可以直接修改节点的文本，方法有两种：
1. 一种是修改innerHTML属性，这个方式非常强大，不但可以修改一个DOM节点的文本内容，还可以直接通过HTML片段修改DOM节点内部的子树; 用innerHTML时要注意，是否需要写入HTML。如果写入的字符串是通过网络拿到了，要注意对字符编码来避免XSS攻击。
2. 第二种是修改innerText或textContent属性，这样可以自动对字符串进行HTML编码，保证无法设置任何HTML标签; 两者的区别在于读取属性时，innerText不返回隐藏元素的文本，而textContent返回所有文本。



# 插入DOM
当我们获得了某个DOM节点，想在这个DOM节点内插入新的DOM，应该如何做？

如果这个DOM节点是空的, 例如，`<div></div>` ，那么，直接使用innerHTML = 'child'就可以修改DOM节点的内容，相当于“插入”了新的DOM节点。

如果这个DOM节点不是空的，那就不能这么做，因为innerHTML会直接替换掉原来的所有子节点。
1. 有两个办法可以插入新的节点。一个是使用appendChild，把一个子节点添加到父节点的最后一个子节点。



# 练习
~~~js
<body>
    <!-- HTML结构 -->
    <div id="test-div">
        <p id="test-js">javascript</p>
        <p>Java</p>
    </div>

    <script>
        'use strict';
        // 获取<p>javascript</p>节点:
        let js = document.getElementById('test-js');

        // 修改文本为JavaScript:
        js.innerHTML = 'JavaScript'

        // 修改CSS为: color: #ff0000, font-weight: bold
        js.style.color = "#ff0000";
        js.style.fontWeight = "bold";

        // 测试:
        if (js && js.parentNode && js.parentNode.id === 'test-div' && js.id === 'test-js') {
            if (js.innerText === 'JavaScript') {
                if (js.style && js.style.fontWeight === 'bold' && (js.style.color === 'red' || js.style.color === '#ff0000' || js.style.color === '#f00' || js.style.color === 'rgb(255, 0, 0)')) {
                    console.log('测试通过!');
                } else {
                    console.log('CSS样式测试失败!');
                }
            } else {
                console.log('文本测试失败!');
            }
        } else {
            console.log('节点测试失败!');
        }
    </script>
    <!-- <script src="b.js"></script> -->
</body>
~~~

~~~js
'use strict'
console.log('================这里是测试页面==============');

// 获取页面中类名为lang的所有标签
let str = document.getElementsByClassName('lang')

// 创建一个排序函数
function sort(x) {
    // 使用冒泡排序
    let middle;
    for (let i = 0; i < x.length; i++) {
        for (let j = 0; j < x.length - i - 1; j++) {
            // 比较元素内第一个字母的大小并进行交换
            if (x[j].innerText.substring(0, 1) > x[j + 1].innerText.substring(0, 1)) {
                middle = x[j].innerHTML;
                x[j].innerHTML = x[j + 1].innerHTML;
                x[j + 1].innerHTML = middle;
            }
        }
    }
    return x;
}
// 执行函数
sort(str);


// 测试:
; (function () {
    var arr, i, t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 5) {
        arr = [];
        for (i = 0; i < t.children.length; i++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['Haskell', 'JavaScript', 'Python', 'Ruby', 'Scheme'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();   
~~~

~~~js
'use strict';
// 获取父节点
let parent = document.getElementById('test-list')

for (let i = 0; i < parent.children.length; i++) {
    // 判断子节点是否为需要内容
    if(!(parent.children[i].innerText === 'JavaScript' || parent.children[i].innerText === 'CSS' || parent.children[i].innerText === 'HTML')){
        parent.removeChild((parent.children[i]));
        i--;
    }    
}


// 测试:
;(function () {
    var
        arr, i,
        t = document.getElementById('test-list');
    if (t && t.children && t.children.length === 3) {
        arr = [];
        for (i = 0; i < t.children.length; i ++) {
            arr.push(t.children[i].innerText);
        }
        if (arr.toString() === ['JavaScript', 'HTML', 'CSS'].toString()) {
            console.log('测试通过!');
        }
        else {
            console.log('测试失败: ' + arr.toString());
        }
    }
    else {
        console.log('测试失败!');
    }
})();
~~~

~~~js
'use strict';
// 选择<p>JavaScript</p>:
var js = document.getElementById('test-p');

// 选择<p>Python</p>,<p>Ruby</p>,<p>Swift</p>:
var arr = document.getElementsByClassName('c-red c-green')[0].children;

// 选择<p>Haskell</p>:
var haskell = document.getElementsByClassName('c-green')[1].children[1];

// 测试:
if (!js || js.innerText !== 'JavaScript') {
    alert('选择JavaScript失败!');
} else if (!arr || arr.length !== 3 || !arr[0] || !arr[1] || !arr[2] || arr[0].innerText !== 'Python' || arr[1].innerText !== 'Ruby' || arr[2].innerText !== 'Swift') {
    console.log('选择Python,Ruby,Swift失败!');
} else if (!haskell || haskell.innerText !== 'Haskell') {
    console.log('选择Haskell失败!');
} else {
    console.log('测试通过!');
}
~~~