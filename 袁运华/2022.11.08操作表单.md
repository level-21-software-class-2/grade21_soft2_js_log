表单提交常见的两种方式
1. from 的原始提交，即给表单的action指定提交的路径，然后将按钮的类型修改为submit 类型，当点击这个按钮的时候，表单就会提交
2. 利用js 函数进行提交，即去掉表单里的action路径，然后按钮的类型修改为普通的button， 设置按钮的oncilk

# 练习
~~~js
<body>
    <!-- HTML结构 -->
    <form id="test-register" action="#" target="_blank" onsubmit="return checkRegisterForm()">
        <p id="test-error" style="color:red"></p>
        <p>
            用户名: <input type="text" id="username" name="username" >
        </p>
        <p>
            口令: <input type="password" id="password" name="password" >
        </p>
        <p>
            重复口令: <input type="password" id="password-2" >
        </p>
        <p>
            <button type="submit">提交</button> <button type="reset">重置</button>
        </p>
    </form>
    <script>
        'use strict';
var checkRegisterForm = function () {
    // 获取用户可以输入的控件
    let username = document.getElementById('username');
    let password = document.getElementById('password');
    let password_2 = document.getElementById('password-2');

    // 3~10位字母或数字
    let str1 = /([a-z0-9]){3,10}/gi;

    // 长度为6~20位, 
    let str2 = /\w{6,20}/gi;

    // 使网页第一次加载时直接结束本次事件
    if (username.value === '' && password.value === '' && password_2.value === '') {
        return false;
    }

    // 判断用户输入是否符合条件
    if (username.value.match(str1).length === 1 && password.value === password_2.value && password.value.match(str2).length === 1) {
        alert('登入成功');
        return true;
    }
    alert('账号或密码错误');
    return false;
}



    // 测试:
    ; (function () {
        window.testFormHandler = checkRegisterForm;
        var form = document.getElementById('test-register');
        if (form.dispatchEvent) {
            var event = new Event('submit', {
                bubbles: true,
                cancelable: true
            });
            form.dispatchEvent(event);
        } else {
            form.fireEvent('onsubmit');
        }
    })();
    </script>
</body>
~~~