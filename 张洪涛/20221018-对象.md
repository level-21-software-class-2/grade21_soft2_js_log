# 对象
**JavaScript的对象是一种无序的集合数据类型，它由若干键值对组成**

**JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开**
```JavaScript
var arr = {
    name: '鲲哥',
    ago: 28,
    sex: '男'，
    school: 'Da Jiang high School',
    height: 190,
    weight: 112,
};
```
**获取属性，访问属性是通过.操作符完成**
```JavaScript
arr.name;//'鲲哥'
```
**如果属性名包含特殊字符，就必须用''括起来**
```JavaScript
var xiaohong = {
    name: '鲲哥',
    'high-school': 'Da Jiang high School'
};
```
**当属性名不是一个有效的变量，就需要用''括起来。访问这个属性也无法使用.操作符，必须用['xxx']来访问**
```JavaScript
arr['high-school']; // 'Da Jiang high School'
arr['name']; // '鲲哥'
arr.name; // '鲲哥'
```
**访问一个不存在的属性，并不会不报错，而是返回undefined**
**如果我们要检测arr是否拥有某一属性，可以用in操作符。返回的是布尔值**
```JavaScript
var arr = {
    name: '鲲哥',
    ago: 28,
    sex: '男'，
    school: 'Da Jiang high School',
    height: 190,
    weight: 112,
};
'name' in arr; // true
'grade' in arr; // false
```