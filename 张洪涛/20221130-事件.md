# 事件
什么是事件：页面对不同访问者的响应叫做事件。

事件处理程序指的是当 HTML 中发生某些事件时所调用的方法。

实例：
```
在元素上移动鼠标。
选取单选按钮
点击元素
在事件中经常使用术语"触发"（或"激发"）例如： "当您按下按键时触发 keypress 事件"。
```
常见 DOM 事件：
[![zwjTET.png](https://s1.ax1x.com/2022/11/30/zwjTET.png)](https://imgse.com/i/zwjTET)
jQuery 事件方法语法
在 jQuery 中，大多数 DOM 事件都有一个等效的 jQuery 方法。

页面中指定一个点击事件：
```JS
$("p").click();
```
下一步是定义了点击后触发事件。您可以通过一个事件函数实现：
```JS
$("p").click(function(){
    // 动作触发后执行的代码!!
});
```
## 常用的 jQuery 事件方法
$(document).ready()

click():click() 方法是当按钮点击事件被触发时会调用一个函数。

该函数在用户点击 HTML 元素时执行。

在下面的实例中，当点击事件在某个 <p> 元素上触发时，隐藏当前的 <p> 元素：
```JS
$("p").click(function(){
  $(this).hide();
});
```
