通常来讲静态资源通常指的是js css html
promise解决了异步的问题让异步任务串行执行。
promise可以让任务完成一个之后继续下一个任务。
promise.all(p1,p2)
p1和p2都是promise对象。
all必须保证两个任务都完成。在p1和p2中都执行resolve才会执行then()
```js
let p1=new Promise(function(reslove,reject){
    reslove("x")
})
let p2=new Promise(function(reslove,reject){
    reslove("y")
})
Promise.all([p1,p2]).then((x,y)=>{console.log(x);})
//all需要两个全部成功   
```
玩一下race
```js
let p1=new Promise(function(reslove,reject){
    reslove("x")
})
let p2=new Promise(function(reslove,reject){
    reslove("y")
})
Promise.race([p1,p2]).then((x)=>{console.log(x);})
//好像看起来只要有一个成功就可以

//由于p1执行较快，Promise的then()将获得结果'P1'。p2仍在继续执行，但执行结果将被丢弃。
// 如果我们组合使用Promise，就可以把很多异步任务以并行和串行的方式组合起来执行。


```
race谁快就是谁。如果在其中设置timeout 就是时间段短的被读