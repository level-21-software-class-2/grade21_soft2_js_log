脚本是弱类型的语言。  
vscode是跨平台的？  
# javaScript  
输出语句alert('');  
JavaScript是严格区别大小写的。如果弄错了大小写会报错。  
console.log('');控制台输出日志。  
把页面的网络缓存直接禁用会让你的修改立马生效。  
一般我们使用的都是外部引入的js。在内部中  
```js
<HTML>
<script src="*.js"></script> 
```  
console   document 对象   
Math类的方法  
setInterval(()-{
  
},1000)  
设置间隔函数.  
单引号和双引号的有什么不一样。  
0==false。  
js中 ==弱类型 1='1'  
=== 是严格相等。  
NaN 是none   NaN!=NaN  
唯一的判断NaN是ifNaN(NaN)=true  
js中一切皆对象。  
定义一个像Java那样的对象格式：  
var obj={  
    name:'家伙',  
    age:18,  
    sex: '男',  
    readedBook:['星际穿越','','','','','','']  
    "x,y":19  
}  
  
strict 严格。  
要打开strict模式需要  
'use strict'  
不适用var 变量就是全局变量。所以要使用strict模式.

但是它们必须成对出现，不能前面用一个单引号，后面用一个双引号，这样会报错。

一般情况下，建议优先使用单引号包含字符串，这样做有几个好处：

html中标签的属性是用双引号包裹，在js中如果要动态输出html内容，则用单引号将整体html代码包裹起来，而标签中的属性刚好用双引号，避免了转义

编写代码时，双引号需要按shift才可以输入，无形之中降低了敲代码的速度

虽然在语法上，单引号和双引号没有区别，建议优先选用单引号，如何字符串内还有引号，再用双引号，必要的时候还需要用\进行转义

