instanceof 它的作用是测试它左边的对象是否是它右边的类的实例
白盒测试要懂代码逻辑才能进行测试。
黑核测试就是不知道逻辑是什么样的就硬测。
单元测试：就是对单独的功能进行测试
集成测试：就是将所有的东西整合在一起进行测试
压力测试：测试项目在服务器上的压力测试
回归测试：回归测试就是什么回过头来进行测试。
# jQuey 
除了基本的选择器外，jQuery的层级选择器更加灵活，也更强大。

因为DOM的结构就是层级结构，所以我们经常要根据层级关系进行选择。

# 层级选择器（Descendant Selector）

find('*') 
 ^表示开头。
jQuery对象.html("<p></p>") 类似于 innterHtml属性
jQuery对象.html()这样就是进行获取html的值。
.text()拿到的是纯文本，即使jQuery中有很多行也会进行分行。拿到各种标签之间的文本不会拿什么value的值。
使用html的话会拿到标签一起。
# 操作dom
.css('属性名','设置值')如果是只有属性名没有设置值则是为获取。
如果二者都有则是为设置值。
我们通过jQuery进行修改的css样式是直接写进行内的所以它具有最高优先级。

hasClass（）该方法可以进行判断是否属于该类的
addClass()添加类
removeClass 删除类
show()显示
hide()隐藏
```js

let dy=$('.dy')
//class="lang dy" 表示该对象同时属于两个类


//层级选择器
let layer=$('div .py')
//层级选择器就是可以先用前边的先缩小范围，然后使用选择器再进行选择


//子选择器
// 子选择器$('parent>child')类似层级选择器，但是限定了层级关系必须是父子关系，就是<child>节点必须是<parent>节点的直属子节点。还是以上面的例子
// let child=$('div>.py').py不是div的儿子，而是div的孙子所以子选择器选选择不到。
let child=$('div>ul>li>.py')


//过滤器
// 过滤器一般不单独使用，它通常附加在选择器上，帮助我们更精确地定位元素。观察过滤器的效果：
let dynamic=$('.dy')//选择出来的是三个javaScript  and Python and Scheme
// console.log(dyna mic);
let dynamic2=$('.dy:first-child')//选择第一个


let dynamic3=$('.dy:last-child')//选择最后一个


let dynamic4=$('.dy:nth-child(4)')//选择第三个
//父元素的特定类型的第 n 个子元素的所有元素。因为nth-child会从父类中开始找所以其中不是.dy的元素也会包括在内。
let dynamicNew=$('.dy:nth-child(even)')//偶数从0开始取
let dynamicNew2=$('.dy:nth-child(odd)')//奇数从1
let dynamicNew3=$('.dy:odd')//不用nth-child就不会有上述所说的那个选中不包括其中的问题。
// console.log(dynamic);

// console.log(dynamic4[0]);
/*
    <li class="lang dy"><span>JavaScript</span></li>
    <li class="lang dy" ><span class="py">Python</span></li>
    <li class="lang"><span>Swift</span></li>
    <li class="lang dy"><span>Scheme</span></li>
    这是此时此刻的html中的样式，，虽然我们.dy并没有选中其中的那个Swift但是我们在使用filter过滤器的时候它还是给了它一个位置
    这不知道是为什么就当作是js的特性吧。
*/

//表单相关

let input=$(':button')
//通过:button 可以选择表单中的button全部按钮


let file=$(":file")
//同样的也可以通过：file来选择所有file他是等价于input[type=file]的


let visible=$(":visible")
//这样可以用于获取所有可见的元素


//这样可以获取所有可见的div
let visible2=$("div:visible")

//查找find 过滤filter
let ul=$('ul')
//获取ul 然后再查找其中的空间
let dyS=ul.find(".dy")

//就是find找到其中的元素

let ul2=$('ul li')

//使用其获取ul后还需要获取其中的li才能进行过滤等于需要li组成的一个数组才能进行过滤
let dyS2=ul2.filter(".dy")


let sStart=ul2.filter(function(){
    return this.innerText.indexOf('S')===0;
})  
console.log("================");
//使用过滤器获取其中以S开头的节点。这时候因为调用了innerHtml所以会被绑定成DOM节点对象???。
// console.log(sStart);

let langs=$("li.lang")
//map就是当初学数组的时候的一个方法，这个方法可以对数组中的每一个数据对其进行其中function的操作。
let  langArr=(langs.map(function () {
    return this.innerText;
}));  
// console.log(langArr.get());
//这里的get方法貌似就是返回一个数组

let first=langs.first() 
// console.log(first[0].innerText);//first相当于相当于$('ul.lang li:first-child')
let last =langs.last();
// console.log(last[0].innerText); //相当于$('ul.lang li:last-child')
let slice24=langs.slice(2,4)
// console.log(slice24[0].innerText); //Swift, Scheme, 参数和数组的slice()方法一致

//使用jQuery操作dom：
let langQuery=$("li.lang")
//获取其中的text
// console.log( langQuery.text(""));
// langQuery.text("bbbb");
//使用text时，我们会发现我们不给text传递参数则会获取langQuery的值如果传递参数了则会改变其中的值，html也是同理
//或者其中的html
// console.log(langQuery.html());//该方法用于返回值的时候会返回其中的数组第一个参数。以及标签
// langQuery.html("<p style=\"color:red\">爱上了</p>")//改变的时候则跟text一个道理

//修改CSS
// langQuery.css("color","yellow")//修改第一个参数是属性名词第二个参数是参数值。

//显示和隐藏DOM
let dynamicOne=$(".dy")
/* setInterval(function(){
    //我们现在要做一个小的案例通过Attr方法来判断是否为显示状态，如果显示了则隐藏，反之亦然

    //我们选中需要判断其是否被选中通过百度我们知道了有两种写法一种是判断 .is(:visible) 一种是 .is(:hidden)
    // console.log(dynamicOne.is(":visible"));
    //使用其中自带的visible方法visible是选择html中的visibility属性的其中有visible,hidden,collapse 三种 style="visibility:hidden"
    if(dynamicOne.is(":visible")){
        dynamicOne.hide()
    }else{
        dynamicOne.show()
    }

},2000) */
//ok结束
dynamicOne.hide()   
dynamicOne.show()

//其中Jquery有提供hide和show两种方法

//获取dom信息

//浏览器可视窗口大小
$(window).width();
$(window).height();

//html文档大小
$(document).width();
$(document).height();

//或者某个div也可以这样获取宽高

//hasClass addClass removeClass
// console.log(dynamicOne.hasClass("dy")); 
// //判断其中是否是dy类的

// dynamicOne.addClass("hhh")
// //添加一个hhh类

// dynamicOne.removeClass("dy")

//我们想要获取其中的数据的时候可以使用Attr（attribute） prop(property)进行获取数据同时传递两个参数还可以对数据进行修改不过attr和prop所获取的数据是有区别的。我们要判断是否选中的时候使用is(':selected')。
var radio = $(':radio');
console.log(radio);
console.log( radio.attr('checked')); // 'checked' or undefind
console.log( radio.prop('checked')); // true    or false
var
    input = $('#test-input'),
    select = $('#test-select'),
    textarea = $('#test-textarea');

input.val(); // 'test'
input.val('abc@example.com'); // 文本框的内容已变为abc@example.com

select.val(); // 'BJ'
select.val('SH'); // 选择框已变为Shanghai

textarea.val(); // 'Hello'
textarea.val('Hi'); // 文本区域已更新为'Hi' 
//使用val（）方法直接修改表单的内容。
```

## 作业
```js
'use strict';
var div = $('#test-highlight-css');
// TODO:

let js=$(".js") 
/* js.css("color"," #dd1144")
js.css("background-color","#ffd351")
 */
js.addClass("highlight")


```