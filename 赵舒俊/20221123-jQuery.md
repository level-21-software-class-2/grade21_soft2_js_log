jQuery的作用：
jQuery这么流行，肯定是因为它解决了一些很重要的问题。实际上，jQuery能帮我们干这些事情：

消除浏览器差异：你不需要自己写冗长的代码来针对不同的浏览器来绑定事件，编写AJAX等代码；

简洁的操作DOM的方法：写$('#test')肯定比document.getElementById('test')来得简洁；

轻松实现动画、修改CSS等各种操作。

jQuery的理念“Write Less, Do More“，
    
从jQuery官网可以下载最新版本。jQuery只是一个jquery-xxx.js文件，但你会看到有compressed（已压缩）和uncompressed（未压缩）两种版本，使用时完全一样，但如果你想深入研究jQuery源码，那就用uncompressed版本。

要使用JQuery有两种办法一种是通过下载JQuery到本地，一种是直降讲官网通过src进行导入，这种方法需要有网才行吧。
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

$是著名的jQuery符号。实际上，jQuery把所有功能全部封装在一个全局变量jQuery中，而也是一个合法的变量名，它是变量jQuery的别名： 

按ID查找
如果某个DOM节点有id属性，利用jQuery查找如下：

// 查找<div id="abc">:
var div = $('#abc');

按tag查找
按tag查找只需要写上tag名称就可以了：

var ps = $('p'); // 返回所有<p>节点
ps.length; // 数一数页面有多少个<p>节点

按class查找
按class查找注意在class名称前加一个.：

var a = $('.red'); // 所有节点包含`class="red"`都将返回
// 例如:
// <div class="red">...</div>
// <p class="green red">...</p>

按属性查找
一个DOM节点除了id和class外还可以有很多属性，很多时候按属性查找会非常方便，比如在一个表单中按属性来查找：

var email = $('[name=email]'); // 找出<??? name="email">

组合查找
组合查找就是把上述简单选择器组合起来使用。如果我们查找$('[name=email]')，很可能把表单外的<div name="email">也找出来，但我们只希望查找<input>，就可以这么写：

var emailInput = $('input[name=email]'); // 不会找出<div name="email">

多项选择器
多项选择器就是把多个选择器用,组合起来一块选：

$('p,div'); // 把<p>和<div>都选出来
$('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来


## 练习
```js
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<!-- HTML结构 -->
<div id="test-jquery">
    <p id="para-1" class="color-red">JavaScript</p>
    <p id="para-2" class="color-green">Haskell</p>
    <p class="color-red color-green">Erlang</p>
    <p name="name" class="color-black">Python</p>
    <form class="test-form" target="_blank" action="#0" onsubmit="return false;">
        <legend>注册新用户</legend>
        <fieldset>
            <p><label>名字: <input name="name"></label></p>
            <p><label>邮件: <input name="email"></label></p>
            <p><label>口令: <input name="password" type="password"></label></p>
            <p><button type="submit">注册</button></p>
        </fieldset>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.js"></script>
    <script src="./a.js"></script>
    <script>
        'use strict';
// 使用jQuery选择器分别选出指定元素：

// 仅选择JavaScript

// 仅选择Erlang

// 选择JavaScript和Erlang

// 选择所有编程语言

// 选择名字input

// 选择邮件和名字input
'use strict';

var selected = null;
selected = $('#para-1');

// 高亮结果:
if (!(selected instanceof jQuery)) {
   console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');
 
JavaScript

Haskell

Erlang

Python

    // 高亮结果:
    if (!(selected instanceof jQuery)) {
            console.log('不是有效的jQuery对象!');
         }
         $('#test-jquery').find('*').css('background-color', '');
         selected.css('background-color', '#ffd351');
 
         JavaScript
 
         Haskell
 
         Erlang
 
         Python
 
    </script>
</body>
</html>
```