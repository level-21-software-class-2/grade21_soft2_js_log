# jQuery
* 地位：是JavaScript世界中使用最广泛的一个库。
* jQuery能帮我们干这些事情：
```
    1. 消除浏览器差异：你不需要自己写冗长的代码来针对不同的浏览器来绑定事件，编写AJAX等代码；
    2. 简洁的操作DOM的方法：写$('#test')肯定比document.getElementById('test')来得简洁；
    3. 轻松实现动画、修改CSS等各种操作。

    ** jQuery的理念“Write Less, Do More“，让你写更少的代码，完成更多的工作！
```
## jQuery版本
* 目前jQuery有1.x和2.x两个主要版本，区别在于2.x移除了对古老的IE 6、7、8的支持，因此2.x的代码更精简。选择哪个版本主要取决于你是否想支持IE 6~8。

* 从jQuery官网可以下载最新版本。jQuery只是一个jquery-xxx.js文件，但你会看到有compressed（已压缩）和uncompressed（未压缩）两种版本，使用时完全一样，但如果你想深入研究jQuery源码，那就用uncompressed版本。

## 使用jQuery
* 使用jQuery只需要在页面的

* 引入jQuery文件即可：
```JavaScript
    <html>
    <head>
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        ...
    </head>
    <body>
        ...
    </body>
    </html>
```
* 好消息是，当你在学习这个教程时，由于网站本身已经引用了jQuery，所以你可以直接使用：
```JavaScript
    'use strict';
    console.log('jQuery版本：' + $.fn.jquery);
```
## $符号
* "$"是著名的符号。实际上，把所有功能全部封装在一个全局变量中，而也是一个`合法的变量名`，它是变量jQuery的别名：
```JavaScript
    window.jQuery; // jQuery(selector, context)
    window.$; // jQuery(selector, context)
    $ === jQuery; // true
    typeof($); // 'function'    
```
* 本质上就是一个函数，但是函数也是对象，于是除了可以直接调用外，也可以有很多其他属性。

* 注意，你看到的`函数名`可能不是，因为很多压缩工具可以对`函数名和参数`改名，所以压缩过的源码函数可能变成`a(b, c)`。

* 绝大多数时候，我们都直接用（因为写起来更简单嘛）
* 但是，如果这个变量不幸地被占用了，而且还不能改，那我们就只能让jQuery把$变量交出来，然后就只能使用jQuery这个变量：
```JavaScript
    $; // jQuery(selector, context)
    jQuery.noConflict();
    $; // undefined
    jQuery; // jQuery(selector, context)

    ***
    这种黑魔法的原理是jQuery在占用之前，先在内部保存了原来的,调用jQuery.noConflict()时会把原来保存的变量还原。

```

# 选择器
* 选择器是jQuery的核心。一个选择器写出来类似$('#dom-id')。

* （为什么jQuery要发明选择器？回顾一下DOM操作中我们经常使用的代码）:
```JavaScript
    // 按ID查找：
    var a = document.getElementById('dom-id');

    // 按tag查找：
    var divs = document.getElementsByTagName('div');

    // 查找<p class="red">：
    var ps = document.getElementsByTagName('p');
    // 过滤出class="red":
    // TODO:

    // 查找<table class="green">里面的所有<tr>：
    var table = ...
    for (var i=0; i<table.children; i++) {
        // TODO: 过滤出<tr>
    }
```
* 这些代码实在太繁琐了，并且，在层级关系中，例如，查找里面的所有，一层循环实际上是错的，因为
的标准写法是：
```JavaScript
    <table>
        <tbody>
            <tr>...</tr>
            <tr>...</tr>
        </tbody>
    </table>

    ***

    很多时候，需要递归查找所有子节点。
```
* jQuery的选择器就是帮助我们快速定位到一个或多个DOM节点。

## 按ID查找
* 如果某个DOM节点有id属性，利用jQuery查找如下：
```JavaScript
    // 查找<div id="abc">:
    var div = $('#abc');

    注意，#abc以#开头。返回的对象是jQuery对象。
```
* 什么是jQuery对象？jQuery对象类似数组，它的每个元素都是一个引用了DOM节点的对象。
```JavaScript
    *--* 以上面的查找为例，如果id为abc的存在，返回的jQuery对象如下：

    [<div id="abc">...</div>]
    如果id为abc的

    *--* 不存在，返回的jQuery对象如下：

    []

    *--* 总之jQuery的选择器不会返回undefined或者null，这样的好处是你不必在下一行判断if (div === undefined)。
```

* jQuery对象和DOM对象之间可以互相转化：
```JavaScript
    var div = $('#abc'); // jQuery对象
    var divDom = div.get(0); // 假设存在div，获取第1个DOM元素
    var another = $(divDom); // 重新把DOM包装为jQuery对象
```
* 通常情况下你不需要获取DOM对象，直接使用jQuery对象更加方便。如果你拿到了一个DOM对象，那可以简单地调用$(aDomObject)把它变成jQuery对象，这样就可以方便地使用jQuery的API了。

## 按tag查找
* 按tag查找只需要写上tag名称就可以了：
```JavaScript
    var ps = $('p'); // 返回所有<p>节点
    ps.length; // 数一数页面有多少个<p>节点
    按class查找
    按class查找注意在class名称前加一个.：

    var a = $('.red'); // 所有节点包含`class="red"`都将返回
    // 例如:
    // <div class="red">...</div>
    // <p class="green red">...</p>

    *--* 通常很多节点有多个class，我们可以查找同时包含red和green的节点：

    var a = $('.red.green'); // 注意没有空格！
    // 符合条件的节点：
    // <div class="red green">...</div>
    // <div class="blue green red">...</div>
```

## 按属性查找
* 一个DOM节点除了id和class外还可以有很多属性，很多时候按属性查找会非常方便，比如在一个表单中按属性来查找：
```JavaScript
    var email = $('[name=email]'); // 找出<??? name="email">
    var passwordInput = $('[type=password]'); // 找出<??? type="password">
    var a = $('[items="A B"]'); // 找出<??? items="A B">

    *--*  当属性的值包含空格等特殊字符时，需要用双引号括起来。

    *--* 按属性查找还可以使用前缀查找或者后缀查找：

    var icons = $('[name^=icon]'); // 找出所有name属性值以icon开头的DOM
    // 例如: name="icon-1", name="icon-2"
    var names = $('[name$=with]'); // 找出所有name属性值以with结尾的DOM
    // 例如: name="startswith", name="endswith"

    *--* 这个方法尤其适合通过class属性查找，且不受class包含多个名称的影响：

    var icons = $('[class^="icon-"]'); // 找出所有class包含至少一个以`icon-`开头的DOM
    // 例如: class="icon-clock", class="abc icon-home"
```

## 组合查找
* 组合查找就是把上述简单选择器组合起来使用。如果我们查找$('[name=email]')，很可能把表单外的<div name="email">也找出来，但我们只希望查找<input>，就可以这么写：
```JavaScript
    var emailInput = $('input[name=email]'); // 不会找出<div name="email">
    *--* 同样的，根据tag和class来组合查找也很常见：

    var tr = $('tr.red'); // 找出<tr class="red ...">...</tr>
```
## 多项选择器
* 多项选择器就是把多个选择器用,组合起来一块选：
```JavaScript
    $('p,div'); // 把<p>和<div>都选出来
    $('p.red,p.green'); // 把<p class="red">和<p class="green">都选出来

    *--* 要注意的是，选出来的元素是按照它们在HTML中出现的顺序排列的，而且不会有重复元素。例如，<p class="red green">不会被上面的$('p.red,p.green')选择两次。
```
