# JavaScript
* JS是最流行的脚本语言
1. 不强制在结尾加“；”
2. 对嵌套层次无限制，若出现此类情况，只需把部分代码抽出来再用函数调用，可以减少代码的复杂度

## (一) JS中单引号和双引号的使用

1. 可以使用单引号或双引号；
2. 可以在 字符串 中使用引号，字符串中的引号不要与字符串的引号相同;
3. 可以在字符串添加转义字符来使用引号，即\"或\'；
```
var str1 = "字符串1"；
var str2 = '字符串2'；
var name1 = " 她的名字叫 '小丽 ' "  ;
var name2 = " 她的名字叫\\"汤姆\\" " ;

1、引号（同类型的引号，单引号和双引号是不同类型）是成双对的，在读第一个引号时开始，读到第二个结束，遇到第三个又开始，第四个又结束......

2、不同类型引号之间可以嵌套，最多2层（当然通过转义可以继续往下套，但是因为可读性太差，不要这样做）
```
## (二) HTML
```
<p></p> ： 段落标签
<br> : 换行
<title></title> : 文档标题
caption : 标题
font : 字体

<body>
    //网页显示内容
</body>

```
## (三) 间隔函数
```
JavaScript 支持暂停和时间间隔，这课有效的告诉浏览器应该何时执行某行代码。暂停就是在指定的毫秒数。
1、setTimeout('function',time):
第一个参数可以是代码串，也是可以函数指针，第二个参数是暂停时间（秒）。
function time(){
alert('执行');
}
window.οnlοad=function(){
setTimeout(time,1000);
}
** 这个表示在一秒后执行time()函数,调用setTimeout()时，它创见一个数字暂停ID，与操作系统的进程ID相似。暂停ID本质上是要延迟的进程的ID，在调用setTimeout()时后，就不应该在执行其他代码。想 取消还未执行的暂停，可以用clearTimeout ()来取消

2、clearTimeout()：
接受一个参数：执行setTimeout()时，创建的暂停ID；
<script>
 var s = 0;
 function run(){
  s = setTimeout(a,1000)
 }
 function a(){
  alert(2)
 }
 function stop(){
  clearTimeout(s)
 }
</script>

<input type='button' value='run' οnclick='run()'>
<input type='button' value='stop' οnclick='stop()'>

** 在按下run时出发run()函数，执行setTimeout()并将创建的ID传给s，在一秒后执行a(),如果在1秒内点击stop就会停止setTimeout()函数的执行。

2、还有一种执行方法，与暂停类似，setInterval()就是时间间隔表示在某段时间内不停的去执行一行代码，除了停止它否则会一直执行下去，这就是与setTimeout()不同之处，一个是暂停多少秒后执行一次，而setInterval()是一直执行。

function time(){
alert('执行');
}
window.οnlοad=function(){
setInterval(time,1000);
}
 
** 1秒内不停的调用time().和setTimeout()一样setInterval()也有停止它的函数，clearInterval()停止函数执行。

 <script>
 var s = 0;
 function run(){
  s = setInterval(a,1000)
 }
 function a(){
  alert(2)
 }
 function stop(){
  clearInterval(s)
 }

</script>

<input type='button' value='run' οnclick='run()'>
<input type='button' value='stop' οnclick='stop()'>

** 在按下run时出发run()函数，执行setInterval()并将创建的ID传给s，在一秒后执行a(),如果在1秒内点击stop就会停止setInterval()函数的执行。否则就会一直执行a()函数，这是与setTimeout()的不同之处

** 如何选择使用那种方法，在执行一组代码前要等待一段时间，就使用暂停setTimeout()，如果要反复的去执行一组代码时就用间隔setInterout().
```