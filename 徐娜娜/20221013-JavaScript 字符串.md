# JavaScript 字符串

* 单引号：用于表示单个字符
* 双引号：用于表示字符串，多个字符
* 在 JS 里，无差别，可使用转义符 "\\"

  ```
  例："今\"天气晴朗"  转义符可以代替一个引号,不会报错 -->今 天气晴朗
    "\\" 一个转义符，一个反斜杠
     '我\\你'  -->运行  '我\你'
     
  ```
* 后缀加 "\n" 可换行

## (一)多行字符串：

* "`" 一个反单引号
```
  var str = `小红
             小明
             小兰`   //运行结果是有换行的，(多行文本)
```

* 插入字符串：
```
  var x = '小明';
  var str = `小红 ${x}
           小明
           小兰`;    //运行结果  小红 小兰
                               小明
                               小兰
```

## (二)模板字符串：

* 模板字符串，从名字上可以得出其实返回的是字符串，普通使用其实就想引号一样的使用，只是加了一些功能（注：先这些实验例子都是自浏览器控制台中测试的)

```
  当做引号使用，返回字符串
  
  `aaaaa`
  //返回字符串"aaaaa"
  插入表达式，其实有点格式化输出的感觉
  
  var a=123;
  `aaa${a}aaa`
  //返回字符串"aaa123aaa"
  带标签的模板字符串，可以自定义函数处理模板字符串
  
  var person = 'Mike';
  var age = 28;
  
  function myTag(strings, personExp, ageExp) {
  
    var str0 = strings[0]; // "that "
    var str1 = strings[1]; // " is a "
  
    // There is technically a string after
    // the final expression (in our example),
    // but it is empty (""), so disregard.
    // var str2 = strings[2];
  
    var ageStr;
    if (ageExp > 99){
      ageStr = 'centenarian';
    } else {
      ageStr = 'youngster';
    }
  
    return str0 + personExp + str1 + ageStr;
  
  }
  
  var output = myTag`that ${ person } is a ${ age }`;
  
  console.log(output);
  //输出字符串that Mike is a youngster
  这里需要注意的一点是，使用标签函数时，模板字符串会解析成字符串数组跟各个${}的值，然后传入标签函数中，因为标签函数的入参就是这样的，下面会深入将标签函数的。
  
```

## (三)操作字符串：
```
var s = 'hello,worId';
  s.length;  //长度
```
 * 字符串是不可改变的，如对字符串中的某一个索引赋值，不会有错误，但也没有效果
```
    var s = 'Text';
    s[0] = 'x';
    alert(s);  //s 仍为'Text'
```

## (四)Indexof

#### 1. 定义 ： indexOf() 方法可返回某个指定的字符串值在字符串中首次出现的位置。

#### 2. 语法 ： stringObject.indexOf(searchvalue,fromindex)

#### 3. 注意： 
```
indexOf() 方法对大小写敏感！

如果要检索的字符串值没有出现，则该方法返回 -1。
```
#### 4. 实例：
```
在本例中，我们将在 "Hello world!" 字符串内进行不同的检索：

<script type="text/javascript">

var str="Hello world!"
document.write(str.indexOf("Hello") + "<br />")
document.write(str.indexOf("World") + "<br />")
document.write(str.indexOf("world"))

</script>

*------------------------------------*
以上代码的输出：
    0
    -1
    6
```
## (五)substring

#### 1. 定义:  substring() 方法用于提取字符串中介于两个指定下标之间的字符。

#### 2. 语法:  stringObject.substring(start,stop)

#### 3. **
```
(1.)参数：start    
    描述：必需。一个非负的整数，规定要提取的子串的第一个字符在 stringObject 中的位置。

(2.)参数：stop
    描述：可选。一个非负的整数，比要提取的子串的最后一个字符在 stringObject 中的位置多 1。
   *** 如果省略该参数，那么返回的子串会一直到字符串的结尾。

*-* 返回值 ：

    一个新的字符串，该字符串值包含 stringObject 的一个子字符串，其内容是从 start 处到 stop-1 处的所有字符，其长度为 stop 减 start。

*-* 说明 :
    substring() 方法返回的子串包括 start 处的字符，但不包括 stop 处的字符。

    如果参数 start 与 stop 相等，那么该方法返回的就是一个空串（即长度为 0 的字符串）。如果 start 比 stop 大，那么该方法在提取子串之前会先交换这两个参数。
```
#### 5. 注意： 与 slice() 和 substr() 方法不同的是，substring() 不接受负的参数

#### 6. 实例 :
```
例子 1
在本例中，我们将使用 substring() 从字符串中提取一些字符：

<script type="text/javascript">

var str="Hello world!"
document.write(str.substring(3))

</script>
输出：

lo world!

*---------------------------------------------------------*

例子 2
在本例中，我们将使用 substring() 从字符串中提取一些字符：

<script type="text/javascript">

var str="Hello world!"
document.write(str.substring(3,7))

</script>
输出：

lo w
```