## 获取DOM信息

利用jQuery对象的若干方法，我们直接可以获取DOM的高宽等信息，而无需针对不同浏览器编写特定代码：

```javascript
// 浏览器可视窗口大小:
$(window).width(); // 800
$(window).height(); // 600

// HTML文档大小:
$(document).width(); // 800
$(document).height(); // 3500

// 某个div的大小:
var div = $('#test-div');
div.width(); // 600
div.height(); // 300
div.width(400); // 设置CSS属性 width: 400px，是否生效要看CSS是否有效
div.height('200px'); // 设置CSS属性 height: 200px，是否生效要看CSS是否有效
attr()和removeAttr()方法用于操作DOM节点的属性：

// <div id="test-div" name="Test" start="1">...</div>
var div = $('#test-div');
div.attr('data'); // undefined, 属性不存在
div.attr('name'); // 'Test'
div.attr('name', 'Hello'); // div的name属性变为'Hello'
div.removeAttr('name'); // 删除name属性
div.attr('name'); // undefined
```

prop()方法和attr()类似，但是HTML5规定有一种属性在DOM节点中可以没有值，只有出现与不出现两种，例如：

```html
<input id="test-radio" type="radio" name="test" checked value="1">
```

等价于：

```html
<input id="test-radio" type="radio" name="test" checked="checked" value="1">
```

attr()和prop()对于属性checked处理有所不同：

```javascript
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
```

prop()返回值更合理一些。不过，用is()方法判断更好：

```javascript
var radio = $('#test-radio');
radio.is(':checked'); // true
```

类似的属性还有selected，处理时最好用is(':selected')。

## 操作表单

对于表单元素，jQuery对象统一提供val()方法获取和设置对应的value属性：

```javascript
/*
    <input id="test-input" name="email" value="test">
    <select id="test-select" name="city">
        <option value="BJ" selected>Beijing</option>
        <option value="SH">Shanghai</option>
        <option value="SZ">Shenzhen</option>
    </select>
    <textarea id="test-textarea">Hello</textarea>
*/
var
    input = $('#test-input'),
    select = $('#test-select'),
    textarea = $('#test-textarea');

input.val(); // 'test'
input.val('abc@example.com'); // 文本框的内容已变为abc@example.com

select.val(); // 'BJ'
select.val('SH'); // 选择框已变为Shanghai

textarea.val(); // 'Hello'
textarea.val('Hi'); // 文本区域已更新为'Hi'
```

可见，一个val()就统一了各种输入框的取值和赋值的问题。

## 练习

```html
<div class="test-selector">
    <ul class="test-lang">
        <li class="lang-javascript">JavaScript</li>
        <li class="lang-python">Python</li>
        <li class="lang-lua">Lua</li>
    </ul>
    <ol class="test-lang">
        <li class="lang-swift">Swift</li>
        <li class="lang-java">Java</li>
        <li class="lang-c">C</li>
    </ol>
</div>
```



```javascript

//选出相应的内容并观察效果：
// 分别选择所有语言，所有动态语言，所有静态语言，JavaScript，Lua，C等:
'use strict';
var selected = null;
// 分别选择所有语言，所
// selected =$(".test-selector") 

// 有动态语言
// selected =$(".lang-javascript,.lang-python,.lang-lua") 

// 所有静态语言，JavaScript，Lua，C等:

// selected = $(".lang-swift,.lang-java,.lang-c")

// 高亮结果:
if (!(selected instanceof jQuery)) {
    console.log('不是有效的jQuery对象!');
}
$('#test-jquery').find('*').css('background-color', '');
selected.css('background-color', '#ffd351');
 
// JavaScript 动态
// Python 动态
// Lua 动态
// Swift    静态
// Java 静态
// C    静态
```

```html

<form id="test-form" action="#0" onsubmit="return false;">
    <p><label>Name: <input name="name"></label></p>
    <p><label>Email: <input name="email"></label></p>
    <p><label>Password: <input name="password" type="password"></label></p>
    <p>Gender: <label><input name="gender" type="radio" value="m" checked> Male</label> <label><input name="gender" type="radio" value="f"> Female</label></p>
    <p><label>City: <select name="city">
    	<option value="BJ" selected>Beijing</option>
    	<option value="SH">Shanghai</option>
    	<option value="CD">Chengdu</option>
    	<option value="XM">Xiamen</option>
    </select></label></p>
    <p><button type="submit">Submit</button></p>
</form>
```

```javascript
// 输入值后，用jQuery获取表单的JSON字符串，key和value分别对应每个输入的name和相应的value，例如：{"name":"Michael","email":...}

'use strict';
var json = null;
let form=$("#test-form input,#test-form select")
let form2=form.filter(function(){
    
    if(this.type=="radio" && !this.checked){
        return false
    }else{
        return true
    }
})
let result={}

form.map(function(){
    result[this.name]=this.value

})

console.log(result);

//stringify可以返回josn文本的字符串
json=JSON.stringify(result)

显示结果:
if (typeof(json) === 'string') {
    console.log(json);
}
else {
    console.log('json变量不是string!');
}

```

