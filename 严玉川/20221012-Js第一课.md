## JavaScript学习

### 一、静态页面和动态页面的区别

需要明白的一点是：不是“会动的页面”就叫做动态页面，，静态与动态的**区别在于：是否和服务器进行数据交互，或者简单来说是否用到了后端技术**

### 二、初识JavaScript

### 2.1   JavaScript是什么

① JavaScript是世界上最流行的语言之一，**是一种运行在客户端的脚本语言（script是脚本的意思）**

② **脚本语言：不需要编译，运行过程中有js解释器（js引擎）逐行来进行解释并执行**

③ 现在也可以基于Node.js技术进行服务器端编程

### 2.2  js的作用

```
表单动态校验（密码强度检测）（js产生的目的）
网页特效
服务器端开发（Node.js）
桌面程序
APP
控制硬件-物联网
游戏开发
```

### 2.3  js书写的几种方式

#### 2.3.1   外部JavaScript

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!-- 引入外部CSS -->
	　　<link href="./index.css" rel="stylesheet" type="text/css" href=""/>
		<!-- 引入外部JavaScript -->
		<script src="./js/index.js" type="text/javascript" charset="utf-8"></script>
	</head>
	<body>	
	</body>
   
</html>

 <!--  
	说明：引入外部的CSS文件使用的是link标签，而引入外部JavaScript文件使用的是script标签
		
	切记！！！！别搞混了！！！！yyc
 -->
```

#### 2.3.2   内部JavaScript

```html
<!-- 
内部的JavaScript，指的是把HTML代码和JavaScript代码放在同一个文件夹里面。
其中，JavaScript代码写在<script></script>标签内
-->
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!-- 1、在head中引入 -->
		<script type="text/javascript">
			......
		</script>
	</head>
	<body>
		<!-- 1、在body中引入 -->
		<script type="text/javascript">
			......
		</script>
	</body>
</html>
```

举例：

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
	</head>
	<body>
		<!-- 1、在body中引入 -->
		<script type="text/javascript">
			document.write("淡写青春的第一篇JavaScript笔记")
		</script>
	</body>
</html>

<!-- 
	说明：document.write（）表示在页面输出一个内容，，，，，，这个方法后面经常用到
-->
```

![](./img/day1.jpg)

#### 2.3.3    元素标签

```HTML
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
	</head>
	<body>
		<!-- 1、在body中引入 -->
		<input type="button" value="按钮" onclick="alert('淡写青春的第一篇JavaScript笔记,如有补充/不足支出,可以告诉我哦')" />
	</body>
</html>

```

![](./img/day1.2.jpg)

举例：**在元素中调用函数**

![](./img/day1.3.jpg)
