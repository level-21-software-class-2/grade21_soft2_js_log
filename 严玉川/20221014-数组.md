### 数组

```JavaScript
var 数组名=[元素1，元素2，......,元素n]     //简写模式
```

### 6.2  数组的获取

```html
<!--数组的下标都是从0开始的，而不是从1开始-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        //创建数组
        var arr=["成都","福州","重庆"];
        document.write(arr[2])
    </script>
</head>
<body>
</body>
</html>

<!--
分析：arr[2]表示获取数组里面的第三个元素！而不是第二个元素
-->
```

### 6.3   数组的赋值

```javascript
arr[i]=值；
```

### 6.4   获取数组的长度

```
数组名 .length
```

### 6.5   截取数组的某个部分

```javascript
数组名 .slice(start,end);

//说明：
//start表示开始的位置，end表示结束的位置。start和end都是整数，都是从0开始，其中end要大于start
```

### 6.6   数组的比较大小

```JavaScript
数组名 .sort(函数名)      //“函数名”就是定义数组元素排序的元素名字
```

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        //定义一个升序
        function up(a,b)
        {
            return a-b;
        }
        //定义一个降序
        function down(a,b)
        {
            return b-a;
        }
        //定义数组
        var arr=[3,6,5,9,65,52,18,21,11]
        arr.sort(up)
        document.write("升序："+arr.join("、"))
    </script>
</head>
<body>
</body>
</html>
```

