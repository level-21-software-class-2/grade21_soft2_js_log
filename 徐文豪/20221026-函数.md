# filter() 

### 定义

filter() 方法创建一个新的数组，新数组中的元素是通过检查指定数组中符合条件的所有元素。

注意： filter() 不会对空数组进行检测。
注意： filter() 不会改变原始数组。

### 实例
返回数组 ages 中所有元素都大于 18 的元素:

var ages = [32, 33, 16, 40];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.filter(checkAdult);
}
输出结果为:

32,33,40


# every()

## 定义和用法
every() 方法用于检测数组所有元素是否都符合指定条件（通过函数提供）。

every() 方法使用指定函数检测数组中的所有元素：

如果数组中检测到有一个元素不满足，则整个表达式返回 false ，且剩余的元素不会再进行检测。
如果所有元素都满足条件，则返回 true。
注意： every() 不会对空数组进行检测。

注意： every() 不会改变原始数组。

## 实例

检测数组 ages 的所有元素是否都大于等于 18 :

var ages = [32, 33, 16, 40];

function checkAdult(age) {
    return age >= 18;
}

function myFunction() {
    document.getElementById("demo").innerHTML = ages.every(checkAdult);
}
输出结果为:

false

## 语法

array.every(function(currentValue,index,arr), thisValue)



# find()


## 概念
find()方法用于查找数组中符合条件的第一个元素，如果没有符合条件的元素，则返回undefined

注意：

 find() 对于空数组，函数是不会执行的。

 find() 并没有改变数组的原始值。

## 实例
获取数组中年龄大于 18 的第一个元素

var ages = [3, 10, 18, 20];
 
function checkAdult(age) {
    return age >= 18;
}
 
function myFunction() {
    document.getElementById("demo").innerHTML = ages.find(checkAdult);
}
fruits 输出结果：

18


 # findIndex()
  
## 定义和用法
findIndex() 方法返回传入一个测试条件（函数）符合条件的数组第一个元素位置。

findIndex() 方法为数组中的每个元素都调用一次函数执行：

当数组中的元素在测试条件时返回 true 时, findIndex() 返回符合条件的元素的索引位置，之后的值不会再调用执行函数。
如果没有符合条件的元素返回 -1
注意: findIndex() 对于空数组，函数是不会执行的。

注意: findIndex() 并没有改变数组的原始值。

## 实例

获取数组中年龄大于等于 18 的第一个元素索引位置

var ages = [3, 10, 18, 20];
 
function checkAdult(age) {
    return age >= 18;
}
 
function myFunction() {
    document.getElementById("demo").innerHTML = ages.findIndex(checkAdult);
}
fruits 输出结果：

2