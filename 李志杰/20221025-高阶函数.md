#  高阶函数
接受收一个函数B作为参数的函数A，称A为高阶函数
```js
    function (x,y,fn){
        return fn(x,y);
    }
    let rest1 = fun(1,3,(x,y)=>x+y);
    console.log(rest1);
    let rest2 = fun(1,3,function(x,y){return x-y});
    console.log(rest2);
```
## map
```js
    //将函数作用于一个数组中
    function pow(x){
        return x * x;
    }
    var arr = [1,2,3,4,5,6,7,8,9,10];
    let newarr = arr.map(pow);
    console.log(newarr);
```
## reduce
```js
    //接受两个参数，把结果继续和序列下一个元素做累计计算
    var arr =  [1,2,3,4];
    arr.reduce(function (x,y){
                return x+y;
    })
```
### 例题
```js
//求积

function product(arr){
    let res = 1;
    for(let val of arr){
        res = res * val;
    }
    console.log(res);
    return res;
}

if(product([1,2,3,4]) === 24 && product([0,1,2])=== 0 && product([99,88,77,66] ) === 44274384 ){
   console.log('测试成功');
}
else{
    console.log('测试失败');

}
```

```js
//把用户输入的不规范的英文名字，变为首字母大写，其它小写的规范名字。输入['adam','USA','barT']
//输出['Adam','Lisa','Bart']
 var arr =['adam','USA','barT']
 var newarr =[];
function normalize(arr){
    for(let item of arr ){
       newarr = (item.substring(1,item.length)).toLowerCase();
       item = (item.substring(0,1)).toUpperCase();  
       newarr = item+ newarr;
        
    }
    return newarr;
}
console.log(normalize(arr));
```