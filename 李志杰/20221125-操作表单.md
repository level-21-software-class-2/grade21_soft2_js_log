# 操作表单
### val()
```
    在表单中 value= '';
    input.val();为空
    再次调用val()才能获取文本值
    val();
    获取或设置value属性
    传参为为设置值
```
### prop(),attr(),is()
```js
    prop('');//需要传参，获取属性值
    attr('');//需要传参，返回true或false
    
    is(':checked');//true
```