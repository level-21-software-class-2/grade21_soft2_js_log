# 事件
```js
    values //返回html预设的values
    /*
    表单提交常见的两种方式
        1.form的原始提交，即给表单的action指定提交的路径，然后将按钮的类型修改我为submit类型，当点击这个按钮的时候，表单就会被提交
        2.利用js函数进行提交，即去掉表单的action路径，然后将按钮的类型修改为普通的button，设置onclik事件绑定一个函数（或者多个）则点击这个按钮的时候，事件被触发，事件就会执行绑定的函数
    */
   <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

</head>
<body>
    <form action="">
        用户名：<input type="text"> <br>
        密码： <input type="password" name="" id="">
        <input type="button" value="提交" onclick="" id = "cc">

    </form>
    <script src="a.js"></script>
</body>
</html>


//js
function aa (){
    console.log("aaa");
}
function bb(){
    console.log("bbb");
}

let cc = document.getElementById("cc");
cc.addEventListener('click',function() {
    console.log("cc");
})

```