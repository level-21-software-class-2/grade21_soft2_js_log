

1.箭头函数

ES6标准新增了一种新的函数:Arrow Function(箭头函数)。为什么叫Arrow Function?因为它的定义用的就是一个箭头:


X=>x*x


上面的箭头函数相当于:

function (x) {

returnxx;}


在继续学习箭头函数之前，请测试你的浏览器是否支持ES6的Arrow Function:

"use strict';

var fn-x->x*x;

console.log("你的浏览器支持ES6的Arrow Function!);

箭头函数相当于匿名画数，并且简化了函数定义。箭头画数有两种格式，一种像上面的，只包含一个表达式，连!….)和retun都省略掉了。还有一种可以包含多条语句。这时候就不能省略{-}和return:

x => {

if (x>0){

returnx*x:

2.this

箭头函数看上去是匿名函数的一种简写，但实际上，箭头函数和匿名函数有个明显的区别:箭头函数内部的this是词法作用域，由上下文确定。

回顾前面的例子，由于JavaScript函数对this绑定的错误处理，下面的例子无法得到预期结果:

var obj={

birth:1990,


getAge: function (){

var b =this.birth; // 1990 var fn = function () {

return new Date().getFullYear()-this.birth;//this指向window或undefined};

return fn();


现在，箭头函数完全修复了this的指向，this总是指向词法作用域，也就是外层调用者obj;

var obj={

birth: 1990,

getAge: function (){

var b=this.birth; // 1990

var fn-()->new Date()-getFullYear()-this.birth;// this指向obj对象 return fn();

