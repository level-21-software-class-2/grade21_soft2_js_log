
arr(s」='X';
arr; // arr变为[1，2，3，undefined, undefined, "x']
大多数其他编程语言不允许直接改变数组的大小，越界访问索引会报错。然而，JavaScript的Array却不会有任何错误。在编写代码时，不建议直接修改Array的大小，访问索引时要确保素引不会越界。
indexOf
与String类似，Array也可以通过indexOf0来搜索一个指定的元素的位置:
var arr -[10,20,"30°,"xyz']; arr.index0f(10);//元素10的素引为日 arr.index0f(20);//元素20的索引为1
arr.indexof(30);// 元素30没有找到，返回-1 arr.indexOf(“30);//元素30的索引为2

注意了，数字30和字符串30是不同的元素。
slice
slice0就是对应String的substring0版本，它截取Array的部分元素，然后返回一个新的Array
var arr = ['A*, "B"，'℃', 'D',"E'，"F"，"G'];
arr.slice(e，3);// 从索引e开始，到索引3结束，但不包括索引3:["A”，"B"，'c' ] arr.slice(3);// 从索引3开始到结束:['D'，"'E'，"F'，'G']
注意到slice的起止参数包括开始索引，不包括结束索引。
如果不给slice0传递任何参数，它就会从头到尾截取所有元素。利用这一点，我们可以很容易地复制一个Array:
var arr - ['A', 'B",C','D','E','F',“G']; var aCopy-arr.slice();
aCopy; // ['A','B','c','D', 'E','F', "G' ] aCopy- arr;// false
push和pop
push0向Array的末尾添加若千元素，pop0则把Array的最后一个元素删除掉:
var arr - [1,2];
arr.push('A'，'8');// 返回Array新的长度:4 arr; // [1, 2, "A"，"B'] arr.pop(); // pop()返回'B' arr; // [1,2,"A']
arr.pop();arr.pop();arr.pop();// 连续pop 3次 arr; // []
arr.pop();//空数组继续pop不会报错，而是返回undefined arr; // []
unshift和shift
如果要往Array的头部添加若千元素，使用unshift0)方法，shift0方法则把Array的第一个元素删掉:
var arr -[1,2];
arr.unshift('A'，'B');// 返回Array新的长度:4 arr; // ['A','B', 1, 2] arr.shift(); // 'A° arr; // ['8', 1, 2]
arr.shift();arr.shift();arr.shift();// 连续shift 3次 arr; // []
arr.shift();// 空数组继续shift不会报错，而是返回undefined arr; // []
sort
sort0)可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序:
var arr - ['B','C', 'A']; arr.sort():


