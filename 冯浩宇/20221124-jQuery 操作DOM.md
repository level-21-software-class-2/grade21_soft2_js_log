# jQuery 操作DOM

## 修改Text和HTML

jQuery对象的text()和html()方法分别获取节点的文本和原始HTML文本

无参数调用text()是获取文本，传入参数就变成设置文本

```js
$('#test-ul li[name=book]').text(); 
$('#test-ul li[name=book]').html();
$('#test-ul li[name=book]').text('JavaScript'); 
$('#test-ul li[name=book]').html('<span style="color: red">JavaScript</span>'); 
```

## 修改CSS

```js
let div = $('#test-div');
div.css('color'); // '#000033', 获取CSS属性
div.css('color', '#336699'); // 设置CSS属性
div.css('color', ''); // 清除CSS属性
```

##  显示和隐藏DOM

```js
let a = $('a[target=_blank]');
a.hide(); // 隐藏
a.show(); // 显示
```

##  获取DOM信息

```js
var div = $('#test-div');
div.attr('data'); // undefined, 属性不存在
div.attr('name'); // 'Test'
div.attr('name', 'Hello'); // div的name属性变为'Hello'
div.removeAttr('name'); // 删除name属性
div.attr('name'); // undefined
```

### attr()和prop()对于属性checked处理

```js
var radio = $('#test-radio');
radio.attr('checked'); // 'checked'
radio.prop('checked'); // true
//prop()返回值更合理一些。不过，用is()方法判断更好：
radio.is(':checked'); // true
```

