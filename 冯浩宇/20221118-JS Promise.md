# JS Promise 

## Promise操作

```js
function fn(success,failed){
    //这里有一系列要执行的命令或者代码
    //判断
    if (条件) {
        success();
    } else {
     	failed()   
    } 
}
 
let p = new Promise(fn);

p.then(function(){
   //是执行then函数
    console.log(); 
}).catch(function(){
   //否执行catch函数
    console.log();  
});
//或者
//get post put delete hand options
//restfull
p.get().then().catch()

//----------------------------------------------------------
//简写
new Promise(function(resolve,reject){
    
}).then().catch(); 
//或者------------------------------------------------------
let fn = function(resolve,reject){
    //这里有一系列要执行的命令或者代码
    //判断
    if (条件) {
        resolve();
    } else {
     	reject()   
    } 
}

let p = new Promise(fn);

p.then(function(){
   //是执行then函数
    console.log(); 
}).catch(function(){
   //否执行catch函数
    console.log();  
});
```

