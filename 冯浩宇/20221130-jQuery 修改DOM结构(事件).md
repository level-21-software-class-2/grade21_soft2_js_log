# jQuery 修改DOM结构

### 添加节点的方式

```js
//方法1
//如何向列表新增一个语言？首先要拿到`<ul>`节点：
let ul = $('#test-div>ul');
//然后，调用append()传入HTML片段：
ul.append('<li><span>Haskell</span></li>');
-------------------------------------------------
//方法2
//或者随机内容插入
let arr = ['',''];
let rndindex = Math.floor(Math.random()*arr.length);
let ul = $('');
let p = `<p>${arr[rndindex]}</p>`;
ul.append(p);
--------------------------------------------------
//方法3
let ul = $('ul');
// 创建DOM对象:
let p = document.createElement('p');
p.textContent = 'Pascal';
// 添加DOM对象:
ul.append(p);
```

`append()`把DOM添加到最后，`prepend()`则把DOM添加到最前。

### 拿到DOM对象的方式(拿到节点的方式)

```js
let ul = $('ul');
//方法1
let p = document.createElement('p');
p.textContent = 'Pascal';
//方法2
let ps = ul.find('p:first').next();//当前节点的下一个
//方法3
let ps = document.getElementsByTagName('p')[1];
```

### 删除节点

```js
//拿到节点
let ps = ul.find('p:first').next();
//删除节点
ps.remove();
```

# jQuery事件

## jQuery 事件绑定

jQuery 中提供了四种事件监听绑定方式，分别是 bind、live、delegate、on，

对应的解除监听的函数分别是 unbind、die、undelegate、off。

live、delegate 不多用，在jQuery1.7中已经移除

## jQuery能够绑定的事件主要包括：

### 鼠标事件

- click: 鼠标单击时触发；
- dblclick：鼠标双击时触发；
- mouseenter：鼠标进入时触发；
- mouseleave：鼠标移出时触发；
- mousemove：鼠标在DOM内部移动时触发；
- hover：鼠标进入和退出时触发两个函数，相当于mouseenter加上mouseleave。

### 键盘事件

键盘事件仅作用在当前焦点的DOM上，通常是`<input>`和`<textarea>`。

- keydown：键盘按下时触发；
- keyup：键盘松开时触发；
- keypress：按一次键后触发。 其他事件
- focus：当DOM获得焦点时触发；
- blur：当DOM失去焦点时触发；
- change：当`<input>`、`<select>`或`<textarea>`的内容改变时触发；
- submit：当`<form>`提交时触发；
- ready：当页面被载入并且DOM树完成初始化后触发。 其中，ready仅作用于document对象。由于ready事件在DOM完成初始化后触发，且只触发一次。

## 取消绑定

一个已被绑定的事件可以解除绑定，通过off('click', function)实现

```js
function hello() {
    alert('hello!');
}

a.click(hello); // 绑定事件

// 10秒钟后解除绑定:
setTimeout(function () {
    a.off('click', hello);
}, 10000);
```