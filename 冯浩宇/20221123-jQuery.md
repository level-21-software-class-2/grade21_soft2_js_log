# jQuery
## JS Promise异步
1、异步任务串行 P.then().then()
2、异步任务并行 P.all([P1,P2]).then()
3、异步任务容错并行 P.race([P1,P2]).then()
### 为何要有Promise
1、为了代码好看
2、为了避免回调地狱

## jQuery特性
#### jQuery 库 - 特性

jQuery 是一个 JavaScript 函数库。

jQuery 库包含以下特性：

- HTML 元素选取

- HTML 元素操作

- CSS 操作

- HTML 事件函数

- JavaScript 特效和动画

- HTML DOM 遍历和修改

- AJAX

- Utilities

  ## jQuery 语法

  jQuery 语法是为 HTML 元素的选取编制的，可以对元素执行某些操作。

  基础语法是：*$(selector).action()*

  - 美元符号定义 jQuery

  - 选择符（selector）“查询”和“查找” HTML 元素

  - jQuery 的 action() 执行对元素的操作

    ### 示例

    $(this).hide() - 隐藏当前元素

    $("p").hide() - 隐藏所有段落

    $(".test").hide() - 隐藏所有 class="test" 的所有元素

    $("#test").hide() - 隐藏所有 id="test" 的元素

<script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.1/jquery.js"></script>