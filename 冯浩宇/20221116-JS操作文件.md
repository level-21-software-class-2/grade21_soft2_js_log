# JS操作文件

```js
let fileUploader = document.getElementById('fileUploader");
let photo = document.getElementById("photo');
fileUploader.addEventListener('change', function () {
	// 清除背景图片
	photo.style.backgroundImage =''
	//判断文件是否选择
	if(!fileUploader.value){
	console.log("文件没有选择");
	return;
	}
	//获取文件
	let file = fileUploader.files[0];
	// 读取文件
	let reader = new FileReader();
	reader.onload = function (x){
	let data = x.target.result;
	photo.style.backgroundImage =`url(${data})`；
	}
	reader.readAsDataURL(file);
});
```

