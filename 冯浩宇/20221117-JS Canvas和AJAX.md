# JS Canvas和AJAX

## Ajax 的工作原理

JavaScript 需要使用浏览器内置的 XMLHttpRequest 对象向服务器发送 HTTP 请求，并接收服务器响应的数据。

### 发送 Ajax 请求

```js
//要发送 Ajax 请求，首先需要实例化一个 XMLHttpRequest 对象
let request = new XMLHttpRequest();
//使用 XMLHttpRequest 对象的 open() 方法来初始化一个请求
XMLHttpRequest.open(method, url, async, user, password);
//最后，使用 XMLHttpRequest 对象的 send() 方法将请求发送到服务器
XMLHttpRequest.send(body);
```

```js
 function displayFullName() {
       // 创建 XMLHttpRequest 对象
       let request = new XMLHttpRequest();
       // 实例化请求对象
       request.open("GET", "test.php ?name=C语言中文网&url=http://c.biancheng.net/");
       // 监听 readyState 的变化
      request.onreadystatechange = function() {
       // 检查请求是否成功
         if(this.readyState === 4 && this.status === 200) {
            // 将来自服务器的响应插入当前页面
            document.getElementById("result").innerHTML = this.responseText;
         }
       };
          // 将请求发送到服务器
          request.send();
 }
```



参数说明：

- method：请求的类型（使用的 HTTP 方法），例如 GET、POST、PUT、HEAD、DELETE 等；
- url：请求的地址；
- async：可选参数，布尔类型，表示是否请求是否异步执行，默认值为 true；
- user：可选参数，表示用户名，主要用来认证，默认值为 null；
- password：可选参数，表示密码，同样用来认证，默认值为 null。

## Canvas的基本使用

#### 1.图形绘制

需要理解些概念：

- 路径的概念
- 路径的绘制
  - 描边 stroke()
  - 填充 fill()
- 闭合路径
  - 手动闭合
  - 程序闭合 closePath()
- 填充规则(非零环绕)
- 开启新的路径 beginPath()

#### 2.设置样式

- 画笔的状态
  - lineWidth 线宽，默认1px
  - lineCap 线末端类型：(butt默认)、round、square
  - lineJoin 相交线的拐点 miter(默认)、round、bevel
  - strokeStyle 线的颜色
  - fillStyle 填充颜色
  - setLineDash() 设置虚线
  - getLineDash() 获取虚线宽度集合
  - lineDashOffset 设置虚线偏移量（负值向右偏移）

### 四.Canvas图形绘制

#### 1.矩形绘制

- rect(x,y,w,h) 没有独立路径
- strokeRect(x,y,w,h) 有独立路径，不影响别的绘制
- fillRect(x,y,w,h) 有独立路径，不影响别的绘制
- clearRect(x,y,w,h) 擦除矩形区域

#### 2.圆弧绘制

- 弧度概念
- arc()
  - x 圆心横坐标
  - y 圆心纵坐标
  - r 半径
  - startAngle 开始角度
  - endAngle 结束角度
  - anticlockwise 是否逆时针方向绘制（默认false表示顺时针；true表示逆时针）

#### 3.绘制文本

- ctx.font = '微软雅黑' 设置字体
- strokeText()
- fillText(text,x,y,maxWidth)
  - text 要绘制的文本
  - x,y 文本绘制的坐标（文本左下角）
  - maxWidth 设置文本最大宽度，可选参数
- ctx.textAlign文本水平对齐方式，相对绘制坐标来说的
  - left
  - center
  - right
  - start 默认
  - end
  - direction属性css(rtl ltr) start和end于此相关
    - 如果是ltr,start和left表现一致
    - 如果是rtl,start和right表现一致
- ctx.textBaseline 设置基线（垂直对齐方式  ）
  - top 文本的基线处于文本的正上方，并且有一段距离
  - middle 文本的基线处于文本的正中间
  - bottom 文本的基线处于文本的证下方，并且有一段距离
  - hanging 文本的基线处于文本的正上方，并且和文本粘合
  - alphabetic 默认值，基线处于文本的下方，并且穿过文字
  - ideographic 和bottom相似，但是不一样
- measureText() 获取文本宽度obj.width

### 五.做动画

#### 1.绘制图片

- drawImage()
  - 三个参数drawImage(img,x,y)
    - img 图片对象、canvas对象、video对象
    - x,y 图片绘制的左上角
  - 五个参数drawImage(img,x,y,w,h)
    - img 图片对象、canvas对象、video对象
    - x,y 图片绘制的左上角
    - w,h 图片绘制尺寸设置(图片缩放，不是截取)
  - 九个参数drawImage(img,x,y,w,h,x1,y1,w1,h1)
    - img 图片对象、canvas对象、video对象
    - x,y,w,h 图片中的一个矩形区域
    - x1,y1,w1,h1 画布中的一个矩形区域

#### 2.序列帧动画

- 绘制精灵图
- 动起来
- 控制边界
- 键盘控制

#### 3.坐标变换

- 平移 移动画布的原点
  - translate(x,y) 参数表示移动目标点的坐标
- 缩放
  - scale(x,y) 参数表示宽高的缩放比例
- 旋转
  - rotate(angle) 参数表示旋转角度